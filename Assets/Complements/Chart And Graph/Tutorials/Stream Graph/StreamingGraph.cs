#define Graph_And_Chart_PRO
using System.Collections.Generic;
using UnityEngine;
using System.Collections;
using ChartAndGraph;

public class StreamingGraph_Macerado : MonoBehaviour
{
    [SerializeField]
    private GraphChart[] graph;
    public int TotalPoints = 5;
    float lastTime = 0f;
    float lastX = 0f;

    void Start()
    {
        if (graph == null) // the ChartGraph info is obtained via the inspector
            return;

        graph[0].DataSource.StartBatch(); // calling StartBatch allows changing the graph data without redrawing the graph for every change
        graph[0].DataSource.ClearCategory("SP1"); // clear the "Player 1" category. this category is defined using the GraphChart inspector
        graph[0].DataSource.ClearCategory("Altura1"); // clear the "Player 1" category. this category is defined using the GraphChart inspector
        graph[0].DataSource.EndBatch(); // finally we call EndBatch , this will cause the GraphChart to redraw itself
        graph[1].DataSource.StartBatch(); // calling StartBatch allows changing the graph data without redrawing the graph for every change
        graph[1].DataSource.ClearCategory("SP2"); // clear the "Player 1" category. this category is defined using the GraphChart inspector
        graph[1].DataSource.ClearCategory("Altura2"); // clear the "Player 1" category. this category is defined using the GraphChart inspector
        graph[1].DataSource.EndBatch(); // finally we call EndBatch , this will cause the GraphChart to redraw itself


    }



    void Update()
    {
        float time = Time.time;
        if (lastTime + 0.1f < time)
        {
            lastTime = time;

            graph[0].DataSource.AddPointToCategoryRealtime("SP1", Setting_Macerado.setting._time, Setting_Macerado.setting._SP1); // each time we call AddPointToCategory
            graph[0].DataSource.AddPointToCategoryRealtime("Altura1", Setting_Macerado.setting._time, Setting_Macerado.setting._volumen); // each time we call AddPointToCategory
            graph[1].DataSource.AddPointToCategoryRealtime("SP2", Setting_Macerado.setting._time, Setting_Macerado.setting._SP2); // each time we call AddPointToCategory
            graph[1].DataSource.AddPointToCategoryRealtime("Altura2", Setting_Macerado.setting._time, Setting_Macerado.setting._volumen2); // each time we call AddPointToCategory


        }

    }
}
