﻿Shader "FI/Evaporator" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_HeatColor("HeatColor", Color) = (1, 0, 0, 0)
		_FillingColor("FillingColor", Color) = (1, 0, 0, 0)
		_MaskTex ("Albedo (RGB)", 2D) = "white" {}
		_Glossiness ("Smoothness", Range(0,1)) = 0.5
		_Metallic ("Metallic", Range(0,1)) = 0.0

		_Emission("Emission", Float) = 1
		_Percentage("Percentage", Range(0, 1)) = 0.5
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200

		Cull Off

		CGPROGRAM

		#pragma surface surf Standard fullforwardshadows

		#pragma target 3.0

		sampler2D _MaskTex;

		struct Input {
			float2 uv_MaskTex;
		};

		half _Glossiness;
		half _Metallic;

		fixed4 _Color, _HeatColor, _FillingColor;

		half _Percentage, _Emission;

		UNITY_INSTANCING_BUFFER_START(Props)

		UNITY_INSTANCING_BUFFER_END(Props)

		void surf (Input IN, inout SurfaceOutputStandard o) {
			fixed4 c = _Color;
			fixed mask = tex2D(_MaskTex, IN.uv_MaskTex);

			half em = 0;

			if (_Percentage >= IN.uv_MaskTex.x)
			{
				c = _FillingColor;
				em = _Emission;
			}
			else
			{
				em = 0;
			}

			o.Albedo = lerp(c.rgb, _HeatColor.rgb, mask);
			o.Emission = lerp(c * em, lerp(_Color, _HeatColor * _Emission * 1.5, _Percentage), mask);
			o.Metallic = _Metallic;
			o.Smoothness = _Glossiness;
			o.Alpha = c.a;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
