﻿Shader "FI/Pipeline" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_FillingColor("FillingColor", Color) = (1, 0, 0, 0)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Glossiness ("Smoothness", Range(0,1)) = 0.5
		_Metallic ("Metallic", Range(0,1)) = 0.0

		_Emission("Emission", Float) = 1
		_Percentage("Percentage", Range(0, 1)) = 0.5
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200

		Cull Off

		CGPROGRAM

		#pragma surface surf Standard fullforwardshadows

		#pragma target 3.0

		sampler2D _MainTex;

		struct Input {
			float2 uv_MainTex;
		};

		half _Glossiness;
		half _Metallic;

		fixed4 _Color, _FillingColor;

		half _Percentage, _Emission;

		UNITY_INSTANCING_BUFFER_START(Props)

		UNITY_INSTANCING_BUFFER_END(Props)

		void surf (Input IN, inout SurfaceOutputStandard o) {
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;

			half em = 0;

			if (_Percentage >= IN.uv_MainTex.x)
			{
				c = _FillingColor;
				em = _Emission;
			}
			else
			{
				em = 0;
			}

			o.Albedo = c.rgb;
			o.Emission = c * em;
			o.Metallic = _Metallic;
			o.Smoothness = _Glossiness;
			o.Alpha = c.a;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
