﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityStandardAssets.Characters.FirstPerson;

public class ManagerScriptCharacter : MonoBehaviour
{
    Vector3 position =new Vector3(-8, -0.5f, 4);
    Vector3 escala = new Vector3(7, 7, 7);
    Quaternion rotation = Quaternion.Euler(0, -180, 0);
    private void Awake()
    {
        if (SceneManager.GetActiveScene().buildIndex == MultiplayerSetting.multiplayerSetting.menuScene)
        {
            gameObject.transform.position = position;
            gameObject.transform.localScale = escala;
            gameObject.transform.rotation = rotation;
            GetComponent<FirstPersonController>().enabled = false;
            GetComponent<Menu>().enabled = false;
            GetComponent<Rigidbody>().useGravity = false;
            GetComponent<Rigidbody>().isKinematic = false;
            GetComponentInChildren<Camera>().gameObject.SetActive(false);
            GetComponent<PhotonView>().enabled = false;
            GetComponentInChildren<PhotonAnimatorView>().enabled = false;
            GetComponent<PhotonRigidbodyView>().enabled = false;
            GetComponent<PhotonTransformView>().enabled = false;
        }
        else if (SceneManager.GetActiveScene().buildIndex == 4)
        {
            GetComponent<FirstPersonController>().enabled = true;
            GetComponent<Menu>().enabled = true;
            GetComponent<Rigidbody>().useGravity = true;
            GetComponent<Rigidbody>().isKinematic = true;
            GetComponentInChildren<Camera>().gameObject.SetActive(true);
            GetComponent<PhotonView>().enabled = false;
            GetComponentInChildren<PhotonAnimatorView>().enabled = false;
            GetComponent<PhotonRigidbodyView>().enabled = false;
            GetComponent<PhotonTransformView>().enabled = false;
        }
        else
        {
            GetComponent<FirstPersonController>().enabled = true;
            GetComponent<Menu>().enabled = true;
            GetComponent<Rigidbody>().useGravity = true;
            GetComponent<Rigidbody>().isKinematic = true;
            GetComponentInChildren<Camera>().gameObject.SetActive(true);
            GetComponent<PhotonView>().enabled = true;
            GetComponentInChildren<PhotonAnimatorView>().enabled = true;
            GetComponent<PhotonRigidbodyView>().enabled = true;
            GetComponent<PhotonTransformView>().enabled = true;
        }

    }
}
