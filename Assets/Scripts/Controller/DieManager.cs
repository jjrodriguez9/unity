﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class DieManager : MonoBehaviour
{
    private void OnCollisionEnter(Collision collision)
    {
        Debug.LogError(collision.other.tag);
        if (collision.other.tag ==  "Player")
        {
            int spawnPicker = GameSetup.gameSetup._player.GetComponent<FirstPersonController>()._PV.Owner.ActorNumber; //sacando random una posicion
            if (spawnPicker > 0)
            {
                spawnPicker = spawnPicker - 1;
            }
            GameSetup.gameSetup._player.transform.position = CharacterSetup.characterSetup.spawPoints[spawnPicker].position;
            GameSetup.gameSetup._player.transform.rotation = CharacterSetup.characterSetup.spawPoints[spawnPicker].rotation;
        }
    }
}
