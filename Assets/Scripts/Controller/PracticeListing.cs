﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Photon.Pun;

public class PracticeListing : MonoBehaviourPunCallbacks
{
    [SerializeField]
    private TMP_Text namePractice; //nombre de la practica
    private Practice practic; // objeto de la entidad de la practica
    public Practice _practice { get { return practic; } } //entidad publico que es un intermedio entre la variable publica y la privada para la obtencion de sus datos
    [SerializeField]
    private TMP_Text text; //asignacion de un texto 

    public void Practice_Listing(Practice practice) //metodo para enviar la entidad en la clase
    {
        namePractice.text = practice.tema;
        practic = practice;
    }

    public void onClickJoin() // metodo para union a la sala en el servidor de photon 
    {
        //Debug.Log(practic.tema);
        DataInfo.dataInfo._practiceJoind = practic; //cargando los datos de la practica dentro de la variable
        GameObject panel = GameObject.Find("Screen").gameObject.transform.Find("Load").gameObject;
        panel.transform.Find("Canvas").gameObject.transform.Find("Texto").GetComponent<TMP_Text>().text = "Conectando";
        panel.SetActive(true);
        StartCoroutine(wait(3));
    }
    IEnumerator wait(int seconds)
    {
        yield return new WaitForSeconds(seconds);
        GameObject panel = GameObject.Find("Screen").gameObject.transform.Find("Load").gameObject;
        panel.transform.Find("Canvas").gameObject.transform.Find("Texto").GetComponent<TMP_Text>().text = "Conexion Existosa"; // cambiando el texto
        StartCoroutine(waitone(1));
    }
    IEnumerator waitone(int seconds)
    {
        yield return new WaitForSeconds(seconds);
        PhotonNetwork.JoinRoom(practic.tema); //uniendo a la sala creada en el servidor de photon
    }

    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        //base.OnJoinRandomFailed(returnCode, message);
        GameObject panel = GameObject.Find("Screen").gameObject.transform.Find("Load").gameObject;
        panel.transform.Find("Canvas").gameObject.transform.Find("Texto").GetComponent<TMP_Text>().text = message; // cambiando el texto
        panel.transform.Find("Canvas").gameObject.transform.Find("Error_student").gameObject.SetActive(true);
    }
}
