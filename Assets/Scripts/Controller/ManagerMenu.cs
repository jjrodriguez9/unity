﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ManagerMenu : MonoBehaviour
{
    private TypeRequest typeRequest;
    [SerializeField]
    private GameObject[] tabs;
    // Start is called before the first frame update
    private void Awake()
    {
        typeRequest = new TypeRequest();
        if (DataInfo.dataInfo._authentication.tipo_us == typeRequest.getStudent())
        {
            tabs[1].SetActive(false);
        }
        if (SceneManager.GetActiveScene().buildIndex == MultiplayerSetting.multiplayerSetting.multiplayerScene)
        {
            if (DataInfo.dataInfo._authentication.tipo_us == typeRequest.getTeacher())
            {
                tabs[1].SetActive(false);
            }
        }
    }
}
