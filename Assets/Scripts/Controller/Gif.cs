﻿using UnityEngine;
using UnityEngine.UI;

public class Gif : MonoBehaviour
{
    [SerializeField]
    private Texture2D[] frames; //realizando las textura del expiral
    [SerializeField]
    private int fps = 10; //timpo en el que se realiza el cambio de la imagen
    // Update is called once per frame
    void Update()
    {
        int index = (int)(Time.time * fps) % frames.Length; //sacando los index de cada posicion de la textura del espiral
        GetComponent<RawImage>().texture = frames[index]; //cambiado las textura en el espiral
    }
}
