﻿
using UnityEngine;
using System.Runtime.InteropServices;
using System;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson;
using Photon.Pun;

public class Control_trayectoria_soldador : MonoBehaviour
{
    
    [SerializeField] private Transform Q1T;
    [SerializeField] private Transform Q2T;
    [SerializeField] private Transform Q3T;
    [SerializeField] private Transform Q4T;
    [SerializeField] private Transform PINZA;
    [SerializeField] private GameObject hd;
    [SerializeField] private GameObject h_robot;
    [SerializeField] private GameObject Linea;
    

    private float time;
    private float To;
    private float TimeSample;

    //[Range(-1, 1)]
    public float hxd;
    //[Range(-1, 1)]
    public float hyd;
    //[Range(0, 2)]
    public float hzd;

    [Range(-90, 90)]
    public float q1d;
    [Range(-90, 90)]
    public float q2d;
    [Range(-90, 90)]
    public float q3d;
    [Range(-90, 90)]
    public float q4d;

    // [SerializeField] 
    private float hx, hy, hz;
    private float hxdp, hydp, hzdp;
    //[SerializeField]
    private float q1, q2, q3, q4;
    private float k = 0;
    private float paso = 0.33f;
    private float tf;
    private Memoryshare memoryshare;

    public Slider slider_xd;
    public Slider slider_yd;
    public Slider slider_zd;

    public Text[] datos = new Text[9];
    public Text[] datos_qd = new Text[4];
    public Slider[] sliders_qd = new Slider[4];

    public float hex;
    public float hey;
    public float hez;

    public GameObject panel_controlador;
    public GameObject panel_cinematica;
    public GameObject canvas_chart;

    [SerializeField]
    private PhotonView PV;

    //public GameObject primera_persona;
    //public GameObject camara;



    void Start()
    {
        if (!DataInfo.dataInfo.singlePractice)
        {
            PV = GetComponent<PhotonView>();
            if (PV.IsMine)
            {
                Inicializate();
            }
        }
        else
        {
            Inicializate();
        }
    }

    // Update is called once per frame
    void Update()

    {
        if (!DataInfo.dataInfo.singlePractice)
        {
            if (PV.IsMine)
            {
                ROBOT();
                //Mov_hd();
                Objetivos_Secundarios();
            }
        }
        else
        {
            ROBOT();
            //Mov_hd();
            Objetivos_Secundarios();
        }
        
    }

    private void Inicializate()
    {
        memoryshare = new Memoryshare();
        memoryshare.freeMemoriShare();

        memoryshare.openMemoryShare("UNITY", 2);
        memoryshare.openMemoryShare("MATLAB", 2);
        memoryshare.openMemoryShare("CINEMATICA", 2);
        memoryshare.openMemoryShare("ENABLE", 1);

        panel_cinematica.SetActive(false);
        panel_controlador.SetActive(true);
        canvas_chart.SetActive(true);
        //primera_persona = GameObject.Find("Male").GetComponent<FirstPersonController>().gameObject;
        //primera_persona.SetActive(true);
        //camara.SetActive(false);

        float q1 = (0 * Mathf.PI) / 180;
        float q2 = (60 * Mathf.PI) / 180;
        float q3 = (-30 * Mathf.PI) / 180;
        float q4 = (0 * Mathf.PI) / 180;

        memoryshare.Setfloat("UNITY", q1, 0);
        memoryshare.Setfloat("UNITY", q2, 1);
        memoryshare.Setfloat("UNITY", q3, 2);
        memoryshare.Setfloat("UNITY", q4, 3);

        Cinematica();

        memoryshare.Setfloat("CINEMATICA", hx, 0);
        memoryshare.Setfloat("CINEMATICA", hy, 1);
        memoryshare.Setfloat("CINEMATICA", hz, 2);

        k = 0;

        // Funciones que define la forma corazón
        hxd = 13 * (0.12f * Mathf.Sin(0.01f * k) - 0.04f * Mathf.Sin(0.03f * k));
        hyd = 13 * (0.13f * Mathf.Cos(0.01f * k) - 0.05f * Mathf.Cos(0.02f * k) - 0.02f * Mathf.Cos(0.03f * k) - 0.01f * Mathf.Cos(0.04f * k)) + 0.25f;
        hzd = 0.25f * Mathf.Sin(0.01f * k) + 1.5f;

        // Velocidades de la trayectoria del corazón
        hxdp = 13 * (0.12f * Mathf.Cos(0.01f * k) * 0.03f - 0.04f * Mathf.Cos(0.03f * k) * 0.03f);
        hydp = 13 * (-0.13f * Mathf.Sin(0.01f * k) * 0.01f + 0.05f * Mathf.Sin(0.02f * k) * 0.02f + 0.02f * Mathf.Sin(0.03f * k) * 0.03f + 0.01f * Mathf.Sin(0.04f * k) * 0.04f);
        hzdp = 0.25f * 0.01f * Mathf.Cos(0.01f * k);

        memoryshare.Setfloat("UNITY", hxd, 4);
        memoryshare.Setfloat("UNITY", hyd, 5);
        memoryshare.Setfloat("UNITY", hzd, 6);


        this.Errores();

        memoryshare.SetInt("ENABLE", 1, 0);



        To = 0;
        TimeSample = 0.1f;

    }

    private void ROBOT()
    {

        To += Time.deltaTime;
        if (To >= TimeSample)

        {
            time += Time.deltaTime;
            k = tf / 1f;

            hxd = 6 * (0.12f * Mathf.Sin(0.01f * k) - 0.04f * Mathf.Sin(0.03f * k));
            hyd = 6 * (0.13f * Mathf.Cos(0.01f * k) - 0.05f * Mathf.Cos(0.02f * k) - 0.02f * Mathf.Cos(0.03f * k) - 0.01f * Mathf.Cos(0.04f * k)) + 0.25f;
            hzd = 0.25f * Mathf.Sin(0.01f * k) + 1.5f;

            hxdp = 6 * (0.12f * Mathf.Cos(0.01f * k) * 0.03f - 0.04f * Mathf.Cos(0.03f * k) * 0.03f);
            hydp = 6 * (-0.13f * Mathf.Sin(0.01f * k) * 0.01f + 0.05f * Mathf.Sin(0.02f * k) * 0.02f + 0.02f * Mathf.Sin(0.03f * k) * 0.03f + 0.01f * Mathf.Sin(0.04f * k) * 0.04f);
            hzdp = 0.25f * 0.01f * Mathf.Cos(0.01f * k);

            //hxdp = 0f;
            //hydp = 0f;
            //hzdp = 0f;

            float q1p = memoryshare.Getfloat("MATLAB", 0);
            float q2p = memoryshare.Getfloat("MATLAB", 1);
            float q3p = memoryshare.Getfloat("MATLAB", 2);
            float q4p = memoryshare.Getfloat("MATLAB", 3);

            k = memoryshare.Getfloat("MATLAB", 4);

            q1 = q1p * To + q1;
            q2 = q2p * To + q2;
            q3 = q3p * To + q3;
            q4 = q4p * To + q4;

            Cinematica();

            memoryshare.Setfloat("UNITY", q1,0);
            memoryshare.Setfloat("UNITY", q2,1);
            memoryshare.Setfloat("UNITY", q3,2);
            memoryshare.Setfloat("UNITY", q4,3);

            memoryshare.Setfloat("UNITY", hxd,4);
            memoryshare.Setfloat("UNITY", hyd,5);
            memoryshare.Setfloat("UNITY", hzd,6);

            memoryshare.Setfloat("UNITY", hxdp,7);
            memoryshare.Setfloat("UNITY", hydp,8);
            memoryshare.Setfloat("UNITY", hzdp,9);

            memoryshare.Setfloat("UNITY", q1d,10);
            memoryshare.Setfloat("UNITY", q2d,11);
            memoryshare.Setfloat("UNITY", q3d,12);
            memoryshare.Setfloat("UNITY", q4d,13);

            memoryshare.Setfloat("CINEMATICA", hx,0);
            memoryshare.Setfloat("CINEMATICA", hy,1);
            memoryshare.Setfloat("CINEMATICA", hz,2);

            // animacion unity
            float q4_m = ((-q4) * Mathf.Rad2Deg) -90 ;
            float q3_m = ((-q3) * Mathf.Rad2Deg);
            float q2_m = ((-q2) * Mathf.Rad2Deg) ;
            float q1_m = ((q1) * Mathf.Rad2Deg);

            Q1T.transform.localRotation = Quaternion.Euler(90f, 0f, -q1_m);
            Q2T.transform.localRotation = Quaternion.Euler(-q2_m, 0f, 0f);
            Q3T.transform.localRotation = Quaternion.Euler(-90f, -90f, q3_m);
            Q4T.transform.localRotation = Quaternion.Euler(q4_m, -90f, 90f);

            Errores();
            hd.transform.localPosition = new Vector3(hyd, hzd, hxd);
            //                                           //3.953445       
            h_robot.transform.localPosition = new Vector3(hy, hz, hx);
            text_values();
            tf = tf + paso;
            To = 0;
        }


    }

    private void Cinematica()
    {
        float l1 = 0.125f;
        float l2 = 0.275f;
        float l3 = 0.275f;
        float l4 = 0.15f;

        l1 = 0.7233581f;
        l2 = 0.8889994f;
        l3 = 0.830925f;
        l4 = 0.644475f;


        
        
       

        hx = l2 * Mathf.Cos(q1) * Mathf.Cos(q2) + l3 * Mathf.Cos(q1) * Mathf.Cos(q1 + q3) + l4 * Mathf.Cos(q1) * Mathf.Cos(q2 + q3 + q4) + 0.225f * Mathf.Cos(q1); hy = Linea.transform.position.x;
        hy = l2 * Mathf.Sin(q1) * Mathf.Cos(q2) + l3 * Mathf.Sin(q1) * Mathf.Cos(q2 + q3) + l4 * Mathf.Sin(q1) * Mathf.Cos(q2 + q3 + q4) + 0.225f * Mathf.Sin(q1); hx = Linea.transform.position.z;
        hz = l1 + l2 * Mathf.Sin(q2) + l3 * Mathf.Sin(q2 + q3) + l4 * Mathf.Sin(q2 + q3 + q4); hz = Linea.transform.position.y;

    }

    private void Errores()
    {
        hex = hxd - hx;
        hey = hyd - hy;
        hez = hzd - hz;

        if (!DataInfo.dataInfo.singlePractice)
        {
            PV.RPC("MultiGraphics", RpcTarget.All,hex,hey,hez);
        }

    }


    private void Mov_hd()
    {
        if (Input.GetKeyDown(KeyCode.Keypad8))
            hxd += 0.1f;

        if (Input.GetKeyDown(KeyCode.Keypad2))
            hxd -= 0.1f;

        if (Input.GetKeyDown(KeyCode.Keypad4))
            hyd += 0.1f;

        if (Input.GetKeyDown(KeyCode.Keypad6))
            hyd -= 0.1f;

        if (Input.GetKeyDown(KeyCode.Keypad7))
            hzd -= 0.1f;

        if (Input.GetKeyDown(KeyCode.Keypad9))
            hzd += 0.1f;

    }


    private void Objetivos_Secundarios()
    {
        if (Input.GetKeyDown(KeyCode.T))
            q1d += 5f;
        else if (Input.GetKeyDown(KeyCode.G))
           q1d -= 5f;


        if (Input.GetKeyDown(KeyCode.Y))
            q2d += 5f;
        else if (Input.GetKeyDown(KeyCode.H))
            q2d -= 5f;


        if (Input.GetKeyDown(KeyCode.U))
            q3d += 5f;
        else if (Input.GetKeyDown(KeyCode.J))
            q3d -= 5f;


        if (Input.GetKeyDown(KeyCode.I))
            q4d += 5f;
        else if (Input.GetKeyDown(KeyCode.K))
            q4d -= 5f;

    }





    private void text_values()
    {
        datos[0].text = "hxd: " + Math.Round(hxd, 2).ToString();
        datos[1].text = "hyd: " + Math.Round(hyd, 2).ToString();
        datos[2].text = "hzd: " + Math.Round(hzd, 2).ToString();
        datos[3].text = "hx: " + Math.Round(hx, 3).ToString();
        datos[4].text = "hy: " + Math.Round(hy, 3).ToString();
        datos[5].text = "hz: " + Math.Round(hz, 3).ToString();
        datos[6].text = "hxe: " + Math.Round(hex, 4).ToString();
        datos[7].text = "hye: " + Math.Round(hey, 4).ToString();
        datos[8].text = "hze: " + Math.Round(hez, 4).ToString();

        datos_qd[0].text = Math.Round(q1d, 2).ToString() + "º";
        datos_qd[1].text = Math.Round(q2d, 2).ToString() + "º";
        datos_qd[2].text = Math.Round(q3d, 2).ToString() + "º";
        datos_qd[3].text = Math.Round(q4d, 2).ToString() + "º";

        sliders_qd[0].value = q1d;
        sliders_qd[1].value = q2d;
        sliders_qd[2].value = q3d;
        sliders_qd[3].value = q4d;

    }

    
}
