﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;


public class EfectoSoldadura : MonoBehaviour
{
    [SerializeField]
    private PhotonView PV;

    public GameObject Fuego;

    private void Start()
    {
        if (!DataInfo.dataInfo.singlePractice)
        {
            PV = GetComponent<PhotonView>();
            if (PV.IsMine)
            {
                Inicializate();
            }
        }
        else
        {
            Inicializate();
        }
    }


    private void Inicializate()
    {
        
        Fuego.SetActive(false);

    }
    public void OnTriggerEnter(Collider other)
    {
        Debug.Log("Entro");

        //Destroy(other.gameObject);
        if (!DataInfo.dataInfo.singlePractice)
        {
            PV.RPC("FuegoAct", RpcTarget.All, true);
        }
        else
        {
            FuegoAct(true);
        }
    }

    public void OnTriggerExit(Collider other)
    {
        Debug.Log("Salio");
        if (!DataInfo.dataInfo.singlePractice)
        {
            PV.RPC("FuegoAct", RpcTarget.All, false);
        }
        else
        {
            FuegoAct(false);
        }
    }


    [PunRPC]
    private void FuegoAct(bool val)
    {
        Fuego.SetActive(val);
    }
}
