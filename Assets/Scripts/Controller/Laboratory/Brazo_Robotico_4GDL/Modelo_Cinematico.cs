﻿
using UnityEngine;
using System.Runtime.InteropServices;
using System;
using UnityEngine.UI;
using Photon.Pun;

public class Modelo_Cinematico : MonoBehaviour
{
    


    //[SerializeField] private Cerrar_Enlace Close = new Cerrar_Enlace();

    // [SerializeField] private Control_Interfaz Interfaz = new Control_Interfaz();


    [SerializeField] private Transform Q1T;
    [SerializeField] private Transform Q2T;
    [SerializeField] private Transform Q3T;
    [SerializeField] private Transform Q4T;
    [SerializeField] private Transform PINZA;
    [SerializeField] private GameObject hd;
    [SerializeField] private GameObject h_robot;
    [SerializeField] private GameObject Linea;
    [SerializeField] private float correxion;

    private float time;
    private float To;
    private float TimeSample;

    

    // [SerializeField] 
    private float hx, hy, hz;
    private float hxdp, hydp, hzdp;
    //[SerializeField]
    public float q1, q2, q3, q4;
    private float k = 0;
    private Memoryshare memoryshare;

    public Slider slider_q1;
    public Slider slider_q2;
    public Slider slider_q3;
    public Slider slider_q4;


    public Text[] datos = new Text[7];

    public float hex;
    public float hey;
    public float hez;

    public GameObject panel_controlador;
    public GameObject panel_cinematica;
    public GameObject canvas_chart;
    //public GameObject primera_persona;
    //public GameObject camara;
    [SerializeField]
    private PhotonView PV;



    void Start()
    {
        if (!DataInfo.dataInfo.singlePractice)
        {
            PV = GetComponent<PhotonView>();
            if (PV.IsMine)
            {
                Inicializate();
            }
        }
        else
        {
            Inicializate();
        }

    }

    private void Inicializate()
    {
        memoryshare = new Memoryshare();
        memoryshare.freeMemoriShare();

        memoryshare.openMemoryShare("UNITY", 2);
        memoryshare.openMemoryShare("MATLAB", 2);
        memoryshare.openMemoryShare("CINEMATICA", 2);
        memoryshare.openMemoryShare("ENABLE", 1);

        panel_cinematica.SetActive(true);
        panel_controlador.SetActive(false);
        canvas_chart.SetActive(false);
        //primera_persona.SetActive(false);
        //camara.SetActive(true);

        q1 = (0 * Mathf.PI) / 180;
        q2 = (60 * Mathf.PI) / 180;
        q3 = (-30 * Mathf.PI) / 180;
        q4 = (0 * Mathf.PI) / 180;


        memoryshare.Setfloat("UNITY", q1, 0);
        memoryshare.Setfloat("UNITY", q2, 1);
        memoryshare.Setfloat("UNITY", q3, 2);
        memoryshare.Setfloat("UNITY", q4, 3);



        Cinematica();

        memoryshare.Setfloat("CINEMATICA", hx, 0);
        memoryshare.Setfloat("CINEMATICA", hy, 1);
        memoryshare.Setfloat("CINEMATICA", hz, 2);

        
        memoryshare.SetInt("ENABLE", 1, 0);

        To = 0;
        TimeSample = 0.1f;
    }

    // Update is called once per frame
    void Update()

    {
        if (!DataInfo.dataInfo.singlePractice)
        {
            if (PV.IsMine)
            {
                ROBOT();
            }
        }
        else
        {
            ROBOT();
        }
    }


    private void ROBOT()
    {

        To += Time.deltaTime;
        if (To >= TimeSample)

        {

            q1 = (slider_q1.value) * 3.1416f / 180;
            q2 = (slider_q2.value) * 3.1416f / 180;
            q3 = (slider_q3.value) * 3.1416f / 180;
            q4 = (slider_q4.value) * 3.1416f / 180;

            Cinematica();

            memoryshare.Setfloat("UNITY", q1,0);
            memoryshare.Setfloat("UNITY", q2,1);
            memoryshare.Setfloat("UNITY", q3,2);
            memoryshare.Setfloat("UNITY", q4,3);

            memoryshare.Setfloat("CINEMATICA", hx,0);
            memoryshare.Setfloat("CINEMATICA", hy,1);
            memoryshare.Setfloat("CINEMATICA", hz,2);


            // animacion unity
            float q4_m = ((-q4) * Mathf.Rad2Deg) -90 ;
            float q3_m = ((-q3) * Mathf.Rad2Deg);
            float q2_m = ((-q2) * Mathf.Rad2Deg) ;
            float q1_m = ((q1) * Mathf.Rad2Deg);

            //GameObject.FindObjectOfType<Control_Interfaz>().Actualizacion_Velocidades(q1 * Mathf.Rad2Deg, q2 * Mathf.Rad2Deg, q3 * Mathf.Rad2Deg, q4 * Mathf.Rad2Deg);

            Q1T.transform.localRotation = Quaternion.Euler(90f, 0f, -q1_m);
            Q2T.transform.localRotation = Quaternion.Euler(-q2_m, 0f, 0f);
            Q3T.transform.localRotation = Quaternion.Euler(-90f, -90f, q3_m);
            Q4T.transform.localRotation = Quaternion.Euler(q4_m, -90f, 90f);
            // PINZA.transform.localEulerAngles = new Vector3(0f,3f, 0f);
  
            //hd.transform.localPosition = correxion * new Vector3(hyd, hzd, hxd);
            //                                           //3.953445       
            h_robot.transform.localPosition = new Vector3(hy, hz, hx);
            text_values();
            To = 0;
        }


    }


    private void Cinematica()
    {


        float l1 = 1.25f;
        float l2 = 0.49f;
        float l3 = 0.4f;
        float l4 = 0.55f;


        hx = l2 * Mathf.Cos(q1) * Mathf.Cos(q2) + l3 * Mathf.Cos(q1) * Mathf.Cos(q1 + q3) + l4 * Mathf.Cos(q1) * Mathf.Cos(q2 + q3 + q4) + 0.225f * Mathf.Cos(q1);           
        hy = l2 * Mathf.Sin(q1) * Mathf.Cos(q2) + l3 * Mathf.Sin(q1) * Mathf.Cos(q2 + q3) + l4 * Mathf.Sin(q1) * Mathf.Cos(q2 + q3 + q4) + 0.225f * Mathf.Sin(q1);
        hz = l1 + l2 * Mathf.Sin(q2) + l3 * Mathf.Sin(q2 + q3) + l4 * Mathf.Sin(q2 + q3 + q4);                                                                                                                                                                                                                         hy = Linea.transform.position.x; hz = Linea.transform.position.y; hx = Linea.transform.position.z;

    }


    private void text_values()
    {
        datos[0].text = "q1: " + Math.Round(q1, 3).ToString();
        datos[1].text = "q2: " + Math.Round(q2, 3).ToString();
        datos[2].text = "q3: " + Math.Round(q3, 3).ToString();
        datos[3].text = "q4 " +  Math.Round(q4, 3).ToString();
        datos[4].text = "hx " + Math.Round(hx, 3).ToString();
        datos[5].text = "hy " + Math.Round(hy, 3).ToString();
        datos[6].text = "hz " + Math.Round(hz, 3).ToString();
        //datos[7].gameObject.SetActive(false);
        //datos[8].gameObject.SetActive(false);
    }

    
}
