﻿using UnityEngine;
using System.Runtime.InteropServices;
using System;
using UnityEngine.UI;
using Photon.Pun;

public class Hallar_trayectoria : MonoBehaviour
{
    
    // Start is called before the first frame update


    public GameObject trayectoria_d;
    float hxd, hyd, hzd;
    private Memoryshare memoryshare;

    [SerializeField]
    private PhotonView PV;

    void Start()
    {
        if (!DataInfo.dataInfo.singlePractice)
        {
            PV = GetComponent<PhotonView>();
            if (PV.IsMine)
            {
                Inicializate();
            }
        }
        else
        {
            Inicializate();
        }
    }

    private void Inicializate()
    {
        memoryshare = new Memoryshare();
        memoryshare.freeMemoriShare();

        memoryshare.openMemoryShare("MemoriaDat", 2);
        memoryshare.openMemoryShare("Enable", 1);

        memoryshare.SetInt("Enable", 0, 1);
    }

    // Update is called once per frame 
    void Update()
    {
        if (!DataInfo.dataInfo.singlePractice)
        {
            PV = GetComponent<PhotonView>();
            if (PV.IsMine)
            {
                Calculate();
            }
        }
        else
        {
            Calculate();
        }

    }

    private void Calculate()
    {
        hxd = trayectoria_d.transform.position.x;
        hyd = trayectoria_d.transform.position.y;
        hzd = trayectoria_d.transform.position.z;

        memoryshare.Setfloat("MemoriaDat", hxd, 0);
        memoryshare.Setfloat("MemoriaDat", hyd, 1);
        memoryshare.Setfloat("MemoriaDat", hzd, 2);
    }
}
