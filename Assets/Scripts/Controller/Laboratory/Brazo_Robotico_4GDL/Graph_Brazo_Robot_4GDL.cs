﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ChartAndGraph;
using Photon.Pun;

public class Graph_Brazo_Robot_4GDL : MonoBehaviour
{
    public GraphChart Graph;
    public int TotalPoints = 5;
    float lastTime = 0f;
    float lastX = 0f;
    public Control_posicion_soldador he;
    public Control_trayectoria_soldador he2;
    public GameObject RobotSoldador;

    [SerializeField]
    private PhotonView PV;
    void Start()
    {
        if (!DataInfo.dataInfo.singlePractice)
        {
            PV = GetComponent<PhotonView>();
            Inicializate();
        }
        else
        {
            Inicializate();
        }
    }

    void Update()
    {
        if (DataInfo.dataInfo.singlePractice)
        {
            SingleGraph();
        }
    }
    private void Inicializate()
    {
        if (Graph == null) // the ChartGraph info is obtained via the inspector
            return;
        Graph.DataSource.StartBatch(); // calling StartBatch allows changing the graph data without redrawing the graph for every change
        Graph.DataSource.ClearCategory("hxe"); // clear the "Player 1" category. this category is defined using the GraphChart inspector
        Graph.DataSource.ClearCategory("hye"); // clear the "Player 2" category. this category is defined using the GraphChart inspector
        Graph.DataSource.ClearCategory("hze"); // clear the "Player 2" category. this category is defined using the GraphChart inspector
        Graph.DataSource.EndBatch(); // finally we call EndBatch , this will cause the GraphChart to redraw itself
    }

    private void SingleGraph()
    {
        float time = Time.time;
        if (lastTime + 0.1f < time)
        {
            lastTime = time;
            //            System.DateTime t = ChartDateUtility.ValueToDate(lastX);
            if (RobotSoldador.gameObject.GetComponent<Control_posicion_soldador>().enabled)
            {
                Graph.DataSource.AddPointToCategoryRealtime("hxe", System.DateTime.Now, he.hex, 1f); // each time we call AddPointToCategory 
                Graph.DataSource.AddPointToCategoryRealtime("hye", System.DateTime.Now, he.hey, 1f); // each time we call AddPointToCategory 
                Graph.DataSource.AddPointToCategoryRealtime("hze", System.DateTime.Now, he.hez, 1f); // each time we call AddPointToCategory 
                Debug.Log("Errores 1");

            }
            else if (RobotSoldador.gameObject.GetComponent<Control_trayectoria_soldador>().enabled)
            {
                Graph.DataSource.AddPointToCategoryRealtime("hxe", System.DateTime.Now, he2.hex, 1f); // each time we call AddPointToCategory 
                Graph.DataSource.AddPointToCategoryRealtime("hye", System.DateTime.Now, he2.hey, 1f); // each time we call AddPointToCategory 
                Graph.DataSource.AddPointToCategoryRealtime("hze", System.DateTime.Now, he2.hez, 1f); // each time we call AddPointToCategory 
                Debug.Log("Errores 2");
            }
        }
    }

    private void MultiGraph(float hex2,float hey2,float hez2)
    {
        float time = Time.time;
        if (lastTime + 0.1f < time)
        {
            lastTime = time;
            //            System.DateTime t = ChartDateUtility.ValueToDate(lastX);
             if (RobotSoldador.gameObject.GetComponent<Control_trayectoria_soldador>().enabled)
            {
                Graph.DataSource.AddPointToCategoryRealtime("hxe", System.DateTime.Now, hex2, 1f); // each time we call AddPointToCategory 
                Graph.DataSource.AddPointToCategoryRealtime("hye", System.DateTime.Now, hey2, 1f); // each time we call AddPointToCategory 
                Graph.DataSource.AddPointToCategoryRealtime("hze", System.DateTime.Now, hez2, 1f); // each time we call AddPointToCategory 
                Debug.Log("Errores 2");
            }
        }
    }
}
