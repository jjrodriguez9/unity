﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;


    public class DesactivaCamara : MonoBehaviour
{
    // Start is called before the first frame update

    bool selector;

    void Start()
    {
        selector = true;
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    void FixedUpdate()
    {
        if (Input.GetKeyDown(KeyCode.LeftAlt) && selector)
        {
            gameObject.GetComponent<FirstPersonController>().enabled = false;
            selector = false;

        }else if (Input.GetKeyDown(KeyCode.LeftAlt) && !selector)
        {
            gameObject.GetComponent<FirstPersonController>().enabled = true;
            selector = true;

        }







    }



}
