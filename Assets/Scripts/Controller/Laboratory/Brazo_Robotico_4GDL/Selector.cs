﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Selector : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        //gameObject.GetComponent<Control_posicion_soldador>().enabled = false;
        //gameObject.GetComponent<Control_trayectoria_soldador>().enabled = false;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            gameObject.GetComponent<Control_posicion_soldador>().enabled = false;
            gameObject.GetComponent<Control_trayectoria_soldador>().enabled = true;
            Debug.Log("Activa Trayectoria");

        }
        else if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            gameObject.GetComponent<Control_posicion_soldador>().enabled = true;
            gameObject.GetComponent<Control_trayectoria_soldador>().enabled = false;
            Debug.Log("Activa Posicion");

        }
    }

    public void Trayectoria()
    {
        gameObject.GetComponent<Control_posicion_soldador>().enabled = false;
        gameObject.GetComponent<Control_trayectoria_soldador>().enabled = true;
        Debug.Log("Activa Trayectoria");
    }

    public void Posicion()
    {
        gameObject.GetComponent<Control_posicion_soldador>().enabled = true;
        gameObject.GetComponent<Control_trayectoria_soldador>().enabled = false;
        Debug.Log("Activa Posicion");
    }
}
