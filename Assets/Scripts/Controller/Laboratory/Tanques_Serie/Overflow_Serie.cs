﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Overflow_Serie : MonoBehaviour
{
    [SerializeField]
    private Setting_Serie seting;

    [SerializeField]
    private Explotion_Serie1 serie1;

    [SerializeField]
    private Explotion_Serie2 serie2;

    [SerializeField]
    private Explotion_Serie3 serie3;

    void Start()
    {
        serie1.activefluido(false);
        serie2.activefluido(false);
        serie3.activefluido(false);
    }


    void Update()
    {
        if (seting._level_one >= 4f) { serie1.activefluido(true); } else { serie1.activefluido(false); }
        if (seting._level_two >= 4f) { serie2.activefluido(true); } else { serie2.activefluido(false); }
        if (seting._level_three >= 4f) { serie3.activefluido(true); } else { serie3.activefluido(false); }
    }
}
