﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Efects_Serie : MonoBehaviour
{
    [SerializeField]
    private Setting_Serie seting;

    [SerializeField] private GameObject efecto, efecto1;
    private ParticleSystem particle, particle1;
    private Vector3 scale, scale1;

    void Start()
    {
        seting = GameObject.Find("Setting").GetComponent<Setting_Serie>();

        efecto.gameObject.SetActive(true);
        efecto.GetComponent<ParticleSystem>().Play(true);
        scale = efecto.gameObject.transform.localScale;
        scale.x = 0f;
        scale.y = 0f;
        efecto.gameObject.transform.localScale = scale;


        efecto1.gameObject.SetActive(true);
        efecto1.GetComponent<ParticleSystem>().Play(true);
        scale1 = efecto.gameObject.transform.localScale;
        scale1.x = 0f;
        scale1.y = 0f;
        efecto1.gameObject.transform.localScale = scale1;


    }


    void Update()
    {
        efecto.gameObject.SetActive(true);
        efecto.GetComponent<ParticleSystem>().Play(true);
        scale.x = 0.5f * seting._valve_v3 * 100f / 100f;
        scale.y = scale.x;
        efecto.gameObject.transform.localScale = scale;

        efecto1.gameObject.SetActive(true);
        efecto1.GetComponent<ParticleSystem>().Play(true);
        scale1.x = 0.5f * seting._valve_v4 * 100f / 100f;
        scale1.y = scale1.x;
        efecto1.gameObject.transform.localScale = scale1;

    }
}
