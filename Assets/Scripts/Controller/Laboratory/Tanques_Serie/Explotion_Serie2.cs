﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explotion_Serie2 : MonoBehaviour
{
    [SerializeField]
    private ParticleSystem EfectoFluido;

    void Start()
    {

        Inicializate();

    }


    private void Inicializate()
    {
        gameObject.SetActive(false);
        EfectoFluido = GetComponent<ParticleSystem>();
    }


    public void activefluido(bool active)
    {
        if (active)
        {
            EfectoFluido.gameObject.SetActive(active);
            EfectoFluido.Play(active);
        }
        else
        {
            gameObject.SetActive(false);
            EfectoFluido.gameObject.SetActive(false);
            EfectoFluido.Play(false);
        }

    }


}
