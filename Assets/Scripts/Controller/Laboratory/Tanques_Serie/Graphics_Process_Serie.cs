﻿using ChartAndGraph;
using UnityEngine;
using Photon.Pun;
public class Graphics_Process_Serie : MonoBehaviour
{
    public GraphChart[] Graph;
    float lastTime = 0f;
    private float X = 0f;
    private float ts = 0.1f;
    // Start is called before the first frame update
    [SerializeField]
    private Setting_Serie seting;
    [SerializeField]
    private PhotonView PV;

    void Start()
    {
        if (!DataInfo.dataInfo.singlePractice)
        {
            PV = GetComponent<PhotonView>();
            Inicializate();
        }
        else
        {
            Inicializate();
        }
    }

    public void Inicializate()
    {
        seting = GameObject.Find("Setting").GetComponent<Setting_Serie>();

        Graph[0].DataSource.HorizontalViewSize = 40f;
        Graph[0].DataSource.StartBatch();
        Graph[0].DataSource.ClearCategory("SP TANK ONE");
        Graph[0].DataSource.ClearCategory("PV TANK ONE");
        Graph[0].DataSource.EndBatch();

        Graph[1].DataSource.HorizontalViewSize = 40f;
        Graph[1].DataSource.StartBatch();
        Graph[1].DataSource.ClearCategory("SP TANK THREE");
        Graph[1].DataSource.ClearCategory("PV TANK THREE");
        Graph[1].DataSource.EndBatch();

        Graph[2].DataSource.HorizontalViewSize = 40f;
        Graph[2].DataSource.StartBatch();
        Graph[2].DataSource.ClearCategory("V1");
        Graph[2].DataSource.ClearCategory("V2");
        Graph[2].DataSource.ClearCategory("V3");
        Graph[2].DataSource.ClearCategory("V4");
        Graph[2].DataSource.EndBatch();
    }

    void Update()
    {
        if (DataInfo.dataInfo.singlePractice)
        {
            SingleGraphics();
        }
        

    }

    private void SingleGraphics()
    {
        float time = Time.time;
        if (lastTime + ts * 2 < time)
        {
            lastTime = time;

            Graph[0].DataSource.AddPointToCategoryRealtime("SP TANK ONE", System.DateTime.Now, seting._level_one_d, 1f);
            Graph[0].DataSource.AddPointToCategoryRealtime("PV TANK ONE", System.DateTime.Now, seting._level_one, 1f);

            Graph[1].DataSource.AddPointToCategoryRealtime("SP TANK THREE", System.DateTime.Now, seting._level_three_d, 1f);
            Graph[1].DataSource.AddPointToCategoryRealtime("PV TANK THREE", System.DateTime.Now, seting._level_three, 1f);

            Graph[2].DataSource.AddPointToCategoryRealtime("V1", System.DateTime.Now, seting._valve_v1 * 100f, 1f);
            Graph[2].DataSource.AddPointToCategoryRealtime("V2", System.DateTime.Now, seting._valve_v2 * 100f, 1f);
            Graph[2].DataSource.AddPointToCategoryRealtime("V3", System.DateTime.Now, seting._valve_v3 * 100f, 1f);
            Graph[2].DataSource.AddPointToCategoryRealtime("V4", System.DateTime.Now, seting._valve_v4 * 100f, 1f);


        }
    }

    [PunRPC]
    private void MultiGraphics(float level_one,float level_one_d,float level_three,float level_three_d,float valve_v1,float valve_v2,float valve_v3,float valve_v4)
    {
        float time = Time.time;
        if (lastTime + ts * 2 < time)
        {
            lastTime = time;

            Graph[0].DataSource.AddPointToCategoryRealtime("SP TANK ONE", System.DateTime.Now, level_one_d, 1f);
            Graph[0].DataSource.AddPointToCategoryRealtime("PV TANK ONE", System.DateTime.Now, level_one, 1f);

            Graph[1].DataSource.AddPointToCategoryRealtime("SP TANK THREE", System.DateTime.Now, level_three_d, 1f);
            Graph[1].DataSource.AddPointToCategoryRealtime("PV TANK THREE", System.DateTime.Now, level_three, 1f);

            Graph[2].DataSource.AddPointToCategoryRealtime("V1", System.DateTime.Now, valve_v1 * 100f, 1f);
            Graph[2].DataSource.AddPointToCategoryRealtime("V2", System.DateTime.Now, valve_v2 * 100f, 1f);
            Graph[2].DataSource.AddPointToCategoryRealtime("V3", System.DateTime.Now, valve_v3 * 100f, 1f);
            Graph[2].DataSource.AddPointToCategoryRealtime("V4", System.DateTime.Now, valve_v4 * 100f, 1f);


        }
    }
}
