﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Setting_Serie : MonoBehaviour
{
    public static Setting_Serie setting;

    private float level_one_d, level_one, level_one_e;
    private float level_two_d, level_two, level_two_e;
    private float level_three_d, level_three, level_three_e;
    private float valve_v1, valve_v2, valve_v3, valve_v4;

    private float w1, w2, w3;

    private float timeX;
    private bool f1, f2, f3, f4, f5, f6, f7, f8;


    [SerializeField] private double containerVolume1;
    public double _containerVolume1 { set { this.containerVolume1 = value; } get { return containerVolume1; } }
    private double volumen1;
    public double _volumen1 { set { this.volumen1 = value; } get { return volumen1; } }

    [SerializeField] private double containerVolume2;
    public double _containerVolume2 { set { this.containerVolume2 = value; } get { return containerVolume2; } }
    private double volumen2;
    public double _volumen2 { set { this.volumen2 = value; } get { return volumen2; } }

    [SerializeField] private double containerVolume3;
    public double _containerVolume3 { set { this.containerVolume3 = value; } get { return containerVolume3; } }
    private double volumen3;
    public double _volumen3 { set { this.volumen3 = value; } get { return volumen3; } }

    public float _level_one_d { set { this.level_one_d = value; } get { return level_one_d; } }
    public float _level_one { set { this.level_one = value; } get { return level_one; } }
    public float _level_one_e { set { this.level_one_e = value; } get { return level_one_e; } }

    public float _level_two_d { set { this.level_two_d = value; } get { return level_two_d; } }
    public float _level_two { set { this.level_two = value; } get { return level_two; } }
    public float _level_two_e { set { this.level_two_e = value; } get { return level_two_e; } }

    public float _level_three_d { set { this.level_three_d = value; } get { return level_three_d; } }
    public float _level_three { set { this.level_three = value; } get { return level_three; } }
    public float _level_three_e { set { this.level_three_e = value; } get { return level_three_e; } }

    public float _valve_v1 { set { this.valve_v1 = value; } get { return valve_v1; } }
    public float _valve_v2 { set { this.valve_v2 = value; } get { return valve_v2; } }
    public float _valve_v3 { set { this.valve_v3 = value; } get { return valve_v3; } }
    public float _valve_v4 { set { this.valve_v4 = value; } get { return valve_v4; } }

    public float _w1 { set { this.w1 = value; } get { return w1; } }
    public float _w2 { set { this.w2 = value; } get { return w2; } }
    public float _w3 { set { this.w3 = value; } get { return w3; } }


    public float _time { set { this.timeX = value; } get { return timeX; } }

    public bool _f1 { set { this.f1 = value; } get { return f1; } }
    public bool _f2 { set { this.f2 = value; } get { return f2; } }
    public bool _f3 { set { this.f3 = value; } get { return f3; } }
    public bool _f4 { set { this.f4 = value; } get { return f4; } }
    public bool _f5 { set { this.f5 = value; } get { return f5; } }
    public bool _f6 { set { this.f6 = value; } get { return f6; } }
    public bool _f7 { set { this.f7 = value; } get { return f7; } }
    public bool _f8 { set { this.f8 = value; } get { return f8; } }

    private void Awake()
    {
        if (Setting_Serie.setting == null)
        {
            Setting_Serie.setting = this;
        }
        else
        {
            if (Setting_Serie.setting != this)
            {
                Destroy(this.gameObject);
            }
        }
        DontDestroyOnLoad(this.gameObject);
    }
}


