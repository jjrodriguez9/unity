﻿
using UnityEngine;
using TMPro;
using Photon.Pun;

public class Interface_Serie : MonoBehaviour
{
    [SerializeField] private TMP_Text[] METODS_T;

    [SerializeField]
    private Setting_Serie seting;

    [SerializeField]
    private PhotonView PV;

    void Start()
    {
        Inicializate();
        if (!DataInfo.dataInfo.singlePractice)
        {
            PV = GetComponent<PhotonView>();
            Inicializate();
        }
        else
        {
            Inicializate();
        }

    }



    private void Inicializate() {
        seting = GameObject.Find("Setting").GetComponent<Setting_Serie>();
        seting._level_one_d = 1.50f;
        seting._level_three_d = 2.50f;
        seting._valve_v1 = 0.214f;
        seting._valve_v2 = 0.184f;


        for (int i = 0; i < METODS_T.Length; i++)
        {
            METODS_T[i].text = System.Convert.ToString(0);
        }
    }


    void Update()
    {

       if (!DataInfo.dataInfo.singlePractice)
        {
            if (PV.IsMine)
            {
                V_F3();
                CambioPantalla();
            }
        }
        else
        {
            V_F3();
            CambioPantalla();
        }
    }


    private void V_F3()
    {

        METODS_T[0].text = System.Convert.ToString(System.Math.Round(seting._level_one_d, 3));
        METODS_T[1].text = System.Convert.ToString(System.Math.Round(seting._level_one, 3));
        METODS_T[2].text = System.Convert.ToString(System.Math.Round(seting._level_one_e, 3));
        METODS_T[3].text = System.Convert.ToString(System.Math.Round(seting._level_two, 3));
        METODS_T[4].text = System.Convert.ToString(System.Math.Round(seting._valve_v1 * 100f, 3));
        METODS_T[5].text = System.Convert.ToString(System.Math.Round(seting._valve_v2 * 100f, 3));
        METODS_T[6].text = System.Convert.ToString(System.Math.Round(seting._valve_v3 * 100f, 3));
        METODS_T[7].text = System.Convert.ToString(System.Math.Round(seting._valve_v4 * 100f, 3));
        METODS_T[8].text = System.Convert.ToString(System.Math.Round(seting._level_three_d, 3));
        METODS_T[9].text = System.Convert.ToString(System.Math.Round(seting._level_three, 3));
        METODS_T[10].text = System.Convert.ToString(System.Math.Round(seting._level_three_e, 3));

    }

    [PunRPC]
    private void Multi_V_F3(float level_one,float level_one_d,float level_one_e,float level_two,float level_three,float level_three_d,float level_three_e,float valve_v1,float valve_v2,float valve_v3,float valve_v4)
    {

        METODS_T[0].text = System.Convert.ToString(System.Math.Round(level_one_d, 3));
        METODS_T[1].text = System.Convert.ToString(System.Math.Round(level_one, 3));
        METODS_T[2].text = System.Convert.ToString(System.Math.Round(level_one_e, 3));
        METODS_T[3].text = System.Convert.ToString(System.Math.Round(level_two, 3));
        METODS_T[4].text = System.Convert.ToString(System.Math.Round(valve_v1 * 100f, 3));
        METODS_T[5].text = System.Convert.ToString(System.Math.Round(valve_v2 * 100f, 3));
        METODS_T[6].text = System.Convert.ToString(System.Math.Round(valve_v3 * 100f, 3));
        METODS_T[7].text = System.Convert.ToString(System.Math.Round(valve_v4 * 100f, 3));
        METODS_T[8].text = System.Convert.ToString(System.Math.Round(level_three_d, 3));
        METODS_T[9].text = System.Convert.ToString(System.Math.Round(level_three, 3));
        METODS_T[10].text = System.Convert.ToString(System.Math.Round(level_three_e, 3));

    }
    public void CambioPantalla()
    {
        bool active = Input.GetButtonDown("active_create");
        if (active)
        {
            Debug.Log(seting._f1);
            if (seting._f1)
            {
                seting._f1 = false;
                seting._f2 = true;
            }
            else
            {
                seting._f1 = true;
                seting._f2 = false;
            }
        }
    }
}
