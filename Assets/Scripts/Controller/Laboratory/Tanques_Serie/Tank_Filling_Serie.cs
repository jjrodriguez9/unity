﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tank_Filling_Serie : MonoBehaviour
{
    [SerializeField] private GameObject agua1;
    [SerializeField] private GameObject agua2;
    [SerializeField] private GameObject agua3;
    private Vector3 position1;
    private Vector3 position2;
    private Vector3 position3;
    [SerializeField] private double vel1;
    [SerializeField] private double vel2;
    [SerializeField] private double vel3;

    private double volumen1, porcentaje1, altura1;
    private double volumen2, porcentaje2, altura2;
    private double volumen3, porcentaje3, altura3;

    [SerializeField]
    private Setting_Serie seting;

    [SerializeField]
    private PhotonView PV;

    void Start()
    {
        if (!DataInfo.dataInfo.singlePractice)
        {
            PV = GetComponent<PhotonView>();
            Inicializate();
        }
        else
        {
            Inicializate();
        }
    }

    private void Inicializate()
    {
        seting = GameObject.Find("Setting").GetComponent<Setting_Serie>();
        FILL_TANK_ONE();
    }

    void Update()
    {

        if (!DataInfo.dataInfo.singlePractice)
        {
            if (PV.IsMine)
            {
                FILL_TANK_ONE();
                FILL_TANK_TWO();
                FILL_TANK_THREE();
            }
        }
        else
        {
            FILL_TANK_ONE();
            FILL_TANK_TWO();
            FILL_TANK_THREE();
        }
        
    }

    private void FILL_TANK_ONE()
    {
        float model = (1 - (float)((5307 * Mathf.Exp((float)-(125 * 1) / 193)) / 7720));

        if (seting._level_one >= 2.88f) { 
            seting._volumen1 = 2.88f * model * 5000; 
        } else { 
            seting._volumen1 = seting._level_one * model * 5000; 
        }

        volumen1 = seting._volumen1;
        porcentaje1 = (volumen1 * 100) / seting._containerVolume1;
        altura1 = (porcentaje1 * 0.0485f) / 100;

        float scala = System.Convert.ToSingle(position1.y + altura1 * vel1);
        agua1.transform.localScale = new Vector3(agua1.transform.localScale.x, scala, agua1.transform.localScale.z);
        float traz = System.Convert.ToSingle(position1.z + altura1 * vel1);
        agua1.transform.localPosition = new Vector3(position1.x, position1.y, traz);
    }


    private void FILL_TANK_TWO()
    {
        float model = (1 - (float)((5307 * Mathf.Exp((float)-(125 * 1) / 193)) / 7720));

        if (seting._level_two >= 2.88f) { 
            seting._volumen2 = 2.88f * model * 5000; 
        } else { 
            seting._volumen2 = seting._level_two * model * 5000; 
        }

        volumen2 = seting._volumen2;
        porcentaje2 = (volumen2 * 100) / seting._containerVolume2;
        altura2 = (porcentaje2 * 0.0485f) / 100;

        float scala = System.Convert.ToSingle(position2.y + altura2 * vel2);
        agua2.transform.localScale = new Vector3(agua2.transform.localScale.x, scala, agua2.transform.localScale.z);
        float traz = System.Convert.ToSingle(position2.z + altura2 * vel2);
        agua2.transform.localPosition = new Vector3(position2.x, position2.y, traz);
    }

    private void FILL_TANK_THREE()
    {
        float model = (1 - (float)((5307 * Mathf.Exp((float)-(125 * 1) / 193)) / 7720));

        if (seting._level_three >= 2.88f) {
            seting._volumen3 = 2.88f * model * 5000; 
        } else {
            seting._volumen3 = seting._level_three * model * 5000; 
        }

        volumen3 = seting._volumen3;
        porcentaje3 = (volumen3 * 100) / seting._containerVolume3;
        altura3 = (porcentaje3 * 0.0485f) / 100;

        float scala = System.Convert.ToSingle(position3.y + altura3 * vel3);
        agua3.transform.localScale = new Vector3(agua3.transform.localScale.x, scala, agua3.transform.localScale.z);
        float traz = System.Convert.ToSingle(position3.z + altura3 * vel3);
        agua3.transform.localPosition = new Vector3(position3.x, position3.y, traz);
    }
}
