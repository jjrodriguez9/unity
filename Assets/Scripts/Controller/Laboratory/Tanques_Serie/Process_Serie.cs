﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Runtime.InteropServices;
using Photon.Pun;

public class Process_Serie : MonoBehaviour
{
    private float ts = 0.1f;
    private float To;
    private float time;

    private Memoryshare memoryshare;

    [SerializeField]
    private Setting_Serie seting;

    [SerializeField]
    private PhotonView PV;

    void Start()
    {
        if (!DataInfo.dataInfo.singlePractice)
        {
            PV = GetComponent<PhotonView>();
            if (PV.IsMine)
            {
                Inicializate();
            }
        }
        else
        {
            Inicializate();
        }

    }

    private void Inicializate()
    {
        seting = GameObject.Find("Setting").GetComponent<Setting_Serie>();

        #region MEMORIAS COMPARTIDAS
        memoryshare = new Memoryshare();
        memoryshare.freeMemoriShare();

        memoryshare.createMemoriShare("UNITY", 9, 2);
        memoryshare.createMemoriShare("MATLAB", 2, 2);
        memoryshare.createMemoriShare("ENABLE", 2, 1);

        memoryshare.openMemoryShare("UNITY", 2);
        memoryshare.openMemoryShare("MATLAB", 2);
        memoryshare.openMemoryShare("ENABLE", 1);
        #endregion

        for (int i = 0; i < 8; i++)
        {
            memoryshare.Setfloat("UNITY", 0f, i);
            memoryshare.Setfloat("MATLAB", 0f, i);
        }

        memoryshare.SetInt("ENABLE", 1, 0);
        memoryshare.SetInt("ENABLE", 0, 1);
    }

    void Update()
    {
        if (!DataInfo.dataInfo.singlePractice)
        {
            if (PV.IsMine)
            {
                Calculate();
            }
        }
        else
        {
            Calculate();
        }

    }

    private void Calculate()
    {
        To += Time.deltaTime;

        if (To >= ts)
        {
            time += Time.deltaTime;
            seting._time = time;

            if (seting._f1)
            {
                CONTROLLER_METODOS();
            }

            if (seting._f2)
            {
                memoryshare.SetInt("ENABLE", 0, 1);
            }

            To = 0f;
            // Debug.Log(Setting.setting._f1);
        }
    }

    private void CONTROLLER_METODOS()
    {

        memoryshare.SetInt("ENABLE", 1, 1);

        seting._level_one_e = seting._level_one_d - seting._level_one;
        seting._level_three_e = seting._level_three_d - seting._level_three;

        memoryshare.Setfloat("UNITY", seting._level_one_e, 0);
        memoryshare.Setfloat("UNITY", seting._level_three_e, 1);
        memoryshare.Setfloat("UNITY", seting._level_one_d, 2);
        memoryshare.Setfloat("UNITY", seting._level_three_d, 3);
        memoryshare.Setfloat("UNITY", seting._level_one, 4);
        memoryshare.Setfloat("UNITY", seting._level_two, 5);
        memoryshare.Setfloat("UNITY", seting._level_three, 6);
        memoryshare.Setfloat("UNITY", seting._valve_v1, 7);
        memoryshare.Setfloat("UNITY", seting._valve_v2, 8);


        seting._valve_v3 = memoryshare.Getfloat("MATLAB", 0);
        seting._valve_v4 = memoryshare.Getfloat("MATLAB", 1);

        MATH_MODEL();

    }


    private void MATH_MODEL()
    {
        float A1 = 2f;
        float A2 = 2f;
        float A3 = 2f;

        float k1 = 0.108f;
        float k2 = 0.105f;
        float k3 = 0.415f;
        float k4 = 0.420f;


        float g = 9.81f;

        float S1 = 0.061f;
        float S2 = 0.051f;

        float h1 = seting._level_one;
        float h2 = seting._level_two;
        float h3 = seting._level_three;

        float V1 = seting._valve_v1;
        float V2 = seting._valve_v2;

        float V3 = seting._valve_v3;
        float V4 = seting._valve_v4;

        float hp1 = ((k3 * V3) - (S1 * Mathf.Sqrt(2 * g * h1)) + (S2 * Mathf.Sqrt(2 * g * Mathf.Abs(h2 - h1)) * Mathf.Sign(h2 - h1))) / A1;
        float hp2 = (-(S2 * Mathf.Sqrt(2 * g * Mathf.Abs(h2 - h1)) * Mathf.Sign(h2 - h1)) + (k1 * V1 * Mathf.Sqrt(2 * g * Mathf.Abs(h3 - h2)) * Mathf.Sign(h3 - h2))) / A2;
        float hp3 = (k4 * V4 - (k2 * V2 * Mathf.Sqrt(2 * g * h3)) - (k1 * V1 * Mathf.Sqrt(2 * g * Mathf.Abs(h3 - h2)) * Mathf.Sign(h3 - h2))) / A3;


        float q1 = (S1 * Mathf.Sqrt(2 * g * h1)) / A1;
        float q2 = ((S2 * Mathf.Sqrt(2 * g * Mathf.Abs(h2 - h1)) * Mathf.Sign(h2 - h1))) / A1;
        float q3 = ((k1 * V1 * Mathf.Sqrt(2 * g * Mathf.Abs(h3 - h2)) * Mathf.Sign(h3 - h2))) / A2;
        float q4 = (k2 * V2 * Mathf.Sqrt(2 * g * h3)) / A3;

        h1 = hp1 * ts + h1;
        h2 = hp2 * ts + h2;
        h3 = hp3 * ts + h3;

        if (h1 >= 4f) { h1 = 4f; }
        if (h2 >= 4f) { h2 = 4f; }
        if (h3 >= 4f) { h3 = 4f; }

        seting._level_one = h1;
        seting._level_two = h2;
        seting._level_three = h3;

        seting._valve_v1 = V1;
        seting._valve_v2 = V2;

        seting._valve_v3 = V3;
        seting._valve_v4 = V4;

        if (!DataInfo.dataInfo.singlePractice)
        {
            PV.RPC("MultiGraphics", RpcTarget.All, h1, seting._level_one_d, h3, seting._level_three_d, V1, V2, V3, V4);
            PV.RPC("Multi_V_F3", RpcTarget.All, h1, seting._level_one_d, seting._level_one_e, h2, h3, seting._level_three_d, seting._level_three_e, V1, V2, V3, V4);
        }
    }


    void OnApplicationQuit()
    {

        memoryshare.SetInt("ENABLE", 0, 0);

    }
}
