﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Panel_Controller_Serie : MonoBehaviour
{
    [SerializeField]
    private Setting_Serie seting;

    [SerializeField]
    private PhotonView PV;

    private void Start()
    {
        seting = GameObject.Find("Setting").GetComponent<Setting_Serie>();
    }
    private void OnTriggerStay(Collider other)
    {
        if (!DataInfo.dataInfo.singlePractice)
        {
            PV = GetComponent<PhotonView>();
            if (PV.IsMine)
            {
                METODOS(other.tag);
                //Debug.Log(other.tag);
            }
        }
        else
        {
            METODOS(other.tag);
            //Debug.Log(other.tag);
        }
        
    }

    private void METODOS(string tag)
    {
        if (tag.Equals("Player"))
        {
            float value = Input.GetAxis("slider_move");
            float value1 = Input.GetAxis("active_x");
            float value2 = Input.GetAxis("active_y");
            float value3 = Input.GetAxis("active_z");
            tank1(value);
            tank2(value1);
            Val1(value2);
            Val2(value3);
        }
    }
    private void tank1(float value)
    {
        if (value < 0)
        {
            if (seting._level_one_d <= 0f)
            {
                seting._level_one_d = 0f;
            }
            else
            {
                seting._level_one_d -= 0.0001f;
            }
        }
        else if (value > 0)
        {
            if (seting._level_one_d >= 4f)
            {
                seting._level_one_d = 4f;
            }
            else
            {
                seting._level_one_d += 0.0001f;
            }
        }
    }

    private void tank2(float value)
    {
        if (value < 0)
        {
            if (seting._level_three_d <= 0f)
            {
                seting._level_three_d = 0f;
            }
            else
            {
                seting._level_three_d -= 0.0001f;
            }
        }
        else if (value > 0)
        {
            if (seting._level_three_d >= 4f)
            {
                seting._level_three_d = 4f;
            }
            else
            {
                seting._level_three_d += 0.0001f;
            }
        }
    }
    private void Val1(float value)
    {
        Debug.Log("value1: "+value);
        if (value < 0)
        {
            if (seting._valve_v1 <= 0f)
            {
                seting._valve_v1 = 0f;
            }
            else
            {
                seting._valve_v1 -= 0.001f;
            }
        }
        else if (value > 0)
        {
            if (seting._valve_v1 >= 1f)
            {
                seting._valve_v1 = 1f;
            }
            else
            {
                seting._valve_v1 += 0.001f;
            }
        }
    }

    private void Val2(float value)
    {
        if (value < 0)
        {
            if (seting._valve_v2 <= 0f)
            {
                seting._valve_v2 = 0f;
            }
            else
            {
                seting._valve_v2 -= 0.001f;
            }
        }
        else if (value > 0)
        {
            if (seting._valve_v2 >= 1f)
            {
                seting._valve_v2 = 1f;
            }
            else
            {
                seting._valve_v2 += 0.001f;
            }
        }
    }
}
