﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.ExceptionServices;
using UnityEngine;
using System;
using System.Runtime.InteropServices;
using Photon.Pun;

public class Modelo : MonoBehaviour
{
    private float h1_p;           //Proceso - Diferencial de altura del agua en el tanque
    private float h2_p;
    private float h3_p;
    private float h4_p;
    [SerializeField]
    private float h1;             //Altura del agua en el tanque
    [SerializeField]
    private float h2;
    [SerializeField]
    private float h3;
    [SerializeField]
    private float h4;

    private float k1;
    private float a1;
    private float a2;
    private float a3;
    private float a4;
    private float a13;
    [SerializeField]
    private float mva;
    [SerializeField]
    private float mvb;
    [SerializeField]
    private float mvc;
    [SerializeField]
    private float mvd;
    [SerializeField]
    private float mve;
    [SerializeField]
    private float mvf;
    private float f1;
    private float f2;

    private float A1;             //Area de tanque
    private float g;             //Constante de gravedad
    private float To;            //Tiempo de muestro
    private float TimeSample;    //Referencia del tiempo de muestro
    [SerializeField]
    private float u1;            //Apertura de valvula 1(Cv - Variable controlada)
    [SerializeField]
    private float u2;
    private PhotonView PV;

    private Memoryshare memoryshare;
    //const string dllPath = "smClient64.dll";

    ////Funcion para abrir la memoria compartida
    //[DllImport(dllPath)]
    //static extern int openMemory(String name, int type);

    ////Funcion para escribir flotantes en la memoria compartida
    //[DllImport(dllPath)]
    //static extern void setFloat(String memName, int position, float value);

    ////Funcion para leer flotantes en la memoria compartida
    //[DllImport(dllPath)]
    //static extern float getFloat(String memName, int position);

    // Inicializamos todas las varibales y la memoria compartida
    void Start()
    {
        if (!DataInfo.dataInfo.singlePractice)
        {
            PV = GetComponent<PhotonView>();
            if (PV.IsMine)
            {
                Inicializate();
            }
        }
        else
        {
            Inicializate();
        }
    }
    private void Inicializate()
    {
        //Abrimos memorias necesarias
        memoryshare = new Memoryshare();
        memoryshare.createMemoriShare("Voltajes", 4, 2);
        memoryshare.createMemoriShare("Alturas", 4, 2);
        memoryshare.openMemoryShare("Voltajes", 2);
        memoryshare.openMemoryShare("Alturas", 2);
        for (int i = 0; i < 4; i++)
        {
            memoryshare.Setfloat("Voltajes", 0f, i);
            memoryshare.Setfloat("Alturas", 0f, i);
        }
        //Inicializamos las varibles
        a1 = 0.30f;
        a2 = 0.60f;
        a3 = 0.50f;
        a4 = 0.80f;
        a13 = 0.30f;
        mva = 0.6f;
        mvb = 0.2f;
        mvc = 0.2f;
        mvd = 0.8f;
        mve = 0.2f;
        mvf = 0.2f;
        k1 = 0.9375f;
        A1 = 100.0f;           //Metros cuadrados
        g = 9.81f;          //Metros por segundo al cuadrado
        To = 0.01f;          //Segundos
        TimeSample = 0.001f;  //Segundos
        h1 = 0;              //Metros
        h2 = 0;
        h3 = 0;
        h4 = 0;
        f1 = 16.667f;
        f2 = 10.0f;
    }
    // Update is called once per frame
    void Update()
    {
        To += Time.deltaTime;       //Time.deltaTime = 1/fps, si 60 fps -> Time.deltaTime = 0.01666 = [16 ms]
        if (To >= TimeSample)
        {
            if (!DataInfo.dataInfo.singlePractice)
            {
                if (PV.IsMine)
                {
                    dataUpdate();
                }
            }
            else
            {
                dataUpdate();
            }

            To = 0;
        }
    }
    private void dataUpdate()
    {
        //Recibe el dato de matlab
        u1 = memoryshare.Getfloat("Voltajes", 0);     //Variable controlada
        u2 = memoryshare.Getfloat("Voltajes", 1);     //Variable controlada

        mva = Setting.setting._perturbacion1;
        mvb = Setting.setting._perturbacion2;
        mvc = Setting.setting._perturbacion3;
        mvd = Setting.setting._perturbacion4;
        mve = Setting.setting._perturbacion5;
        mvf = Setting.setting._perturbacion6;

        //**************** MODELO PLANTA NIVEL NO LINEAL ********************

        h1_p = ((k1 * mvb * u1 * f1) + (k1 * mvf * u2 * f1) - (f2 * a1 * Mathf.Sqrt(2 * g * h1)) - (f2 * a13 * Mathf.Sqrt(2 * g * Mathf.Abs(h1 - h3)))) / A1;
        h1 = h1_p * To + h1;

        h2_p = ((f1 * k1 * mva * u2) + (f2 * a1 * Mathf.Sqrt(2 * g * h1)) - (f2 * a2 * Mathf.Sqrt(2 * g * h2))) / A1;
        h2 = h2_p * To + h2;

        h3_p = ((f1 * k1 * mvc * u1) + (f1 * k1 * mve * u2) - (f2 * a3 * Mathf.Sqrt(2 * g * h3)) + (f2 * a13 * Mathf.Sqrt(2 * g * Mathf.Abs(h1 - h3)))) / A1;
        h3 = h3_p * To + h3;

        h4_p = ((f1 * k1 * mvd * u2) + (f2 * a3 * Mathf.Sqrt(2 * g * h3)) - (f2 * a4 * Mathf.Sqrt(2 * g * h4))) / A1;
        h4 = h4_p * To + h4; // Integral por euler

        Setting.setting._volumen1 = h1;
        Setting.setting._volumen2 = h2;
        Setting.setting._volumen3 = h3;
        Setting.setting._volumen4 = h4;


        //*********************************************************
        //Enviamos datos a matlab
        memoryshare.Setfloat("Alturas", h1, 0);
        memoryshare.Setfloat("Alturas", h2, 1);
        memoryshare.Setfloat("Alturas", h3, 2);
        memoryshare.Setfloat("Alturas", h4, 3);
    }
}