﻿
using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class alturas_panel : MonoBehaviour
{
    [SerializeField]
    private Slider[] insp; //declaro el objeto de la interfaz grafica
    private float k1_1;
    private float k1_2;
    private float k1_3;
    private float k1_4;
    private float k1_5;
    private float k1_6;
    [SerializeField]
    private TMP_Text[] label; //Declaro objetos de tipo text para colocar las varibles de nuestras alturas
    [SerializeField]
    private TMP_Text[] label1; //Declaro objetos de tipo text para colocar las varibles de nuestras alturas
    [SerializeField]
    private TMP_Text[] medidaT; //Declaro objetos de tipo text para colocar las varibles de nuestras alturas en los transmisores
    private PhotonView PV;
    void Start()
    {
        for (int i = 0; i < label.Length; i++)
        {
            label[i].text = System.Convert.ToString(0); //asignado el valor de cero a los label para la cual utilizamos la transformacion de propo lenguaje.
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!DataInfo.dataInfo.singlePractice)
        {
            PV.RPC("Asignation", RpcTarget.All);
            PV.RPC("perturbacion", RpcTarget.All);
        }
        else
        {
            Asignation();
            perturbacion();
        }
    }
    [PunRPC]
    private void perturbacion()
    {
        k1_1 = System.Convert.ToSingle(insp[0].value); //procedo a tomar el valor del slider y transformar a la escala del contendor cabe recalcar que los valos del slider va entre 0 y 1
        label1[0].text = System.Convert.ToString(System.Math.Round(k1_1, 2)); //asigno el valor a label haciendo que solo apareca dos decimales
        Setting.setting._perturbacion1 = k1_1;

        k1_2 = System.Convert.ToSingle(insp[1].value); //procedo a tomar el valor del slider y transformar a la escala del contendor cabe recalcar que los valos del slider va entre 0 y 1
        label1[1].text = System.Convert.ToString(System.Math.Round(k1_2, 2)); //asigno el valor a label haciendo que solo apareca dos decimales
        Setting.setting._perturbacion2 = k1_2;

        k1_3 = System.Convert.ToSingle(insp[2].value); //procedo a tomar el valor del slider y transformar a la escala del contendor cabe recalcar que los valos del slider va entre 0 y 1
        label1[2].text = System.Convert.ToString(System.Math.Round(k1_3, 2)); //asigno el valor a label haciendo que solo apareca dos decimales
        Setting.setting._perturbacion3 = k1_3;

        k1_4 = System.Convert.ToSingle(insp[3].value); //procedo a tomar el valor del slider y transformar a la escala del contendor cabe recalcar que los valos del slider va entre 0 y 1
        label1[3].text = System.Convert.ToString(System.Math.Round(k1_4, 2)); //asigno el valor a label haciendo que solo apareca dos decimales
        Setting.setting._perturbacion4 = k1_4;

        k1_5 = System.Convert.ToSingle(insp[4].value); //procedo a tomar el valor del slider y transformar a la escala del contendor cabe recalcar que los valos del slider va entre 0 y 1
        label1[4].text = System.Convert.ToString(System.Math.Round(k1_5, 2)); //asigno el valor a label haciendo que solo apareca dos decimales
        Setting.setting._perturbacion5 = k1_5;

        k1_6 = System.Convert.ToSingle(insp[5].value); //procedo a tomar el valor del slider y transformar a la escala del contendor cabe recalcar que los valos del slider va entre 0 y 1
        label1[5].text = System.Convert.ToString(System.Math.Round(k1_6, 2)); //asigno el valor a label haciendo que solo apareca dos decimales
        Setting.setting._perturbacion6 = k1_6;
    }
    [PunRPC]
    private void Asignation()
    {
        label[0].text = System.Convert.ToString(System.Math.Round(Setting.setting._volumen1, 2)); //asigno el valor a label haciendo que solo apareca dos decimales
        label[1].text = System.Convert.ToString(System.Math.Round(Setting.setting._volumen2, 2));//asigno el valor a label haciendo que solo apareca dos decimales
        label[2].text = System.Convert.ToString(System.Math.Round(Setting.setting._volumen3, 2));//asigno el valor a label haciendo que solo apareca dos decimales
        label[3].text = System.Convert.ToString(System.Math.Round(Setting.setting._volumen4, 2));//asigno el valor a label haciendo que solo apareca dos decimales
        medidaT[0].text = System.Convert.ToString(System.Math.Round(Setting.setting._volumen1, 2));//asigno el valor a label haciendo que solo apareca dos decimales
        medidaT[1].text = System.Convert.ToString(System.Math.Round(Setting.setting._volumen2, 2));//asigno el valor a label haciendo que solo apareca dos decimales
        medidaT[2].text = System.Convert.ToString(System.Math.Round(Setting.setting._volumen3, 2));//asigno el valor a label haciendo que solo apareca dos decimales
        medidaT[3].text = System.Convert.ToString(System.Math.Round(Setting.setting._volumen4, 2));//asigno el valor a label haciendo que solo apareca dos decimales
    }

}
