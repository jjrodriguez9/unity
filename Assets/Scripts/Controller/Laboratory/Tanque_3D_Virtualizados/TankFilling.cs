﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class TankFilling : MonoBehaviourPun
{
    [SerializeField] //directivas que permite visualizar el objeto en se muestre en el inspector pero quede privada al momento de hacer la compilacion.
    private GameObject[] tank;//declracion del objeto que va hacer el recipiente del liquido en el caso de poseer mas de un recipiente se debe colocar de la siguiente manera private GameObject [] tank de esta manera colocamos todos los recipientes en un arreglo y para realizar
    //la activdad dispuesta en los recipientes nos movemos en la posicion del arreglo que contiene el recipiente ejemplo tank[0],tank[1],tank[3] ..... etc
    [SerializeField]
    private Vector3 position1; //declaracion de un vector para saber la posicion del recipiente
    [SerializeField]
    private Vector3 position2; //declaracion de un vector para saber la posicion del recipiente
    [SerializeField]
    private Vector3 position3; //declaracion de un vector para saber la posicion del recipiente
    [SerializeField]
    private Vector3 position4; //declaracion de un vector para saber la posicion del recipiente

    private double percent1; //porcentaje de llenado que se encuentra el recipiente
    private double percent2; //porcentaje de llenado que se encuentra el recipiente
    private double percent3; //porcentaje de llenado que se encuentra el recipiente
    private double percent4; //porcentaje de llenado que se encuentra el recipiente

    private double heigth1; //altura del volumen que esta recibiendo el recipiente
    private double heigth2; //altura del volumen que esta recibiendo el recipiente
    private double heigth3; //altura del volumen que esta recibiendo el recipiente
    private double heigth4; //altura del volumen que esta recibiendo el recipiente

    [SerializeField]
    private double velocity; //es el valor con que velocidad se va realizar la animacion del llenado del recipiente

    [SerializeField]
    private double volumen1; //volumen del liquido que debe ir en el recipiente calculado;
    [SerializeField]
    private double volumen2; //volumen del liquido que debe ir en el recipiente calculado;
    [SerializeField]
    private double volumen3; //volumen del liquido que debe ir en el recipiente calculado;
    [SerializeField]
    private double volumen4; //volumen del liquido que debe ir en el recipiente calculado;

    private double aux1; //creacion de una variable para la ayuda del cambio del signo
    private double aux2; //creacion de una variable para la ayuda del cambio del signo
    private double aux3; //creacion de una variable para la ayuda del cambio del signo
    private double aux4; //creacion de una variable para la ayuda del cambio del signo
    private PhotonView PV;
    // Start is called before the first frame update
    void Start()
    {
        if (!DataInfo.dataInfo.singlePractice)
        {
            PV = GetComponent<PhotonView>();
            if (PV.IsMine)
            {
                Inicializate();
            }
        }
        else
        {
            Inicializate();
        }
    }
    private void Inicializate()
    {
        position1 = tank[0].transform.localPosition; //dando la posicion local (cuando los objetos se encuentran dentro de otro po lo peculiar con position recoje la posicion de plano principal mas no la posicion en realidad en la que se encuentra por lo cal simpre que se este trabajando con objetos dentro de otro plano debemos utilizar localposition para que nos devuleva las verdaderas cordenadas que marcan en el transposition) del recipiente en el caso que exista mas se trabaja con arreglo y se le pasa la posicion del mismo ejemplo positicion[0] = tank[1].transform.position esto debende de la acion que se vaya a realizar. 
        position2 = tank[1].transform.localPosition;
        position3 = tank[2].transform.localPosition;
        position4 = tank[3].transform.localPosition;
    }
    // Update is called once per frame
    void Update()
    {
        if (!DataInfo.dataInfo.singlePractice)
        {
            if (PV.IsMine)
            {
                Filling();
            }
        }
        else
        {
            Filling();
        }
    }
    private void Filling()
    {
        volumen1 = Setting.setting._volumen1; //aqui se cambia por la varibale de la memoria temporal.
        volumen2 = Setting.setting._volumen2;
        volumen3 = Setting.setting._volumen3;
        volumen4 = Setting.setting._volumen4;
        //tank.transform.localPosition = position;
        if (volumen2 != 0 || volumen4 != 0) //validando que la funcionalidad solo se ejecute cuando sea diferente al valor de cero
        {
            if (position1.x < 0) //validando el signo de la cordenada x  es negativo para poder realizar la transformacion del valor del volumen en negativo para el correcto funcionamiento de la animacion
            {
                aux1 = -volumen1; // asignado el valor del volumen a la variable auxiliar para el cambio del signo si cumple la condicion 
            }
            else if (position1.x > 0)
            {
                aux1 = volumen1; //asignando el valor del volumen a la variable en el caso que no cumpla con la anterior condicion
            }

            percent1 = (aux1 * 100) / Setting.setting._heigh; //calculando el porcentaje de llenado con respecto al volumen calculado para vertir en el contenedor
            heigth1 = (percent1 * 1.25f) / 100; //altura que debe tomar el objeto para realizar el llenado
            Debug.Log(position1.y);
            float scaleY1 = System.Convert.ToSingle(position1.y + heigth1 * velocity); //calculado el valor
            tank[0].transform.localScale = new Vector3(tank[0].transform.localScale.x, scaleY1, tank[0].transform.localScale.z);//colocando el vector de escala para la animacion del agua.
            float transformX1 = System.Convert.ToSingle(position1.x + heigth1 * velocity * 1.005);//calculado el valor
            tank[0].transform.localPosition = new Vector3(transformX1, position1.y, position1.z);//colocando el vector de la posicion.



            if (position2.x < 0) //validando el signo de la cordenada x  es negativo para poder realizar la transformacion del valor del volumen en negativo para el correcto funcionamiento de la animacion
            {
                aux2 = -volumen2; // asignado el valor del volumen a la variable auxiliar para el cambio del signo si cumple la condicion 
            }
            else if (position2.x > 0)
            {
                aux2 = volumen2; //asignando el valor del volumen a la variable en el caso que no cumpla con la anterior condicion
            }

            percent2 = (aux2 * 100) / Setting.setting._heigh; //calculando el porcentaje de llenado con respecto al volumen calculado para vertir en el contenedor
            heigth2 = (percent2 * 1.25f) / 100; //altura que debe tomar el objeto para realizar el llenado
            Debug.Log(position2.y);
            float scaleY2 = System.Convert.ToSingle(position2.y + heigth2 * velocity); //calculado el valor
            tank[1].transform.localScale = new Vector3(tank[1].transform.localScale.x, scaleY2, tank[1].transform.localScale.z);//colocando el vector de escala para la animacion del agua.
            float transformX2 = System.Convert.ToSingle(position2.x + heigth2 * velocity * 1.005);//calculado el valor
            tank[1].transform.localPosition = new Vector3(transformX2, position2.y, position2.z);//colocando el vector de la posicion.



            if (position3.x < 0) //validando el signo de la cordenada x  es negativo para poder realizar la transformacion del valor del volumen en negativo para el correcto funcionamiento de la animacion
            {
                aux3 = -volumen3; // asignado el valor del volumen a la variable auxiliar para el cambio del signo si cumple la condicion 
            }
            else if (position3.x > 0)
            {
                aux3 = volumen3; //asignando el valor del volumen a la variable en el caso que no cumpla con la anterior condicion
            }

            percent3 = (aux3 * 100) / Setting.setting._heigh; //calculando el porcentaje de llenado con respecto al volumen calculado para vertir en el contenedor
            heigth3 = (percent3 * 1.25f) / 100; //altura que debe tomar el objeto para realizar el llenado
            Debug.Log(position3.y);
            float scaleY3 = System.Convert.ToSingle(position3.y + heigth3 * velocity); //calculado el valor
            tank[2].transform.localScale = new Vector3(tank[2].transform.localScale.x, scaleY3, tank[2].transform.localScale.z);//colocando el vector de escala para la animacion del agua.
            float transformX3 = System.Convert.ToSingle(position3.x + heigth3 * velocity * 1.005);//calculado el valor
            tank[2].transform.localPosition = new Vector3(transformX3, position3.y, position3.z);//colocando el vector de la posicion.



            if (position4.x < 0) //validando el signo de la cordenada x  es negativo para poder realizar la transformacion del valor del volumen en negativo para el correcto funcionamiento de la animacion
            {
                aux4 = -volumen4; // asignado el valor del volumen a la variable auxiliar para el cambio del signo si cumple la condicion 
            }
            else if (position4.x > 0)
            {
                aux4 = volumen4; //asignando el valor del volumen a la variable en el caso que no cumpla con la anterior condicion
            }

            percent4 = (aux4 * 100) / Setting.setting._heigh; //calculando el porcentaje de llenado con respecto al volumen calculado para vertir en el contenedor
            heigth4 = (percent4 * 1.25f) / 100; //altura que debe tomar el objeto para realizar el llenado
            Debug.Log(position4.y);
            float scaleY4 = System.Convert.ToSingle(position4.y + heigth4 * velocity); //calculado el valor
            tank[3].transform.localScale = new Vector3(tank[3].transform.localScale.x, scaleY4, tank[3].transform.localScale.z);//colocando el vector de escala para la animacion del agua.
            float transformX4 = System.Convert.ToSingle(position4.x + heigth4 * velocity * 1.005);//calculado el valor
            tank[3].transform.localPosition = new Vector3(transformX4, position4.y, position4.z);//colocando el vector de la posicion.
        }
    }
}
