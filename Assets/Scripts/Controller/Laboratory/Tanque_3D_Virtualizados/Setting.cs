﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Setting : MonoBehaviour
{
    public static Setting setting; // declaro que publica la clase setting para poder aceder por este medio a los demas atributos y static para que mantenga los datos a llamar a la clase si no colocamos el static
    //al momento de llamar la clase los valor  estaran incialiados en cero o nullos dependiendo el tipo de dato de la varibale que se tenga en este archivo.
    [SerializeField] //directiva que permite ver el objeto en el inspector de unity mas no al momento de realizar la construcion del proyecto.
    private double heigh; //volumen a soportar el contenedor o recipiente realizado.
    [SerializeField]
    private float volumen1; //valor del volumen que se obtendra de los calculos realizado.
    [SerializeField]
    private float volumen2; //valor del volumen que se obtendra de los calculos realizado.
    [SerializeField]
    private float volumen3; //valor del volumen que se obtendra de los calculos realizado.
    [SerializeField]
    private float volumen4; //valor del volumen que se obtendra de los calculos realizado.

    private float v1;
    private float v2;

    private float perturbacion1;
    private float perturbacion2;
    private float perturbacion3;
    private float perturbacion4;
    private float perturbacion5;
    private float perturbacion6;


    public float _perturbacion1 { set { this.perturbacion1 = value; } get { return perturbacion1; } }
    public float _perturbacion2 { set { this.perturbacion2 = value; } get { return perturbacion2; } }
    public float _perturbacion3 { set { this.perturbacion3 = value; } get { return perturbacion3; } }
    public float _perturbacion4 { set { this.perturbacion4 = value; } get { return perturbacion4; } }
    public float _perturbacion5 { set { this.perturbacion5 = value; } get { return perturbacion5; } }
    public float _perturbacion6 { set { this.perturbacion6 = value; } get { return perturbacion6; } }

    public double _heigh { set { this.heigh = value; } get { return heigh; } } //asignacion de valor privado a una variable publica para la obtencion de los datos utilizando la sintaxis del lenguaje
    public float _volumen1 { set { this.volumen1 = value; } get { return volumen1; } } //asignacion de valor privado a una variable publica para la obtencion de los datos utilizando la sintaxis del lenguaje.
    public float _volumen2 { set { this.volumen2 = value; } get { return volumen2; } }
    public float _volumen3 { set { this.volumen3 = value; } get { return volumen3; } }
    public float _volumen4 { set { this.volumen4 = value; } get { return volumen4; } }
    public float _v1 { set { this.v1 = value; } get { return v1; } } //asignacion de valor privado a una variable publica para la obtencion de los datos utilizando la sintaxis del lenguaje.
    public float _v2 { set { this.v2 = value; } get { return v2; } } //asignacion de valor privado a una variable publica para la obtencion de los datos utilizando la sintaxis del lenguaje.

    private void Awake() //Esta se ejecuta para inicializar el objeto
    {
        if (Setting.setting == null) //verificar que el objeto se encuentre en nulo
        {
            Setting.setting = this; //se asiga el objeto setting  y this a punta si mismo.
        }
        else
        {
            if (Setting.setting != this) //si setting es diferente de si mismo procede a destruir.
            {
                Destroy(this.gameObject); //Desgtruye el objeto.
            }
        }
        DontDestroyOnLoad(this.gameObject); //crea el objeto pero que no sea destruido al momento de realizar un cambio de escena.
    }
}
