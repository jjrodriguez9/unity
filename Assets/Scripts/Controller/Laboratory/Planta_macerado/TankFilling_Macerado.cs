﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankFilling_Macerado : MonoBehaviour
{
    [SerializeField]
    private GameObject water; //declaracion del objeto de tanque 1
    [SerializeField]
    private GameObject water2; //declaracion del objeto de tanque 2
    [SerializeField] //Visualizo en el inspector
    private Vector3 position; //vector para tomar la posicion del objet water
    [SerializeField]
    private Vector3 position2; //vector para tomar la posicion del objet water
    [SerializeField]
    private GameObject Nivel1; //declaracion del objeto de pantalla 1
    [SerializeField]
    private GameObject Nivel2; //declaracion del objeto de pantalla  
    [SerializeField] //Visualizo en el inspector
    private Vector3 positionPantalla; //vector para tomar la posicion del objet water
    [SerializeField]
    private Vector3 positionPantalla2; //vector para tomar la posicion del objet water
    [SerializeField]
    private double velocity; //es el valor con que velocidad se va a realizar la animacion del llenado del recipiente
    [SerializeField]
    private Fluidotank fluidotank = new Fluidotank();
    [SerializeField]
    private Fluidotank2 fluidotank2 = new Fluidotank2();
    [SerializeField]
    private Explotion explotion = new Explotion();
    [SerializeField]
    private LlenadoTank llenadoTank = new LlenadoTank();
    [SerializeField]
    private LlenadoTank2 llenadoTank2 = new LlenadoTank2();
    [SerializeField]
    private EfectoTuberias1 efectoTuberia = new EfectoTuberias1();
    [SerializeField]
    private EfectoTuberia2 efectoTuberia2 = new EfectoTuberia2();
    [SerializeField]
    private EfectoTuberiaSalida efectoTuberiaSalida = new EfectoTuberiaSalida();
    private double volumen;
    private double volumen2;
    private double percent; //porcentaje del llenado que se encuentra el recipiente 1
    private double percent2; //porcentaje del llenado que se encuentra el recipiente 2
    private double altura; //altura del vulumen que esra recibiendo el recipiente 1
    private double altura2; //altura del vulumen que esra recibiendo el recipiente 2

    private double volumenP;
    private double volumen2P;
    private double percentP; //porcentaje del llenado que se encuentra el recipiente 1
    private double percent2P; //porcentaje del llenado que se encuentra el recipiente 2
    private double alturaP; //altura del vulumen que esra recibiendo el recipiente 1
    private double altura2P; //altura del vulumen que esra recibiendo el recipiente 2

    [SerializeField]
    private Setting_Macerado seting;

    [SerializeField]
    private PhotonView PV;

    // Start is called before the first frame update
    void Start()
    {
        if (!DataInfo.dataInfo.singlePractice)
        {
            PV = GetComponent<PhotonView>();
            Inicializate();
        }
        else
        {
            Inicializate();
        }

    }

    // Update is called once per frame
    void Update()
    {
        if (!DataInfo.dataInfo.singlePractice)
        {
            if (PV.IsMine)
            {

                Filling();
                ScreenFilling();
            }
        }
        else
        {
            Filling();
            ScreenFilling();
        }

    }


    private void Inicializate()
    {
        seting = GameObject.Find("Setting").GetComponent<Setting_Macerado>();
        position = water.transform.localPosition; //coloco vector de posicion del recipiente, utilizo local position porque el recipíente se encuentra adentro de un objeto devolviendo la posicion dentro del plano de ese objeto
        position2 = water2.transform.localPosition;
        positionPantalla = Nivel1.transform.localPosition;
        positionPantalla2 = Nivel2.transform.localPosition;
    }
    private void Filling()
    {
        

        volumen = seting._volumen; // valor de la altura obtenido del modelo matematico para el tanque 1
        volumen2 = seting._volumen2; // valor de la altura obtenido del modelo matematico para el tanque 2
        //Debug.Log("Volumen:" + volumen);
        //Debug.Log("Volumen2:" + volumen2);
        #region Condiciones de activacion de efectos
        if (volumen >= Setting_Macerado.setting._heigh )
        {
            if (!DataInfo.dataInfo.singlePractice)
            {
                fluidotank.PV.RPC("activefluido", RpcTarget.All, true);
            }
            else
            {
                fluidotank.activefluido(true);
            }
        }
        else
        {
            if (!DataInfo.dataInfo.singlePractice)
            {
                fluidotank.PV.RPC("activefluido", RpcTarget.All, false);
            }
            else
            {
                fluidotank.activefluido(false);
            }
        }
        if (Setting_Macerado.setting._a2 >= 0.1 || Setting_Macerado.setting._a1 >= 0.1)
        {
            if (!DataInfo.dataInfo.singlePractice)
            {
                llenadoTank.PV.RPC("activellenado", RpcTarget.All, true);
            }
            else
            {
                llenadoTank.activellenado(true);
            }
           
        }
        else
        {
            if (!DataInfo.dataInfo.singlePractice)
            {
                llenadoTank.PV.RPC("activellenado", RpcTarget.All, false);
            }
            else
            {
                llenadoTank.activellenado(false);
            }
        }
        if (Setting_Macerado.setting._a3 >= 0.1 && volumen2 < Setting_Macerado.setting._heigh2 * 0.75)
        {
            if (!DataInfo.dataInfo.singlePractice)
            {
                llenadoTank2.PV.RPC("activellenado2", RpcTarget.All, true);
            }
            else
            {
                llenadoTank2.activellenado2(true);
            }
        }
        else
        {
            if (!DataInfo.dataInfo.singlePractice)
            {
                llenadoTank2.PV.RPC("activellenado2", RpcTarget.All, false);
            }
            else
            {
                llenadoTank2.activellenado2(false);
            }
        }
        if (Setting_Macerado.setting._volumen2 <= 0.08 )
        {

            if (!DataInfo.dataInfo.singlePractice)
            {
                Debug.Log("talking if");
                explotion.PV.RPC("activeexplotar", RpcTarget.All, true,0);
            }
            else
            {
                Debug.Log("talking if");
                explotion.activeexplotar(true,0);
            }
        }
        else
        {
            if (!DataInfo.dataInfo.singlePractice)
            {
                Debug.Log("talking else");
                explotion.PV.RPC("activeexplotar", RpcTarget.All, false,1);
            }
            else
            {
                Debug.Log("talking else");
                explotion.activeexplotar(false,1);
            }
        }
        if (volumen2 >= Setting_Macerado.setting._heigh2)
        {
            if (!DataInfo.dataInfo.singlePractice)
            {
                fluidotank2.PV.RPC("activeRegado", RpcTarget.All, true);
            }
            else
            {
                fluidotank2.activeRegado(true);
            }
            
        }
        else
        {
            if (!DataInfo.dataInfo.singlePractice)
            {
                fluidotank2.PV.RPC("activeRegado", RpcTarget.All, false);
            }
            else
            {
                fluidotank2.activeRegado(false);
            }
        }
        if (Setting_Macerado.setting._a1 >= 0.1)
        {
            if (!DataInfo.dataInfo.singlePractice)
            {
                efectoTuberia.PV.RPC("activeTuberia1", RpcTarget.All, true);
            }
            else
            {
                efectoTuberia.activeTuberia1(true);
            }
        }
        else
        {
            if (!DataInfo.dataInfo.singlePractice)
            {
                efectoTuberia.PV.RPC("activeTuberia1", RpcTarget.All, false);
            }
            else
            {
                efectoTuberia.activeTuberia1(false);
            }
        }
        if (Setting_Macerado.setting._a2 >= 0.1 )
        {

            if (!DataInfo.dataInfo.singlePractice)
            {
                efectoTuberia2.PV.RPC("activeTuberia2", RpcTarget.All, true);
            }
            else
            {
                efectoTuberia2.activeTuberia2(true);
            }
        }
        else
        {
            if (!DataInfo.dataInfo.singlePractice)
            {
                efectoTuberia2.PV.RPC("activeTuberia2", RpcTarget.All, false);
            }
            else
            {
                efectoTuberia2.activeTuberia2(false);
            }
        }
        if (Setting_Macerado.setting._a4 >= 0.1)
        {
            if (!DataInfo.dataInfo.singlePractice)
            {
                efectoTuberiaSalida.PV.RPC("activeTuberiasalida", RpcTarget.All, true);
            }
            else
            {
                efectoTuberiaSalida.activeTuberiasalida(true);
            }
        }
        else
        {
            if (!DataInfo.dataInfo.singlePractice)
            {
                efectoTuberiaSalida.PV.RPC("activeTuberiasalida", RpcTarget.All, false);
            }
            else
            {
                efectoTuberiaSalida.activeTuberiasalida(false);
            }
        }
        #endregion
        percent = (volumen * 100) / seting._heigh; //calculando el porcentaje de llenado con respecto al volumen calculado para vertir en el contenedor
        altura = (percent * 6.7f) / 100;
        float scaley = System.Convert.ToSingle(position.y + altura * velocity ); //conversion del valor
        water.transform.localScale = new Vector3(water.transform.localScale.x, scaley, water.transform.localScale.z); //afecto la escala del objeto
        float transformZ = System.Convert.ToSingle(position.z + altura * velocity);
        water.transform.localPosition = new Vector3(position.x, position.y, transformZ);

        percent2 = (volumen2 * 100) / seting._heigh2; //calculando el porcentaje de llenado con respecto al volumen calculado para vertir en el contenedor
        altura2 = (percent2 * 5.2f) / 100;
        float scaley2 = System.Convert.ToSingle(position2.y + altura2 * velocity); //conversion del valor
        water2.transform.localScale = new Vector3(water2.transform.localScale.x, scaley2, water2.transform.localScale.z); //afecto la escala del objeto
        float transformZ2 = System.Convert.ToSingle(position2.z + altura2 * velocity);
        water2.transform.localPosition = new Vector3(position2.x, position2.y, transformZ2);
    }
    private void ScreenFilling()
    {
        
        volumenP = seting._volumen; // valor de la altura obtenido del modelo matematico para el tanque 1
        volumen2P = seting._volumen2; // valor de la altura obtenido del modelo matematico para el tanque 2
        percentP = (volumen * 100) / seting._heigh;
        alturaP = (percentP * 19f) / 100;
        float scalezP = System.Convert.ToSingle(positionPantalla.z + alturaP * velocity); //conversion del valor
        Nivel1.transform.localScale = new Vector3(Nivel1.transform.localScale.x, Nivel1.transform.localScale.y, scalezP); //afecto la escala del objeto
        float transformY = System.Convert.ToSingle(positionPantalla.y + alturaP * velocity ) * 0.5421f;
        Nivel1.transform.localPosition = new Vector3(positionPantalla.x, transformY, positionPantalla.z);

        percent2P = (volumen2 * 100) / seting._heigh2; //calculando el porcentaje de llenado con respecto al volumen calculado para vertir en el contenedor
        altura2P = (percent2P * 19f) / 100;
        float scalez2 = System.Convert.ToSingle(positionPantalla2.z + altura2P * velocity); //conversion del valor
        Nivel2.transform.localScale = new Vector3(Nivel2.transform.localScale.x, Nivel2.transform.localScale.y, scalez2); //afecto la escala del objeto
        float transformY2 = System.Convert.ToSingle(positionPantalla2.y + altura2P  * velocity) * 0.5421f;
        Nivel2.transform.localPosition = new Vector3(positionPantalla2.x, transformY2, positionPantalla2.z);
    }
}
