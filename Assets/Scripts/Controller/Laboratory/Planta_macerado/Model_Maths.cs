﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;
using TMPro; //interactuar con labels nuevos
using Photon.Pun;

public class Model_Maths : MonoBehaviourPun
{
    [SerializeField]
    private Setting_Macerado seting;

    [SerializeField]
    private RunOperation_Macerado runOperation = new RunOperation_Macerado();//declara la calse de runoperatios ya que el tiene los slider en su clase y reutilizar la clase
    private float h1p;
    private float h2p;
    private float h1;   //Altura del tanque 1 
    private float h2;   //Altura del tanque 2
    private float L1;   //Litros del tanque 1 
    private float L2;   //Litros del tanque 2
    private float L1max;   //Litros del tanque 1 
    private float L2max;   //Litros del tanque 2
    private float k1V1; //Constante de la bomba 1
    private float k2V2; //Constante de la bomba 2
    private float g;    //Constante de gravedad
    private float a1;   // Valvula de control a1 (0-1)
    private float a2;   // Valvula de control a2 (0-1)
    private float a3;   // Valvula de control a3 (0-1)
    private float a4;   // Valvula de perturvacion a1 (0-1)
    private float Ar1;  // Area del tanque 1 [m^2]
    private float Ar2;  // Area del tanque 2 [m^2]
    private float r;  // radio del tanque
    private float s1;   // Constante de Seccion trasversal
    private float s2;   // Constante de Seccion trasversal
    private float s3;   // Constante de Seccion trasversal
    private float SP1;  // SetPoint real (altura deseada Tk1)
    private float SP2;  // SetPoint real (altura deseada Tk2)
    private float err1; // Error de control tanque 1
    private float err2; // Error de control tanque 2
    private float TimeSample;
    private float time;
    private Memoryshare memoryshare;
    private readonly int totalMem = 50;
    float To;
    [SerializeField]
    private PhotonView PV;

    void Start()
    {
        if (!DataInfo.dataInfo.singlePractice)
        {
            PV = GetComponent<PhotonView>();
            if (PV.IsMine)
            {
                Inicializate();
            }
        }
        else
        {
            Inicializate();
        }
    }

    private void Inicializate()
    {
        seting = GameObject.Find("Setting").GetComponent<Setting_Macerado>();

        memoryshare = new Memoryshare();
        memoryshare.createMemoriShare("MemoriaONE", totalMem, 2);
        memoryshare.createMemoriShare("MemoriaTWO", totalMem, 2);
        memoryshare.openMemoryShare("MemoriaONE", 2);
        memoryshare.openMemoryShare("MemoriaTWO", 2);

        for (int i = 0; i < totalMem; i++)
        {
            memoryshare.Setfloat("MemoriaONE", 0f, i);
            memoryshare.Setfloat("MemoriaTWO", 0f, i);
        }
        h1 = 0f;
        h2 = 0.5f;
        L1 = 0f;
        L2 = 0f;
        k1V1 = 0.2f;
        k2V2 = 0.2f;
        g = 9.8f;
        Ar1 = 2;
        Ar2 = 2;
        r = 0.7978f;
        s1 = 0.07296f;
        s2 = 0.07296f;
        s3 = 0.03242f;
        To = 0;
        TimeSample = 0.1f; //[s]
    }

    // Update is called once per frame
    void Update()
    {
        if (!DataInfo.dataInfo.singlePractice)
        {
            if (PV.IsMine)
            {
                modelResult();
            }
        }
        else
        {
            modelResult();
        }
    }

    private void modelResult()
    {
        
        To += Time.deltaTime; // Time.deltaTime = 1/fps, si 60 fps -> Time.deltaTime = 0.01666 [s] = 16.66 [ms] 

        if (To >= TimeSample)
        {

            time += Time.deltaTime;
            seting._time = time;

            SP1 = (runOperation._insp[0].value * System.Convert.ToSingle(seting._heigh)); // Tomo el valor del  slider del Set Point 1 de la clase RunOperation
            seting._SP1 = SP1; //asigno el valor del SP1 a la clase setting
            SP2 = (runOperation._insp[1].value * System.Convert.ToSingle(seting._heigh2)); // Tomo el valor del  slider del Set Point 2 de la clase RunOperation
            seting._SP2 = SP2; //asigno el valor del SP2 a la clase setting
            a4 = (runOperation._insp[2].value * System.Convert.ToSingle(seting._pertuvacionmax)); // Tomo el valor del  slider de la perturvacion de la clase RunOperation
            seting._a4 = a4; //asigno el valor de a4 a la clase setting
            //************ RECIBE DATOS DE MATLAB******************//
            a1 = memoryshare.Getfloat("MemoriaONE", 0); // Valvula de control a1 - Salida del controlador
            seting._a1 = a1; //asigno el valor del a1 a la clase setting

            a2 = memoryshare.Getfloat("MemoriaONE", 1); // Valvula de control a2 - Salida del controlador
            seting._a2 = a2; //asigno el valor del a2 a la clase setting

            a3 = memoryshare.Getfloat("MemoriaONE", 2); // Valvula de control a3 - Salida del controlador
            seting._a3 = a3; //asigno el valor del a3 a la clase setting

            //Modelo Matematico Planta de Macerado (Nivel)
            h1p = ((k1V1 * a1) + ((s2 * Mathf.Sqrt(2 * g * h2) + k2V2) * a2) / 2 - s1 * a3 * Mathf.Sqrt(2 * g * h1)) / Ar1; 
            h1 = To * h1p + h1; // altura del tanque 1 (salida de la planta integrada)
            err1 = SP1 - h1; // error de nivel del tanque 1 (valor deseado - valor real)(entrada del controlador)
            L1 = Mathf.PI * Mathf.Pow(r, 2) * h1 * 1000; //Calculo de litros del tanque 1
            L1max = Mathf.PI * Mathf.Pow(r, 2) * System.Convert.ToSingle(seting._heigh) * 1000; //Calculo de la maxima capacidad de litros del tanque 1
            seting._volumen = h1; //asigno el valor de la altura 1 a la clase setting
            seting._error1 = err1; //asigno el valor del error 1 a la clase setting
            seting._L1 = L1;       //asigno el valor de litros 1 a la clase setting
            seting._L1max = L1max; //asigno el valor de la maxima capacidad en litros Tk1 a la clase setting

            h2p = (s1 * a3 * Mathf.Sqrt(2 * g * h1) - ((s2 * Mathf.Sqrt(2 * g * h2) + k2V2) * a2) / 2 - s3 * a4 * Mathf.Sqrt(2 * g * h2)) / Ar2;
            h2 = To * h2p + h2; // altura del tanque 2 (salida de la planta integrada)
            err2 = SP2 - h2; // error de nivel del tanque 1 (valor deseado - valor real)(entrada del controlador)
            L2 = Mathf.PI * Mathf.Pow(r, 2) * h2 * 1000; //Calculo de litros del tanque 2
            L2max = Mathf.PI * Mathf.Pow(r, 2) * System.Convert.ToSingle(seting._heigh2) * 1000; //Calculo de la maxima capacidad de litros del tanque 2
            seting._volumen2 = h2; //asigno el valor de la altura 2 a la clase setting
            seting._error2 = err2; //asigno el valor del error 2 a la clase setting
            seting._L2 = L2;       //asigno el valor de litros 2 a la clase setting
            seting._L2max = L2max; //asigno el valor de la maxima capacidad en litros TK2 a la clase setting

            //Envio datos a Matlab 

            if (!DataInfo.dataInfo.singlePractice)
            {
                PV.RPC("MultiGraphics", RpcTarget.All,time,SP1,SP2,h1,h2, L1max, L1, L2max, L2);
                PV.RPC("MultiCalculate", RpcTarget.All, a1, a2, a3, a4, SP1, SP2, h1, h2, err1, err2, L1max, L1, L2max, L2);
            }
            memoryshare.Setfloat("MemoriaTWO", h1,0);
            memoryshare.Setfloat("MemoriaTWO", h2,1);
            memoryshare.Setfloat("MemoriaTWO", SP1,2);
            memoryshare.Setfloat("MemoriaTWO", SP2,3);
            memoryshare.Setfloat("MemoriaTWO", a4,4);

            To = 0;
        }
    }
}
