﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class Fluidotank2 : MonoBehaviourPun
{
    private ParticleSystem EfectoRegado;
    // Start is called before the first frame update
    public PhotonView PV;
    
    void Start()
    {
        if (!DataInfo.dataInfo.singlePractice)
        {
            PV = GetComponent<PhotonView>();
            Inicializate();
        }
        else
        {
            Inicializate();
        }
    }

    public void Inicializate()
    {

        gameObject.SetActive(false);
        EfectoRegado = GetComponent<ParticleSystem>();
    }

    [PunRPC]
    public void activeRegado(bool active)
    {
        if (active)
        {
            EfectoRegado.gameObject.SetActive(active);
            EfectoRegado.Play(active);
        }
        else
        {
            gameObject.SetActive(false);
            EfectoRegado.gameObject.SetActive(false);
            EfectoRegado.Play(false);
        }
    }
}
