#define Graph_And_Chart_PRO
using System.Collections.Generic;
using UnityEngine;
using System.Collections;
using ChartAndGraph;

using Photon.Pun;

public class Graph_Planta_Macerado : MonoBehaviourPun
{
    [SerializeField]
    private GraphChart[] graph;
    [SerializeField]
    private PhotonView PV;
    [SerializeField]
    private Setting_Macerado seting;
    public int TotalPoints = 5;
    float lastTime = 0f;
    float lastX = 0f;

    void Start()
    {

        if (!DataInfo.dataInfo.singlePractice)
        {
            PV = GetComponent<PhotonView>();
            Inicializate();
        }
        else
        {
            Inicializate();
        }

    }



    void Update()
    {

        if (DataInfo.dataInfo.singlePractice)
        {
            SingleGraphics();
        }
        
       
    }


    private void Inicializate()
    {
        seting = GameObject.Find("Setting").GetComponent<Setting_Macerado>();
        if (graph == null) // the ChartGraph info is obtained via the inspector
            return;

        graph[0].DataSource.StartBatch(); // calling StartBatch allows changing the graph data without redrawing the graph for every change
        graph[0].DataSource.ClearCategory("SP1"); // clear the "Player 1" category. this category is defined using the GraphChart inspector
        graph[0].DataSource.ClearCategory("Altura1"); // clear the "Player 1" category. this category is defined using the GraphChart inspector
        graph[0].DataSource.EndBatch(); // finally we call EndBatch , this will cause the GraphChart to redraw itself
        graph[1].DataSource.StartBatch(); // calling StartBatch allows changing the graph data without redrawing the graph for every change
        graph[1].DataSource.ClearCategory("SP2"); // clear the "Player 1" category. this category is defined using the GraphChart inspector
        graph[1].DataSource.ClearCategory("Altura2"); // clear the "Player 1" category. this category is defined using the GraphChart inspector
        graph[1].DataSource.EndBatch(); // finally we call EndBatch , this will cause the GraphChart to redraw itself
        graph[2].DataSource.StartBatch(); // calling StartBatch allows changing the graph data without redrawing the graph for every change
        graph[2].DataSource.ClearCategory("LitrosMax1"); // clear the "Player 1" category. this category is defined using the GraphChart inspector
        graph[2].DataSource.ClearCategory("Litros1"); // clear the "Player 1" category. this category is defined using the GraphChart inspector
        graph[2].DataSource.EndBatch(); // finally we call EndBatch , this will cause the GraphChart to redraw itself
        graph[3].DataSource.StartBatch(); // calling StartBatch allows changing the graph data without redrawing the graph for every change
        graph[3].DataSource.ClearCategory("LitrosMax2"); // clear the "Player 1" category. this category is defined using the GraphChart inspector
        graph[3].DataSource.ClearCategory("Litros2"); // clear the "Player 1" category. this category is defined using the GraphChart inspector
        graph[3].DataSource.EndBatch(); // finally we call EndBatch , this will cause the GraphChart to redraw itself
    }

    private void SingleGraphics()
    {
        float time = Time.time;
        if (lastTime + 0.1f < time)
        {
            lastTime = time;

            graph[0].DataSource.AddPointToCategoryRealtime("SP1", seting._time, seting._SP1); // each time we call AddPointToCategory
            graph[0].DataSource.AddPointToCategoryRealtime("Altura1", seting._time, seting._volumen); // each time we call AddPointToCategory
            graph[1].DataSource.AddPointToCategoryRealtime("SP2", seting._time, seting._SP2); // each time we call AddPointToCategory
            graph[1].DataSource.AddPointToCategoryRealtime("Altura2", seting._time, seting._volumen2); // each time we call AddPointToCategory
            graph[2].DataSource.AddPointToCategoryRealtime("NivelMax", seting._time, seting._L1max); // each time we call AddPointToCategory
            graph[2].DataSource.AddPointToCategoryRealtime("Litros1", seting._time, seting._L1); // each time we call AddPointToCategory
            graph[3].DataSource.AddPointToCategoryRealtime("NivelMax2", seting._time, seting._L2max); // each time we call AddPointToCategory
            graph[3].DataSource.AddPointToCategoryRealtime("Litros2", seting._time, seting._L2); // each time we call AddPointToCategory
        }
    }

    [PunRPC]
    private void MultiGraphics(float tiempo,float sp1,float sp2,float volumen1,float volumen2,float l1max,float l1,float l2max, float l2)
    {
        float time = Time.time;
        if (lastTime + 0.1f < time)
        {
            lastTime = time;

            graph[0].DataSource.AddPointToCategoryRealtime("SP1", tiempo, sp1); // each time we call AddPointToCategory
            graph[0].DataSource.AddPointToCategoryRealtime("Altura1", tiempo, volumen1); // each time we call AddPointToCategory
            graph[1].DataSource.AddPointToCategoryRealtime("SP2", tiempo, sp2); // each time we call AddPointToCategory
            graph[1].DataSource.AddPointToCategoryRealtime("Altura2", tiempo, volumen2); // each time we call AddPointToCategory
            graph[2].DataSource.AddPointToCategoryRealtime("NivelMax", time,l1max); // each time we call AddPointToCategory
            graph[2].DataSource.AddPointToCategoryRealtime("Litros1", time, l1); // each time we call AddPointToCategory
            graph[3].DataSource.AddPointToCategoryRealtime("NivelMax2", time, l2max); // each time we call AddPointToCategory
            graph[3].DataSource.AddPointToCategoryRealtime("Litros2", time, l2); // each time we call AddPointToCategory

        }
    }
}
