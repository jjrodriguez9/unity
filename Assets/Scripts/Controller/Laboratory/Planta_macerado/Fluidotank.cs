﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class Fluidotank : MonoBehaviourPun
{
    [SerializeField]
    private ParticleSystem EfectoFluido;
    public PhotonView PV;

    // Start is called before the first frame update
    void Start()
    {
        
        if (!DataInfo.dataInfo.singlePractice)
        {
            PV = GetComponent<PhotonView>();
            Inicializate();
        }
        else
        {
            Inicializate();
        }
    }


    private void Inicializate()
    {
        gameObject.SetActive(false);
        EfectoFluido = GetComponent<ParticleSystem>();
    }

    [PunRPC]
    public void activefluido(bool active)
    {
        if (active)
        {
            EfectoFluido.gameObject.SetActive(active);
            EfectoFluido.Play(active);
        }
        else
        {
            gameObject.SetActive(false);
            EfectoFluido.gameObject.SetActive(false);
            EfectoFluido.Play(false);
        }

    }

   
}
