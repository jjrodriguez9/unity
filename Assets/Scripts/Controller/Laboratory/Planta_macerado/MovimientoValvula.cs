﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MovimientoValvula : MonoBehaviour
{
    [SerializeField]
    private RunOperation_Macerado insp = new RunOperation_Macerado();
    public float Manija1;
    public float Manija1v;

    float sinAngle = 0.0f;
    float cosAngle = 0.0f;
    public Quaternion qy = Quaternion.identity;
    public Quaternion r = Quaternion.identity;
   


    void Update()
    {
        Movimiento();
    }

    private void Movimiento()
    {
        Manija1 = System.Convert.ToSingle(insp._insp[2].value * 10); //procedo a tomar el valor del slider y transformar a la escala del contendor cabe recalcar que los valos del slider va entre 0 y 1

        Manija1v = Manija1;
        sinAngle = Mathf.Sin(Mathf.Deg2Rad * Manija1 * 4.5f);
        cosAngle = Mathf.Cos(Mathf.Deg2Rad * Manija1 * 4.5f);
        qy.Set(0, sinAngle, 0, cosAngle);
        r = qy;
        transform.rotation = r;
    }

}
