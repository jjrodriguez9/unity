﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//setting contiene la informacion que necesitamos
public class Setting_Macerado : MonoBehaviour
{
    public static Setting_Macerado setting; //declaro que sea publica la clase setting para poder acceder por este medio a los demas atributos y static para que mantenga los datos de los atributos
    [SerializeField] //directiva que permite ver el objeto en el inspector de unity mas no al momento de realizar la construcion del proyecto.
    private double heigh; //volumen del a soportar el contenedor o recipiente realizado.
    [SerializeField] //directiva que permite ver el objeto en el inspector de unity mas no al momento de realizar la construcion del proyecto.
    private double heigh2; //volumen del a soportar el contenedor o recipiente realizado.
    [SerializeField] //directiva que permite ver el objeto en el inspector de unity mas no al momento de realizar la construcion del proyecto.
    private double pertuvacionmax; //volumen del a soportar el contenedor o recipiente realizado.
    [SerializeField]
    private float volumen; //valor del volumen que se obtendra de los calculos realizado.
    [SerializeField]
    private float volumen2; //valor del volumen que se obtendra de los calculos realizado.

    private float timeX;
    private float a1;
    private float a2;
    private float a3;
    private float a4;
    private float h1;
    private float h2;
    private float SP1;
    private float SP2;
    private float error1;
    private float error2;
    private float L1;
    private float L2;
    private float L1max;
    private float L2max;

    public float _volumen { set { this.volumen = value; } get { return volumen; } }
    public float _volumen2 { set { this.volumen2 = value; } get { return volumen2; } }
    public float _h1 { set { this.h1 = value; } get { return h1; } } 
    public float _h2 { set { this.h2 = value; } get { return h2; } }
    public float _error1 { set { this.error1 = value; } get { return error1; } }
    public float _error2 { set { this.error2 = value; } get { return error2; } }
    public double _heigh { set { this.heigh = value; } get { return heigh; } } //asignacion de valor privado a una variable publica para la obtencion de los datos utilizando la sintaxis del lenguaje.
    public double _heigh2 { set { this.heigh2 = value; } get { return heigh2; } } //asignacion de valor privado a una variable publica para la obtencion de los datos utilizando la sintaxis del lenguaje.
    public double _pertuvacionmax { set { this.pertuvacionmax = value; } get { return pertuvacionmax; } } //asignacion de valor privado a una variable publica para la obtencion de los datos utilizando la sintaxis del lenguaje.
    public float _time { set { this.timeX = value; } get { return timeX; } } //asignacion de valor privado a una variable publica para la obtencion de los datos utilizando la sintaxis del lenguaje.
    public float _a1 { set { this.a1 = value; } get { return a1; } } //asignacion de valor privado a una variable publica para la obtencion de los datos utilizando la sintaxis del lenguaje.
    public float _a2 { set { this.a2 = value; } get { return a2; } }
    public float _a3 { set { this.a3 = value; } get { return a3; } }
    public float _a4 { set { this.a4 = value; } get { return a4; } }
    public float _SP1 { set { this.SP1 = value; } get { return SP1; } }
    public float _SP2 { set { this.SP2 = value; } get { return SP2; } }
    public float _L1 { set { this.L1 = value; } get { return L1; } }
    public float _L2 { set { this.L2 = value; } get { return L2; } }
    public float _L1max { set { this.L1max = value; } get { return L1max; } }
    public float _L2max { set { this.L2max = value; } get { return L2max; } }
    private void Awake()
    {
        if (Setting_Macerado.setting == null) //verifica que el objeto se encuentre en nulo;
        {
            Setting_Macerado.setting = this; //asigna el objeto setting y this a punta asi mismo
        }
        else
        {
            if (Setting_Macerado.setting != this) //si setting es diferente de si mismo procede a destruir el objeto
            {
                Destroy(this.gameObject);
            }
         }
        DontDestroyOnLoad(this.gameObject);
     }
}