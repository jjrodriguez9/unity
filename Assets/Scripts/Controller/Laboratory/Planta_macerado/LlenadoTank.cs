﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class LlenadoTank : MonoBehaviour
{
    [SerializeField]
    private ParticleSystem llenadoTank1;
    // Start is called before the first frame update
    [SerializeField]
    private Setting_Macerado seting;

    public PhotonView PV;
    void Start()
    {
        if (!DataInfo.dataInfo.singlePractice)
        {
            PV = GetComponent<PhotonView>();
            Inicializate();
        }
        else
        {
            Inicializate();
        }
    }

    private void Inicializate()
    {
        gameObject.SetActive(false);
        llenadoTank1 = GetComponent<ParticleSystem>();
    }

    [PunRPC]
    public void activellenado(bool active)
    {
        if (active && Setting_Macerado.setting._volumen < Setting_Macerado.setting._heigh * 0.75)
        {
            llenadoTank1.gameObject.SetActive(active);
            llenadoTank1.Play(active);
        }
        else
        {
            gameObject.SetActive(false);
            llenadoTank1.gameObject.SetActive(false);
            llenadoTank1.Play(false);
        }

    }
}
