﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Photon.Pun;

public class Explotion : MonoBehaviourPun
{
    [SerializeField]
    private GameObject llamaObjetc;
    [SerializeField]
    private ParticleSystem EfectoExplosión,llama;
    [SerializeField]
    private AudioSource audioSource,Sourcellama;
    [SerializeField]
    private AudioClip breakglass,llam; //audio clip
    bool hasExploded = false;

    public PhotonView PV;
    // Start is called before the first frame update
    void Start()
    {
        if (!DataInfo.dataInfo.singlePractice)
        {
            PV = GetComponent<PhotonView>();
            Inicializate();
        }
        else
        {
            Inicializate();
        }
    }

    private void Inicializate()
    {
        gameObject.SetActive(false);
        EfectoExplosión = GetComponent<ParticleSystem>();
        llama = llamaObjetc.GetComponent<ParticleSystem>();
        audioSource = GetComponent<AudioSource>();
        Sourcellama = llamaObjetc.GetComponent<AudioSource>();
    }


   [PunRPC]
    public void activeexplotar(bool active,int countdown)
    {
        if (active && hasExploded == false && countdown == 0)
        {
            Debug.Log("funcion"+ active);
            EfectoExplosión.gameObject.SetActive(active);
            audioSource.PlayOneShot(breakglass);
            EfectoExplosión.Play(active);
            //llamaObjetc.SetActive(active);
            llama.gameObject.SetActive(active);
            Sourcellama.PlayOneShot(llam);
            llama.Play(active);
            countdown = 3;
            hasExploded = true;
        }
        else if(countdown==1)
        {
            Debug.Log("funcion" + active);
            EfectoExplosión.gameObject.SetActive(active);
            llama.gameObject.SetActive(active);
            EfectoExplosión.Stop();
            llama.Stop();
            countdown = 3;
            hasExploded = false;

        }
    }
}


