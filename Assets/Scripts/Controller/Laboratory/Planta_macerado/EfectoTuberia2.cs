﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;


public class EfectoTuberia2 : MonoBehaviourPun
{
    [SerializeField]
    private ParticleSystem TuberiaTank2;
    // Start is called before the first frame update
    
    public PhotonView PV;
    void Start()
    {
        if (!DataInfo.dataInfo.singlePractice)
        {
            PV = GetComponent<PhotonView>();
            Inicializate();
        }
        else
        {
            Inicializate();
        }
    }
    
    private void Inicializate()
    {
        gameObject.SetActive(false);
        TuberiaTank2 = GetComponent<ParticleSystem>();
    }

    [PunRPC]
    public void activeTuberia2(bool active)
    {
        if (active)
        {
            TuberiaTank2.gameObject.SetActive(active);
            TuberiaTank2.Play(active);
        }
        else
        {
            gameObject.SetActive(false);
            TuberiaTank2.gameObject.SetActive(false);
            TuberiaTank2.Play(false);
        }

    }
}
