﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;


public class EfectoTuberiaSalida : MonoBehaviour
{
    [SerializeField]
    private ParticleSystem TuberiaSalida;
    // Start is called before the first frame update
    public PhotonView PV;

    void Start()
    {
        
        if (!DataInfo.dataInfo.singlePractice)
        {
            PV = GetComponent<PhotonView>();
            Inicializate();
        }
        else
        {
            Inicializate();
        }
    }

    public void Inicializate()
    {
        gameObject.SetActive(false);
        TuberiaSalida = GetComponent<ParticleSystem>();
    }

    [PunRPC]
    public void activeTuberiasalida(bool active)
    {
        if (active)
        {
            TuberiaSalida.gameObject.SetActive(active);
            TuberiaSalida.Play(active);
        }
        else
        {
            gameObject.SetActive(false);
            TuberiaSalida.gameObject.SetActive(false);
            TuberiaSalida.Play(false);
        }

    }
}
