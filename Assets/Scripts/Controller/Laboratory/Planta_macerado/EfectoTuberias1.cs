﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EfectoTuberias1 : MonoBehaviourPun
{
    [SerializeField]
    private ParticleSystem TuberiaTank1;
    // Start is called before the first frame update
    
    public PhotonView PV;
    void Start()
    {

        if (!DataInfo.dataInfo.singlePractice)
        {
            PV = GetComponent<PhotonView>();
            Inicializate();
        }
        else
        {
            Inicializate();
        }
    }

    private void Inicializate()
    {
        gameObject.SetActive(false);
        TuberiaTank1 = GetComponent<ParticleSystem>();

    }
    [PunRPC]
    public void activeTuberia1(bool active)
    {
        if (active)
        {
            TuberiaTank1.gameObject.SetActive(active);
            TuberiaTank1.Play(active);
        }
        else
        {
            gameObject.SetActive(false);
            TuberiaTank1.gameObject.SetActive(false);
            TuberiaTank1.Play(false);
        }

    }
}
