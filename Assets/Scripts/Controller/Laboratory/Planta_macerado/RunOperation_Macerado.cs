﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Runtime.InteropServices;
using TMPro; //interactuar con labels nuevos
using System;
using Photon.Pun;

// PRIMERO


//runoperation realiza la interaccion entre la interfaz grafica y el valor que vamos a arrojar
public class RunOperation_Macerado : MonoBehaviourPun
{
    public static RunOperation_Macerado runOperation;
    [SerializeField]
    private Slider[] insp; //declaro el objeto de la interfaz grafica
    public Slider[] _insp { set { this.insp = value; } get { return insp; } }//declaro el objeto de la interfaz grafica

    [SerializeField]
    private TMP_Text[] label; //arreglo para los nuevos 3 labels

    [SerializeField]
    private Setting_Macerado seting;

    [SerializeField]
    private PhotonView PV;

    // Start is called before the first frame update
    void Start()
    {
        if (!DataInfo.dataInfo.singlePractice)
        {
            PV = GetComponent<PhotonView>();
            Inicializate();
        }
        else
        {
            Inicializate();
        }
    }
    // Update is called once per frame

    private void Inicializate()
    {
        seting = GameObject.Find("Setting").GetComponent<Setting_Macerado>();

        for (int i = 0; i < label.Length; i++)
        {
            label[i].text = System.Convert.ToString(0);
        }
    }
    void Update()
    {
        if (DataInfo.dataInfo.singlePractice)
        {
           Calculate();
        }
    }

    
    private void Calculate()
    {
        label[0].text = System.Convert.ToString(System.Math.Round(seting._a3, 2)); //asigno el valor a label haciendo que solo se muestre 2 decimales en el panel de contro
        label[1].text = System.Convert.ToString(System.Math.Round(seting._a3, 2)); //asigno el valor a label haciendo que solo se muestre 2 decimales en el instrumento que se encuentra en la planta

        label[2].text = System.Convert.ToString(System.Math.Round(seting._a4, 2)); //asigno el valor a label haciendo que solo se muestre 2 decimales en el panel de contro
        label[3].text = System.Convert.ToString(System.Math.Round(seting._a4, 2)); //asigno el valor a label haciendo que solo se muestre 2 decimales en el instrumento que se encuentra en la planta

        label[4].text = System.Convert.ToString(System.Math.Round(seting._volumen, 2)); //asigno el valor a label haciendo que solo se muestre 2 decimales DE LA H1
        label[5].text = System.Convert.ToString(System.Math.Round(seting._volumen2, 2)); //asigno el valor a label haciendo que solo se muestre 2 decimales DE LA H2

        label[6].text = System.Convert.ToString(System.Math.Round(seting._a1, 2)); //asigno el valor a label haciendo que solo se muestre 2 decimales en el panel de contro
        label[7].text = System.Convert.ToString(System.Math.Round(seting._a1, 2)); //asigno el valor a label haciendo que solo se muestre 2 decimales en el instrumento que se encuentra en la planta

        label[8].text = System.Convert.ToString(System.Math.Round(seting._a2, 2)); //asigno el valor a label haciendo que solo se muestre 2 decimales en el panel de contro
        label[9].text = System.Convert.ToString(System.Math.Round(seting._a2, 2)); //asigno el valor a label haciendo que solo se muestre 2 decimales en el instrumento que se encuentra en la planta

        label[10].text = System.Convert.ToString(System.Math.Round(seting._SP1, 2)); //asigno el valor a label haciendo que solo se muestre 2 decimales en el panel de contro del SP1
        label[11].text = System.Convert.ToString(System.Math.Round(seting._SP2, 2)); //asigno el valor a label haciendo que solo se muestre 2 decimales en el panel de contro del SP2

        label[10].text = System.Convert.ToString(System.Math.Round(seting._SP1, 2)); //asigno el valor a label haciendo que solo se muestre 2 decimales en el panel de contro del SP1
        label[11].text = System.Convert.ToString(System.Math.Round(seting._SP2, 2)); //asigno el valor a label haciendo que solo se muestre 2 decimales en el panel de contro del SP2

        label[12].text = System.Convert.ToString(System.Math.Round(seting._error1, 2)); //asigno el valor a label haciendo que solo se muestre 2 decimales en el panel de contro del error1
        label[13].text = System.Convert.ToString(System.Math.Round(seting._error2, 2)); //asigno el valor a label haciendo que solo se muestre 2 decimales en el panel de contro del error2

        label[14].text = System.Convert.ToString(System.Math.Round(seting._L1, 2)); //asigno el valor a label haciendo que solo se muestre 2 decimales en el panel de contro de los litros del tanque 1
        label[15].text = System.Convert.ToString(System.Math.Round(seting._L1max, 2)); //asigno el valor a label haciendo que solo se muestre 2 decimales en el panel de contro de los litros del tanque 2

        label[16].text = System.Convert.ToString(System.Math.Round(seting._L2, 2)); //asigno el valor a label haciendo que solo se muestre 2 decimales en el panel de contro de los litros del tanque 1
        label[17].text = System.Convert.ToString(System.Math.Round(seting._L2max, 2)); //asigno el valor a label haciendo que solo se muestre 2 decimales en el panel de contro de los litros del tanque 2
    }

    [PunRPC]
    private void MultiCalculate(float a1,float a2,float a3,float a4,float SP1,float SP2,float volumen, float volumen2,float error1, float error2, float l1max, float l1, float l2max, float l2)
    {
        label[0].text = System.Convert.ToString(System.Math.Round(a3, 2)); //asigno el valor a label haciendo que solo se muestre 2 decimales en el panel de contro
        label[1].text = System.Convert.ToString(System.Math.Round(a3, 2)); //asigno el valor a label haciendo que solo se muestre 2 decimales en el instrumento que se encuentra en la planta

        label[2].text = System.Convert.ToString(System.Math.Round(a4, 2)); //asigno el valor a label haciendo que solo se muestre 2 decimales en el panel de contro
        label[3].text = System.Convert.ToString(System.Math.Round(a4, 2)); //asigno el valor a label haciendo que solo se muestre 2 decimales en el instrumento que se encuentra en la planta

        label[4].text = System.Convert.ToString(System.Math.Round(volumen, 2)); //asigno el valor a label haciendo que solo se muestre 2 decimales DE LA H1
        label[5].text = System.Convert.ToString(System.Math.Round(volumen2, 2)); //asigno el valor a label haciendo que solo se muestre 2 decimales DE LA H2

        label[6].text = System.Convert.ToString(System.Math.Round(a1, 2)); //asigno el valor a label haciendo que solo se muestre 2 decimales en el panel de contro
        label[7].text = System.Convert.ToString(System.Math.Round(a1, 2)); //asigno el valor a label haciendo que solo se muestre 2 decimales en el instrumento que se encuentra en la planta

        label[8].text = System.Convert.ToString(System.Math.Round(a2, 2)); //asigno el valor a label haciendo que solo se muestre 2 decimales en el panel de contro
        label[9].text = System.Convert.ToString(System.Math.Round(a2, 2)); //asigno el valor a label haciendo que solo se muestre 2 decimales en el instrumento que se encuentra en la planta

        label[10].text = System.Convert.ToString(System.Math.Round(SP1, 2)); //asigno el valor a label haciendo que solo se muestre 2 decimales en el panel de contro del SP1
        label[11].text = System.Convert.ToString(System.Math.Round(SP2, 2)); //asigno el valor a label haciendo que solo se muestre 2 decimales en el panel de contro del SP2

        label[10].text = System.Convert.ToString(System.Math.Round(SP1, 2)); //asigno el valor a label haciendo que solo se muestre 2 decimales en el panel de contro del SP1
        label[11].text = System.Convert.ToString(System.Math.Round(SP2, 2)); //asigno el valor a label haciendo que solo se muestre 2 decimales en el panel de contro del SP2

        label[12].text = System.Convert.ToString(System.Math.Round(error1, 2)); //asigno el valor a label haciendo que solo se muestre 2 decimales en el panel de contro del error1
        label[13].text = System.Convert.ToString(System.Math.Round(error2, 2)); //asigno el valor a label haciendo que solo se muestre 2 decimales en el panel de contro del error2

        label[14].text = System.Convert.ToString(System.Math.Round(l1, 2)); //asigno el valor a label haciendo que solo se muestre 2 decimales en el panel de contro de los litros del tanque 1
        label[15].text = System.Convert.ToString(System.Math.Round(l1max, 2)); //asigno el valor a label haciendo que solo se muestre 2 decimales en el panel de contro de los litros del tanque 2

        label[16].text = System.Convert.ToString(System.Math.Round(l2, 2)); //asigno el valor a label haciendo que solo se muestre 2 decimales en el panel de contro de los litros del tanque 1
        label[17].text = System.Convert.ToString(System.Math.Round(l2max, 2)); //asigno el valor a label haciendo que solo se muestre 2 decimales en el panel de contro de los litros del tanque 2
    }
}
  
