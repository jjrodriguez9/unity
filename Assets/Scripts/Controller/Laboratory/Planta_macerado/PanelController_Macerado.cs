﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;


public class PanelController_Macerado : MonoBehaviourPun
{
    [SerializeField]
    private RunOperation_Macerado insp = new RunOperation_Macerado();
    [SerializeField]
    private PhotonView PV;
    private void OnTriggerStay(Collider other)
    {
        if (!DataInfo.dataInfo.singlePractice)
        {
            PV = GetComponent<PhotonView>();
            if (PV.IsMine)
            {
                ControlPanel(other.tag);
                ControlPanel2(other.tag);
                Perturvacion(other.tag);  //0208 16:26 creo colision para a4
            }
        }
        else
        {
            ControlPanel(other.tag);
            ControlPanel2(other.tag);
            Perturvacion(other.tag);  //0208 16:26 creo colision para a4
        }
       
    }

    void Start()
    {
        insp._insp[1].value = 0.1f;
        if (!DataInfo.dataInfo.singlePractice)
        {
            PV = GetComponent<PhotonView>();
        }
        
    }
    private void Perturvacion(string tag)
    {
        if (tag.Equals("Player"))
        {
            float value = Input.GetAxis("slider_move");
            if (!DataInfo.dataInfo.singlePractice)
            {
                PV.RPC("ValPerturvacion", RpcTarget.All, value);
            }
            else
            {
                ValPerturvacion(value);
            }
        }
    }
    [PunRPC]
    private void ValPerturvacion(float value)
    {
        if (value > 0)
        {
            insp._insp[2].value += 0.01f;
        }
        if (value < 0)
        {
            insp._insp[2].value -= 0.01f;

        }
    }


    private void ControlPanel(string tag)
    {
        if (tag.Equals("Player"))
        {
            float controllerUp = Input.GetAxis("active_x"); //colocando la configuracion de entrada de dispositivos
            bool activeUp = false, activeDown = false; //variables de activacion para la activacion
            if (!DataInfo.dataInfo.singlePractice)
            {
                PV.RPC("ValControlPanel", RpcTarget.All, controllerUp, activeUp, activeDown);
            }
            else
            {
                ValControlPanel(controllerUp, activeUp, activeDown);
            }
        }

    }
    [PunRPC]
    private void ValControlPanel(float controllerUp,bool activeUp,bool activeDown)
    {
        if (controllerUp > 0.6)
        {
            activeUp = true; //activando la variable
        }
        if (controllerUp < -0.6)
        {
            activeDown = true;
        }
        if (activeUp) //verificando cual de las entradas esta ejecutando para realizar el cambio del valor del slider
        {
            insp._insp[0].value += 0.01f; //aumento del valor del slider
        }
        if (activeDown)
        {
            insp._insp[0].value -= 0.01f; //disminucion del valor del slider
        }
    }


    private void ControlPanel2(string tag)
    {
        if (tag.Equals("Player"))
        {
            float controllerUp2 = Input.GetAxis("active_y"); //colocando la configuracion de entrada de dispositivos
            bool activeUp = false, activeDown = false; //variables de activacion para la activacion
            if (!DataInfo.dataInfo.singlePractice)
            {
                PV.RPC("ValControlPanel2", RpcTarget.All, controllerUp2, activeUp, activeDown);
            }
            else
            {
                ValControlPanel2(controllerUp2,activeUp,activeDown);
            }
        }
    }
    [PunRPC]
    private void ValControlPanel2(float controllerUp2,bool activeUp,bool activeDown)
    {
        if (controllerUp2 > 0.8)
        {
            activeUp = true; //activando la variable
        }
        if (controllerUp2 < -0.8)
        {
            activeDown = true;
        }
        if (activeUp) //verificando cual de las entradas esta ejecutando para realizar el cambio del valor del slider
        {
            insp._insp[1].value += 0.01f; //aumento del valor del slider
        }
        if (activeDown)
        {
            insp._insp[1].value -= 0.01f; //disminucion del valor del slider
        }
    }
}

   
  


