﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class LlenadoTank2 : MonoBehaviourPun
{
    [SerializeField]
    private ParticleSystem llenadoTank2;
    // Start is called before the first frame update
    public PhotonView PV;

    void Start()
    {
        if (!DataInfo.dataInfo.singlePractice)
        {
            PV = GetComponent<PhotonView>();
            Inicializate();
        }
        else
        {
            Inicializate();
        }
    }

    private void Inicializate()
    {
        gameObject.SetActive(false);
        llenadoTank2 = GetComponent<ParticleSystem>();
    }
    [PunRPC]
    public void activellenado2(bool active)
    {
        if (active)
        {
            llenadoTank2.gameObject.SetActive(active);
            llenadoTank2.Play(active);
        }
        else
        {
            gameObject.SetActive(false);
            llenadoTank2.gameObject.SetActive(false);
            llenadoTank2.Play(false);
        }

    }
}
