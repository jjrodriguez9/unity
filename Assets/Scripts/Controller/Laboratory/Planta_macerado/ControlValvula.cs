﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlValvula : MonoBehaviour
{
    [SerializeField]
    private RunOperation_Macerado insp = new RunOperation_Macerado();
    private void OnTriggerStay(Collider other)
    {
        Perturvacion2(other.tag);
    }
    private void Perturvacion2(string tag)
    {
        if (tag.Equals("Player"))
        {
            float value = Input.GetAxis("slider_move");
            if (value > 0)
            {
                insp._insp[2].value += 0.01f;
            }
            if (value < 0)
            {
                insp._insp[2].value -= 0.01f;

            }
        }
    }
}
