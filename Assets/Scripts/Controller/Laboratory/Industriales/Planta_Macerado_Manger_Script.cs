﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Planta_Macerado_Manger_Script : MonoBehaviour
{
    private void Awake()
    {
        if (SceneManager.GetActiveScene().buildIndex == MultiplayerSetting.multiplayerSetting.lobby)
        {
            GetComponent<Model_Maths>().enabled = false;
            GetComponent<TankFilling_Macerado>().enabled = false;
            GetComponent<RunOperation_Macerado>().enabled = false;
            GetComponent<Graph_Planta_Macerado>().enabled = false;
        }
    }
}
