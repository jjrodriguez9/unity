﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Planta_Macerado_Manager : MonoBehaviour
{
    [SerializeField]
    private GameObject structure;
    
    private void Awake()
    {
        if (SceneManager.GetActiveScene().buildIndex == MultiplayerSetting.multiplayerSetting.lobby)
        {
            //Panel_Control
            structure = gameObject.transform.Find("Panel_Control").gameObject.transform.Find("Colisionador").gameObject;
            structure.SetActive(false);
        }
    }
}
