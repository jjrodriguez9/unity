﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class Tanque_Serie_Manager : MonoBehaviour
{
    [SerializeField]
    private GameObject structure,interfaces;
    // Start is called before the first frame update
    void Start()
    {
        if (SceneManager.GetActiveScene().buildIndex == MultiplayerSetting.multiplayerSetting.lobby)
        {
            structure = gameObject.transform.Find("Interface").gameObject;
            interfaces = gameObject.transform.Find("Environment").gameObject.transform.Find("Touch Proceso").gameObject;
            structure.SetActive(false);
            interfaces.SetActive(false);
        }
    }
}
