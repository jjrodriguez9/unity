﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyFunctionsKUKA : MonoBehaviour
{
    /*
     * Parametros de D-H a excepción de teta 
     */
    private static float[] d = new float[] { 0.4f, 0f, 0f, 0.42f, 0f, 0.08f };
    private static float[] a = new float[] { 0.025f, 0.455f, 0.035f, 0f, 0f, 0f };
    //private static float[] alfa = new float[] { Mathf.PI / 2f, Mathf.PI, -Mathf.PI / 2f, Mathf.PI / 2f, Mathf.PI / 2f, 0f };
    private static float[] alfa = new float[] { Mathf.PI / 2f, 0, Mathf.PI / 2f, -Mathf.PI / 2f, Mathf.PI / 2f, 0f };


    // Function to get the XYZ coordinates from KUKA
    public static float[] Get_DKinematic_KUKA(float[] q)
    {
        float q1 = q[0];
        float q2 = q[1];
        float q3 = q[2];
        float q4 = q[3];
        float q5 = q[4];
        float q6 = q[5];

        float t2 = Mathf.Cos(q1);
        float t3 = Mathf.Cos(q2);
        float t4 = Mathf.Sin(q3);
        float t5 = Mathf.Cos(q3);
        float t6 = Mathf.Sin(q2);
        float t7 = Mathf.Sin(q1);
        float t8 = Mathf.Sin(q5);
        float t9 = Mathf.Sin(q4);
        float t10 = Mathf.Cos(q4);
        float t11 = Mathf.Cos(q5);



        float[] P_6 = new float[] { t2*(1f/40f)+t8*(t7*t9+t10*(t2*t3*t5-t2*t4*t6))*(2f/25f)+t2*t3*(91f/200f)+t11*(t2*t3*t4+t2*t5*t6)*(2f/25f)+t2*t3*t4*(21f/50f)+t2*t3*t5*(7f/200f)-t2*t4*t6*(7f/200f)+t2*t5*t6*(21f/50f),
                                    t7*(1f/40f)-t8*(t2*t9-t10*(t3*t5*t7-t4*t6*t7))*(2f/25f)+t3*t7*(91f/200f)+t11*(t3*t4*t7+t5*t6*t7)*(2f/25f)+t3*t4*t7*(21f/50f)+t3*t5*t7*(7f/200f)-t4*t6*t7*(7f/200f)+t5*t6*t7*(21f/50f),
                                    t6*(91f/200f)+t3*t4*(7f/200f)-t3*t5*(21f/50f)+t4*t6*(21f/50f)+t5*t6*(7f/200f)-t11*(t3*t5-t4*t6)*(2f/25f)+t8*t10*(t3*t4+t5*t6)*(2f/25f)+2f/5f
                                  };

        //Debug.Log("A6 Vector XYZ: " + P_6[0] + "  " + P_6[1] + "  " + P_6[2]);

        return P_6;
    }

    // Function to Jacobian for KUKA
    public static float[,] Jacobian_KUKA(float[] q)
    {
        

        return new float[,] { { 0f, 0f }, { 0f, 0f} };
    }

    // Función que multiplica dos matrices
    public static float[,] Matriz_Mul_Matriz(float[,] in_M1, float[,] in_M2)
    {
        int[] sizeM1 = new int[] { in_M1.GetLength(0), in_M1.GetLength(1)};
        int[] sizeM2 = new int[] { in_M2.GetLength(0), in_M2.GetLength(1) };

        float[,] out_M = new float[sizeM1[0], sizeM2[1]];

        if (sizeM1[1] == sizeM2[0])
        {

            float acu;

            for (int i = 0; i < sizeM1[0]; i++)
            {
                //Debug.Log("i: " + i);
                for (int j = 0; j < sizeM2[1]; j++)
                {
                    //Debug.Log("j: " + j);
                    acu = 0f;
                    for (int k = 0; k < sizeM2[0]; k++)
                    {
                        acu = acu + (in_M1[i, k] * in_M2[k, j]);
                        //Debug.Log("k: " + k);
                    }
                    //Debug.Log("acu: " + acu);
                    out_M[i, j] = acu;
                }
            }
            //Debug.Log(out_M[0,0] + "   " + out_M[1,0]);            
        }
        else
            Debug.LogError("Size of matrix A and B are not consistent for multiplication.");

        return out_M;
    }

    // Función que multiplica una matriz por un escalar
    public static float[,] Matriz_Mul_Escalar(float[,] in_M, float s)
    {
        int row = in_M.GetLength(0);
        int col = in_M.GetLength(1);

        float[,] out_M = new float[row, col];

        for (int i = 0; i < row; i++)
        {
            for (int j = 0; j < col; j++)
            {
                out_M[i, j] = in_M[i, j] * s;
            }
        }
        
        return out_M;
    }

    // Función que multiplica una matriz por un escalar
    public static float[] Vector_Mul_Escalar(float[] in_V, float s)
    {
        int size_V = in_V.GetLength(0);

        float[] out_V = new float[size_V];

        for (int i = 0; i < size_V; i++)
        {
            out_V[i] = in_V[i] * s;
        }

        return out_V;
    }

    // Función que divide una matriz por un escalar
    public static float[,] Matriz_Div_Escalar(float[,] in_M, float d)
    {
        int row = in_M.GetLength(0);
        int col = in_M.GetLength(1);

        float[,] out_M = new float[row, col];

        for (int i = 0; i < row; i++)
        {
            for (int j = 0; j < col; j++)
            {
                out_M[i, j] = in_M[i, j] / d;
            }
        }

        return out_M;
    }

    // Función que divide un vector por un escalar
    public static float[] Vector_Div_Escalar(float[] in_V, float d)
    {
        int size_V = in_V.GetLength(0);

        float[] out_V = new float[size_V];

        for (int i = 0; i < size_V; i++)
        {
            out_V[i] = in_V[i] / d;
        }

        return out_V;
    }

    // Función que suma dos matrices
    public static float[,] Matriz_Sum_Matriz(float[,] in_M1, float[,] in_M2)
    {
        int[] sizeM1 = new int[] { in_M1.GetLength(0), in_M1.GetLength(1) };
        int[] sizeM2 = new int[] { in_M2.GetLength(0), in_M2.GetLength(1) };

        float[,] out_M = new float[sizeM1[0], sizeM1[1]];

        if ((sizeM1[0] == sizeM2[0]) && (sizeM1[1] == sizeM2[1]))
        {
            for (int i = 0; i < sizeM1[0]; i++)
            {
                for (int j = 0; j < sizeM1[1]; j++)
                {
                    out_M[i, j] = in_M1[i, j] + in_M2[i, j];
                }
            }
        }
        else
            Debug.LogError("Size of matrix A and B are not consistent for sum.");

        return out_M;
    }

    // Función que resta dos matrices
    public static float[,] Matriz_Rest_Matriz(float[,] in_M1, float[,] in_M2)
    {
        int[] sizeM1 = new int[] { in_M1.GetLength(0), in_M1.GetLength(1) };
        int[] sizeM2 = new int[] { in_M2.GetLength(0), in_M2.GetLength(1) };

        float[,] out_M = new float[sizeM1[0], sizeM1[1]];

        if ((sizeM1[0] == sizeM2[0]) && (sizeM1[1] == sizeM2[1]))
        {
            for (int i = 0; i < sizeM1[0]; i++)
            {
                for (int j = 0; j < sizeM1[1]; j++)
                {
                    out_M[i, j] = in_M1[i, j] - in_M2[i, j];
                }
            }
        }
        else
            Debug.LogError("Size of matrix A and B are not consistent for rest.");

        return out_M;
    }

    // Función que resta dos vectorres
    public static float[] Vector_Rest_Vector(float[] in_V1, float[] in_V2)
    {
        int sizeV1 = in_V1.GetLength(0);
        int sizeV2 = in_V2.GetLength(0);

        float[] out_V = new float[sizeV1];

        if (sizeV1 == sizeV2)
        {
            for (int i = 0; i < sizeV1; i++)
            {
                out_V[i] = in_V1[i] - in_V2[i];
            }
        }
        else
            Debug.LogError("Size of Vector A and B are not consistent for rest.");

        return out_V;
    }

    // Función que suma dos vectorres
    public static float[] Vector_Sum_Vector(float[] in_V1, float[] in_V2)
    {
        int sizeV1 = in_V1.GetLength(0);
        int sizeV2 = in_V2.GetLength(0);

        float[] out_V = new float[sizeV1];

        if (sizeV1 == sizeV2)
        {
            for (int i = 0; i < sizeV1; i++)
            {
                out_V[i] = in_V1[i] + in_V2[i];
            }
        }
        else
            Debug.LogError("Size of Vector A and B are not consistent for rest.");

        return out_V;
    }

    // Función que suma una matriz con escalar
    public static float[,] Matriz_Sum_Escalar(float[,] in_M, float s)
    {
        int row = in_M.GetLength(0);
        int col = in_M.GetLength(1);

        float[,] out_M = new float[row, col];

        for (int i = 0; i < row; i++)
        {
            for (int j = 0; j < col; j++)
            {
                out_M[i, j] = in_M[i, j] + s;
            }
        }
        
        return out_M;
    }

    // Función para calcular el error de la posición del efector final del KUKA respecto al Set Point
    public static float[] Get_Error(float[] sp, float[] pos)
    {
        float[] error = new float[sp.Length];
        if (sp.Length == pos.Length)
        {
            for (int i = 0; i < sp.Length; i++)
            {
                error[i] = sp[i] - pos[i];
            }
        }

        return error;
    }

    private static float[,] ri0pi(float a = 0, float d = 0, float alfa = 0)
    {
        // RI0PI         Vector ri0pi.
        // Y = RI0PI(A, D, ALFA) calcula el vector 3x1 que representa la
        // localización de(xi, yi, zi) desde el origen de(xi-1,yi - 1,zi - 1) 
        // con respecto al sistema de coordenadas i-ésimo utilizando los
        // parámetros de Denavit-Hartenberg A, D y ALFA.

        return new float[,] { { a },
            { d * Mathf.Sin(alfa) },
            { d * Mathf.Cos(alfa) } };
    }

    public static float[,] zeros(int n, int m)
    {
        return new float[n, m];
    }

    private static float[,] ri0si(float a = 0f, float d = 0f, float alfa = 0f, float factor = 0f)
    {
        // RI0SI         Vector ri0si.
        // Y = RI0SI(A, D, ALFA, FACTOR) calcula el vector 3x1 que representa
        // la  posición del centro de masa del elemento i respecto de su sistema
        // de coordenadas utilizando los parámetros de Denavit-Hartenberg A, D
        // y ALFA.FACTOR es un escalar que se utiliza para ubicar el centro de masa.

        return new float[,] { { factor * a },
            { factor * d * Mathf.Sin(alfa) },
            { factor * d * Mathf.Cos(alfa) } };
    }

    public static float[,] dh(float teta, float alfa)
    {
        // DH      Matriz de rotación.
        // R = DH(TETA, ALFA) devuelve la matriz de rotación 3x3 utilizando
        // los parámetros de Denavit - Hartenberg TETA y ALFA.

        return new float[,] {{Mathf.Cos(teta), -Mathf.Cos(alfa)*Mathf.Sin(teta),  Mathf.Sin(alfa)*Mathf.Sin(teta) },
                            { Mathf.Sin(teta),  Mathf.Cos(alfa)*Mathf.Cos(teta), -Mathf.Sin(alfa)*Mathf.Cos(teta) },
                            {              0f,                  Mathf.Sin(alfa),                  Mathf.Cos(alfa) } };
    }


    public static float[,] Matriz_Extraer_Rango(float[,] in_M, int row, int n, int col, int m)
    {
        float[,] out_M = new float[n,m];

        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < m; j++)
            {
                out_M[i, j] = in_M[row + i, col + j];
            }
        }

        return out_M;
    }

    public static float[,] tras(float[,] r)
    {
        // Entrega la matriz traspuesta de r

        int n = r.GetLength(0);  // Filas
        int m = r.GetLength(1);  // Columnas
        float[,] r_tras = new float[m, n];

        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < m; j++)
            {
                r_tras[j, i] = r[i, j];
            }
        }

        return r_tras;
    }

    public static float[,] eye(int n)
    { 
        // Entrega una matriz identidad de n x n

        float[,] e = new float[n, n];

        for (int i = 0; i < n; i++)
        {
            e[i, i] = 1f;
        }

        return e;
    }


    private static float[,] ri0wi(float[,] iri_1, float[,] i_1r0wi_1, float qpi)
    {
        // RI0WI         Vector ri0wi.
        // Y = RI0WI(IRI_1, I_1R0WI_1, QPI) calcula el vector 3x1 que representa
        // la  velocidad angular de la articulación i-ésima respecto al sistema de
        // coordenadas i - ésimo.IRI_1 es la matriz de rotación del sistema de
        // coordenadas i - ésimo al(i - 1) - ésimo.I_1R0WI_1 es el vector 3x1 de velocidad
        // angular de la articulación(i - 1) - ésima.QPI es la magnitud de la velocidad
        // angular de la articulación i-ésima respecto del elemento i-1.

        // y = iri_1 * (i_1r0wi_1 + z * qpi);

        float[,] z = new float[,] { { 0f }, { 0f }, { 1f } };

        float[,] m1 = Matriz_Mul_Escalar(z, qpi);
        float[,] m2 = Matriz_Sum_Matriz (i_1r0wi_1, m1);
        
        return Matriz_Mul_Matriz(iri_1, m2);
    }

    public static float[,] cross_vec_x3(float[,] a, float[,] b)
    {
        // Calcula el producto cruz de dos vectores cada uno de 3 elementos
        Vector3 vec_a = new Vector3();
        Vector3 vec_b = new Vector3();
        Vector3 vec_o = new Vector3();

        int[] size_a = new int[] { a.GetLength(0), b.GetLength(1) };
        int[] size_b = new int[] { a.GetLength(0), b.GetLength(1) };

        if ((size_a[0] == size_b[0]) && (size_a[1] == size_b[1]))
        {
            if (size_a[0] > 1)
            {
                vec_a = new Vector3(a[0, 0], a[1, 0], a[2, 0]);
                vec_b = new Vector3(b[0, 0], b[1, 0], b[2, 0]);
            }
            else
            {
                vec_a = new Vector3(a[0, 0], a[0, 1], a[0, 2]);
                vec_b = new Vector3(b[0, 0], b[0, 1], b[0, 2]);
            }
            vec_o = Vector3.Cross(vec_a, vec_b);
        }
        else
            Debug.LogError("Size of matrix A and B are not consistent for Cross.");

        return new float[,] { { vec_o.x}, {vec_o.y}, {vec_o.z} };
    }

    public static float dot_vector_x3(float[,] a, float[,] b)
    {
        // Calcula el producto punto de dos vectores, cada uno de 3 elementos
        Vector3 vec_a = new Vector3();
        Vector3 vec_b = new Vector3();
        float dot_o = 0f;

        int[] size_a = new int[] { a.GetLength(0), b.GetLength(1) };
        int[] size_b = new int[] { a.GetLength(0), b.GetLength(1) };

        if ((size_a[0] == size_b[0]) && (size_a[1] == size_b[1]))
        {
            if (size_a[0] > 1)
            {
                vec_a = new Vector3(a[0, 0], a[1, 0], a[2, 0]);
                vec_b = new Vector3(b[0, 0], b[1, 0], b[2, 0]);
            }
            else
            {
                vec_a = new Vector3(a[0, 0], a[0, 1], a[0, 2]);
                vec_b = new Vector3(b[0, 0], b[0, 1], b[0, 2]);
            }
            dot_o = Vector3.Dot(vec_a, vec_b);
        }
        else
            Debug.LogError("Size of matrix A and B are not consistent for dot product.");

        return dot_o;
    }

    private static float[,] ri0wpi(float[,] iri_1, float[,] i_1r0wpi_1, float[,] i_1r0wi_1, float qpi, float qppi)
    {
        // RI0WPI        Vector ri0wpi.
        // Y = RI0WPI(IRI_1, I_1R0WPI_1, I_1R0WI_1, QPI, QPPI) calcula el vector
        // 3x1 que representa la aceleración angular de la articulación i-ésima
        // respecto al sistema de coordenadas i - ésimo.IRI_1 es la matriz de rotación
        // del sistema de coordenadas i-ésimo al(i - 1) - ésimo.I_1R0WPI_1 es el vector
        // 3x1 de aceleración angular de la articulación(i - 1) - ésima.I_1R0WI_1 es el
        // vector 3x1 de velocidad angular de la articulación(i - 1) - ésima.QPI es la
        // magnitud de la velocidad angular de la articulación i-ésima respecto del
        // elemento i - 1.QPPI es la magnitud de la aceleración angular de la articulación
        // i - ésima respecto del elemento i-1.

        float[,] z = new float[,] { { 0f }, { 0f }, { 1f } };
        
        float[,] m1 = Matriz_Mul_Escalar(z, qpi);
        float[,] m2 = cross_vec_x3(i_1r0wi_1, m1);
        float[,] m3 = Matriz_Mul_Escalar(z, qppi);
        float[,] m4 = Matriz_Sum_Matriz(m3, m2);
        float[,] m5 = Matriz_Sum_Matriz(i_1r0wpi_1, m4);

        return Matriz_Mul_Matriz(iri_1, m5);
    }

    private static float[,] ri0vpi_r(float[,] iri_1, float[,] i_1r0vpi_1, float[,] ir0wpi, float[,] ir0wi, float[,] ir0pi)
    {
        // RI0VPI_R      Vector ri0vpi_r.
        // Y = RI0VPI_R(IRI_1, I_1R0VPI_1, IR0WPI, IR0WI, IR0PI) calcula el
        // vector 3x1 que representa la aceleración lineal de la articulación
        // rotacional i - ésima respecto al sistema de coordenadas i-ésimo.IRI_1
        // es la matriz de rotación del sistema de coordenadas i - ésimo al(i - 1) - ésimo.
        // I_1R0VPI_1 es el vector 3x1 de aceleración lineal de la articulación
        // (i - 1) - ésima.IR0WPI es el vector 3x1 de aceleración angular de la articulación
        // i - ésima.IR0WI es el vector 3x1 de velocidad angular de la articulación
        // i - ésima.IRIPI es el vector 3x1 de localización de(xi, yi, zi) desde el
        // origen de(xi - 1, yi - 1, zi - 1) con respecto al sistema de coordenadas i - ésimo.
        //
        // See also RI0WPI, RI0WI, RI0PI.

        float[,] a = cross_vec_x3(ir0wpi, ir0pi);
        float[,] b = cross_vec_x3(ir0wi, ir0pi);
        float[,] c = cross_vec_x3(ir0wi, b);
        float[,] m1 = Matriz_Sum_Matriz(a, c);
        float[,] m2 = Matriz_Mul_Matriz(iri_1, i_1r0vpi_1);

        return Matriz_Sum_Matriz(m1, m2);
    }

    private static float[,] ri0ai(float[,] ir0vpi, float[,] ir0wpi, float[,] ir0wi, float[,] ir0si)
    {
        // RI0AI         Vector ri0ai.
        // Y = RI0AI(IR0VPI, IR0WPI, IR0WI, IR0SI) calcula el vector 3x1 que
        // representa la aceleración lineal del centro de masa del elemento i. 
        // IR0VPI es el vector 3x1 de aceleración lineal de la articulación i-ésima.
        // IR0WPI es el vector 3x1 de aceleración angular de la articulación i-ésima.
        // IR0WI es el vector 3x1 de velocidad angular de la articulación i-ésima.
        // IR0SI es el vector 3x1 de la posición del centro de masa del elemento i
        // respecto de su sistema de coordenadas.
        //
        // See also RI0VPI_r, RI0WPI, RI0WI, RI0SI.


        float[,] a = cross_vec_x3(ir0wi, ir0si);
        float[,] b = cross_vec_x3(ir0wi, a);
        float[,] c = cross_vec_x3(ir0wpi, ir0si);
        float[,] m1 = Matriz_Sum_Matriz(c, b);

        return Matriz_Sum_Matriz(m1, ir0vpi);
    }

    private static float[,] ri0fi(float[,] ri0ai, float mi)
    {
        // RI0FI         Vector ri0fi.
        // Y = RI0FI(RI0AI, MI) calcula el vector 3x1 de fuerzas que actúan
        // sobre el elemento i en el centro de masa. RI0AI representa la
        // aceleración lineal en el centro de masa del elemento i.MI es la
        // masa del elemento i.
        //
        // See also RI0AI.

        return Matriz_Mul_Escalar(ri0ai, mi);
    }

    private static float[,] ri0ni(float[,] ir0wpi, float[,] ir0wi, float[,] ir0I_0ri)
    {
        // RI0NI         Vector ri0ni.
        // Y = RI0NI(IR0WPI, IR0WI, IR0I_0RI) calcula el vector 3x1 que representa
        // el momento externo ejercido sobre el elemento i en el centro de masa. 
        // IR0WPI es el vector 3x1 de aceleración angular de la articulación i-ésima.
        // IR0WI es el vector 3x1 de velocidad angular de la articulación i-ésima.
        // IR0I_0RI es la matriz 3x3 que representa la inercia del elemento i.
        //
        // See also RI0WPI, RI0WI.

        float[,] m1 = Matriz_Mul_Matriz(ir0I_0ri, ir0wpi);
        float[,] m2 = Matriz_Mul_Matriz(ir0I_0ri, ir0wi);
        float[,] m3 = cross_vec_x3(ir0wi, m2);

        return Matriz_Sum_Matriz(m1, m3);
    }

    private static float[,] ri0fia(float[,] iri1, float[,] i1r0fi1a, float[,] ir0fi)
    {
        // RI0FIA        Vector ri0fia.
        // Y = RI0FIA(IRI1, I1R0FI1A, IR0FI) calcula el vector 3x1 que
        // representa la fuerza ejercida sobre el elemento i por el
        // elemento(i - 1).IRI1    es la matriz de rotación del sistema de
        // coordenadas i - ésimo al(i + 1) - ésimo.I1R0FI1A es el vector 3x1
        // de fuerza ejercida sobre el elemento(i + 1) por el elemento i.
        // IR0FI es el vector 3x1 de fuerzas que actúan sobre el elemento
        // i en el centro de masa.
        //
        // See also RI0FI.

        float[,] m1 = Matriz_Mul_Matriz(iri1, i1r0fi1a);
        
        return Matriz_Sum_Matriz(m1, ir0fi);
    }


    private static float[,] ri0nia(float[,] iri1, float[,] i1r0ni1a, float[,] i1r0fi1a, float[,] ir0ni, float[,] ir0fi, float[,] i1r0pi, float[,] ir0pi, float[,] ir0si)
    {
        // RI0NIA        Vector ri0nia.
        // Y = RI0NIA(IRI1, I1R0NI1A, I1R0FI1A, IR0NI, IR0FI, I1R0PI, IR0PI, IR0SI)
        // calcula el vector 3x1 que representa el momento ejercido sobre el elemento
        // i por el elemento i-1.IRI1 es la matriz de rotación del sistema de
        // coordenadas i - ésimo al(i + 1) - ésimo.I1R0NI1A es el vector 3x1 de momento
        // ejercido sobre el elemento(i + 1) por el elemento i. I1R0FI1A es el vector
        // 3x1 de fuerza ejercida sobre el elemento(i + 1) por el elemento i. IR0NI es
        // el vector 3x1 de fuerza externa que actúa sobre el elemento i en el centro
        // de masa.IR0FI es el vector 3x1 de fuerza externa que actúa sobre el elemento
        // i en el centro de masa.I1R0PI es el vector 3x1 de localización del sistema
        // de referencia(i + 1) - ésimo desde el i - ésimo.IR0PI es el vector 3x1 de
        // localización del sistema de referencia i - ésimo desde el(i-1)-ésimo.
        // IR0SI es el vector 3x1 de la posición del centro de masa del elemento i
        // respecto de su sistema de coordenadas.
        //
        // See also RI0FIA, RI0NI, RI0FI, RI0PI, RI0SI.

        float[,] m1 = cross_vec_x3(i1r0pi, i1r0fi1a);
        float[,] m2 = Matriz_Sum_Matriz(i1r0ni1a, m1);
        float[,] m3 = Matriz_Mul_Matriz(iri1, m2);

        float[,] m4 = Matriz_Sum_Matriz(ir0pi, ir0si);
        float[,] m5 = cross_vec_x3(m4, ir0fi);
        float[,] m6 = Matriz_Sum_Matriz(m5, ir0ni);

        return Matriz_Sum_Matriz(m3, m6);
    }


    private static float t_r(float[,] iri_1, float[,] ir0nia, float qpi, float bi)
    {
        // T_R       Vector de pares articulares.
        // Y = T_R(IRI_1, IR0NIA, QPI, BI) calcula el vector 3x1 que representa
        // el par de entrada a la articulación i.IRI_1 es la matriz de rotación
        // del sistema de coordenadas i-ésimo al(i - 1) - ésimo.IR0NIA es el vector
        // 3x1 de momento ejercido sobre el elemento i por el elemento i-1.QPI es
        // la magnitud de la velocidad angular de la articulación i - ésima respecto
        // del elemento i-1.BI es el coeficiente de amortiguamiento viscoso para
        // la articulación i.
        //
        // See also RI0NIA.

        float[,] z = new float[,] { { 0f }, { 0f }, { 1f } };

        float[,] m1 = tras(ir0nia);
        float[,] m2 = Matriz_Mul_Matriz(iri_1, z);
        float s = bi * qpi;

        float m3 = Matriz_Mul_Matriz(m1, m2)[0, 0];

        return (m3 + s);
    }


    // Función para calcular la Dinámica Inversa del Robot KUKA KR6 R900 según el método de Newton-Euler para 6 DOF
    public static float[] newtoneuler6(float[] q = null, float[] qp = null, float[] qpp = null, float g = 9.8f, float[] m = null, float m7 = 0.5f, float[,] Iexter = null)
    {
        /*NEWTONEULER6 Dinámica inversa utilizando el método de Newton - Euler.
        * TAU = NEWTONEULER6(Q, QP, QPP, G, M7, IEXTER) calcula el vector
        * 6x1 de pares / fuerzas de entrada a las articulaciones.
        * teta: el vetor 6x1 obtenido de la matriz D-H.
        * d: el vector 6x1 obtenido de la matriz D-H
        * a: el vector 6x1 obtenido de la matriz D-H.
        * alfa: el vector 6x1 obtenido de la matriz D-H.
        * q el vector 6x1 de coordenadas articulares. 
        * qp es el vector 6x1 que representa la velocidad de cada articulación.
        * qpp es el vector 6x1 que indica la aceleración de cada articulación. 
        * g es el valor de la gravedad(m / s ^ 2).
        * m es el vector de masas de cada una de las artículaciones
        * m7 es la masa de la carga externa(Kg) que transporta el brazo robot.
        * IEXTER es la matriz 3x3 de inercia de la carga exterior(Kg - m ^ 2).
        *
        * 
        * 
        * See also functions DH, RI0PI, RI0SI, RI0WI, RI0WIP, RI0VPI_R, RI0AI, RI0FI, RI0NI,
        * RI0FIA, RI0NIA, T_R.
        */

        // ------------------------------------------------------------
        // Parámetros de D-H del robot KUKA KR6 R900
        // ------------------------------------------------------------
        //float[] teta = new float[] { q[0], q[1] + Mathf.PI / 2f, q[2], q[3], q[4] + Mathf.PI, q[5] };
        float[] teta = new float[] { q[0], q[1], q[2], q[3], q[4], q[5] };
        //float[] d = new float[] { 0.4f, 0f, 0f, 0.42f, 0f, 0.08f };
        //float[] a = new float[] { 0.025f, 0.455f, 0.035f, 0f, 0f, 0f };
        //float[] alfa = new float[] { Mathf.PI / 2f, Mathf.PI, -Mathf.PI / 2f, Mathf.PI / 2f, Mathf.PI / 2f, 0f };
        // ------------------------------------------------------------
        // Factores de posicionamiento de los centros de gravedad
        // ------------------------------------------------------------
        float factor1 = -0.5f; float factor2 = -0.5f; float factor3 = -0.5f;
        float factor4 = -0.5f; float factor5 = -0.5f; float factor6 = -0.5f;
        // ------------------------------------------------------------
        // Masa de cada elemento(Kg)
        // ------------------------------------------------------------
        float m1 = m[0]; float m2 = m[1]; float m3 = m[2];
        float m4 = m[3]; float m5 = m[4]; float m6 = m[5];
        // ------------------------------------------------------------
        // Coeficiente de rozamiento viscoso de cada articulacion
        // ------------------------------------------------------------
        float b1 = 0.05f; float b2 = 0.05f; float b3 = 0.05f;
        float b4 = 0.05f; float b5 = 0.05f; float b6 = 0.05f;
        // ------------------------------------------------------------
        // Matrices de Inercia(Kg-m ^ 2)
        // ------------------------------------------------------------
        float[,] r10I_r01 = new float[,] { { 0.0084f, 0f, 0f }, { 0f, 0.0064f, 0f }, { 0f, 0f, 0.0084f } };
        float[,] r20I_r02 = new float[,] { { 0.0078f, 0f, 0f }, { 0f, 0.0021f, 0f }, { 0f, 0f, 0.0021f } };
        float[,] r30I_r03 = new float[,] { { 0.0016f, 0f, 0f }, { 0f, 0.0462f, 0f }, { 0f, 0f, 0.0462f } };
        float[,] r40I_r04 = new float[,] { { 0.0016f, 0f, 0f }, { 0f, 0.0016f, 0f }, { 0f, 0f, 0.0009f } };
        float[,] r50I_r05 = new float[,] { { 0.0016f, 0f, 0f }, { 0f, 0.0016f, 0f }, { 0f, 0f, 0.0009f } };
        float[,] r60I_r06 = new float[,] { { 0.0001f, 0f, 0f }, { 0f, 0.0001f, 0f }, { 0f, 0f, 0.0001f } };
        // ------------------------------------------------------------
        // Vectores ri0pi, ri0si.
        // ------------------------------------------------------------
        float[,] r10p1 = ri0pi(a[0], d[0], alfa[0]);
        float[,] r20p2 = ri0pi(a[1], d[1], alfa[1]);
        float[,] r30p3 = ri0pi(a[2], d[2], alfa[2]);
        float[,] r40p4 = ri0pi(a[3], d[3], alfa[3]);
        float[,] r50p5 = ri0pi(a[4], d[4], alfa[4]);
        float[,] r60p6 = ri0pi(a[5], d[5], alfa[5]);
        float[,] r70p7 = zeros(3, 1);
        float[,] r10s1 = ri0si(a[0], d[0], alfa[0], factor1);
        float[,] r20s2 = ri0si(a[1], d[1], alfa[1], factor2);
        float[,] r30s3 = ri0si(a[2], d[2], alfa[2], factor3);
        float[,] r40s4 = ri0si(a[3], d[3], alfa[3], factor4);
        float[,] r50s5 = ri0si(a[4], d[4], alfa[4], factor5);
        float[,] r60s6 = ri0si(a[5], d[5], alfa[5], factor6);
        float[,] r70s7 = zeros(3, 1);
        // ------------------------------------------------------------
        // Matrices de transformacion
        // ------------------------------------------------------------
        float[,] r01 = dh(teta[0], alfa[0]); float[,] r10 = tras(r01);
        float[,] r12 = dh(teta[1], alfa[1]); float[,] r21 = tras(r12);
        float[,] r23 = dh(teta[2], alfa[2]); float[,] r32 = tras(r23);
        float[,] r34 = dh(teta[3], alfa[3]); float[,] r43 = tras(r34);
        float[,] r45 = dh(teta[4], alfa[4]); float[,] r54 = tras(r45);
        float[,] r56 = dh(teta[5], alfa[5]); float[,] r65 = tras(r56);
        float[,] r67 = eye(3); float[,] r76 = tras(r67);
        // ------------------------------------------------------------
        // Velocidad angular de las articulaciones
        // ------------------------------------------------------------
        float[,] r00w0 = zeros(3, 1);
        float[,] r10w1 = ri0wi(r10, r00w0, qp[0]);
        float[,] r20w2 = ri0wi(r21, r10w1, qp[1]);
        float[,] r30w3 = ri0wi(r32, r20w2, qp[2]);
        float[,] r40w4 = ri0wi(r43, r30w3, qp[3]);
        float[,] r50w5 = ri0wi(r54, r40w4, qp[4]);
        float[,] r60w6 = ri0wi(r65, r50w5, qp[5]);
        float[,] r70w7 = ri0wi(r76, r60w6, 0f);
        // ------------------------------------------------------------
        // Aceleracion angular de las articulaciones
        // ------------------------------------------------------------
        float[,] r00wp0 = zeros(3, 1);
        float[,] r10wp1 = ri0wpi(r10, r00wp0, r00w0, qp[0], qpp[0]);
        float[,] r20wp2 = ri0wpi(r21, r10wp1, r10w1, qp[1], qpp[1]);
        float[,] r30wp3 = ri0wpi(r32, r20wp2, r20w2, qp[2], qpp[2]);
        float[,] r40wp4 = ri0wpi(r43, r30wp3, r30w3, qp[3], qpp[3]);
        float[,] r50wp5 = ri0wpi(r54, r40wp4, r40w4, qp[4], qpp[4]);
        float[,] r60wp6 = ri0wpi(r65, r50wp5, r50w5, qp[5], qpp[5]);
        float[,] r70wp7 = ri0wpi(r76, r60wp6, r60w6, 0f, 0f);
        // ------------------------------------------------------------
        // Aceleracion lineal articular
        // ------------------------------------------------------------
        float[,] r00vp0 = new float[,] { { 0f }, { 0f }, { g } };
        float[,] r10vp1 = ri0vpi_r(r10, r00vp0, r10wp1, r10w1, r10p1);
        float[,] r20vp2 = ri0vpi_r(r21, r10vp1, r20wp2, r20w2, r20p2);
        float[,] r30vp3 = ri0vpi_r(r32, r20vp2, r30wp3, r30w3, r30p3);
        float[,] r40vp4 = ri0vpi_r(r43, r30vp3, r40wp4, r40w4, r40p4);
        float[,] r50vp5 = ri0vpi_r(r54, r40vp4, r50wp5, r50w5, r50p5);
        float[,] r60vp6 = ri0vpi_r(r65, r50vp5, r60wp6, r60w6, r60p6);
        float[,] r70vp7 = ri0vpi_r(r76, r60vp6, r70wp7, r70w7, r70p7);
        // ------------------------------------------------------------
        // Aceleracion del centro de masa de cada elemento
        // ------------------------------------------------------------
        float[,] r10a1 = ri0ai(r10vp1, r10wp1, r10w1, r10s1);
        float[,] r20a2 = ri0ai(r20vp2, r20wp2, r20w2, r20s2);
        float[,] r30a3 = ri0ai(r30vp3, r30wp3, r30w3, r30s3);
        float[,] r40a4 = ri0ai(r40vp4, r40wp4, r40w4, r40s4);
        float[,] r50a5 = ri0ai(r50vp5, r50wp5, r50w5, r50s5);
        float[,] r60a6 = ri0ai(r60vp6, r60wp6, r60w6, r60s6);
        float[,] r70a7 = ri0ai(r70vp7, r70wp7, r70w7, r70s7);
        // ------------------------------------------------------------
        // Fuerza en el centro de masa de cada elemento
        // ------------------------------------------------------------
        float[,] r70f7 = ri0fi(r70a7, m7);
        float[,] r60f6 = ri0fi(r60a6, m6);
        float[,] r50f5 = ri0fi(r50a5, m5);
        float[,] r40f4 = ri0fi(r40a4, m4);
        float[,] r30f3 = ri0fi(r30a3, m3);
        float[,] r20f2 = ri0fi(r20a2, m2);
        float[,] r10f1 = ri0fi(r10a1, m1);
        // ------------------------------------------------------------
        // Par en el centro de masa de cada elemento
        // ------------------------------------------------------------
        float[,] r70n7 = ri0ni(r70wp7, r70w7, Iexter);
        float[,] r60n6 = ri0ni(r60wp6, r60w6, r60I_r06);
        float[,] r50n5 = ri0ni(r50wp5, r50w5, r50I_r05);
        float[,] r40n4 = ri0ni(r40wp4, r40w4, r40I_r04);
        float[,] r30n3 = ri0ni(r30wp3, r30w3, r30I_r03);
        float[,] r20n2 = ri0ni(r20wp2, r20w2, r20I_r02);
        float[,] r10n1 = ri0ni(r10wp1, r10w1, r10I_r01);
        // ------------------------------------------------------------
        // Fuerzas articulares
        // ------------------------------------------------------------
        float[,] r70f7a = r70f7;
        float[,] r60f6a = ri0fia(r67, r70f7a, r60f6);
        float[,] r50f5a = ri0fia(r56, r60f6a, r50f5);
        float[,] r40f4a = ri0fia(r45, r50f5a, r40f4);
        float[,] r30f3a = ri0fia(r34, r40f4a, r30f3);
        float[,] r20f2a = ri0fia(r23, r30f3a, r20f2);
        float[,] r10f1a = ri0fia(r12, r20f2a, r10f1);
        // ------------------------------------------------------------
        // Pares articulares
        // ------------------------------------------------------------
        float[,] r20p1 = Matriz_Mul_Matriz(r21, r10p1); float[,] r30p2 = Matriz_Mul_Matriz(r32, r20p2);
        float[,] r40p3 = Matriz_Mul_Matriz(r43, r30p3); float[,] r50p4 = Matriz_Mul_Matriz(r54, r40p4);
        float[,] r60p5 = Matriz_Mul_Matriz(r65, r50p5); float[,] r70p6 = Matriz_Mul_Matriz(r76, r60p6);
        float[,] r70n7a = r70n7;
        float[,] r60n6a = ri0nia(r67, r70n7a, r70f7a, r60n6, r60f6, r70p6, r60p6, r60s6);
        float[,] r50n5a = ri0nia(r56, r60n6a, r60f6a, r50n5, r50f5, r60p5, r50p5, r50s5);
        float[,] r40n4a = ri0nia(r45, r50n5a, r50f5a, r40n4, r40f4, r50p4, r40p4, r40s4);
        float[,] r30n3a = ri0nia(r34, r40n4a, r40f4a, r30n3, r30f3, r40p3, r30p3, r30s3);
        float[,] r20n2a = ri0nia(r23, r30n3a, r30f3a, r20n2, r20f2, r30p2, r20p2, r20s2);
        float[,] r10n1a = ri0nia(r12, r20n2a, r20f2a, r10n1, r10f1, r20p1, r10p1, r10s1);
        // ------------------------------------------------------------
        // Fuerzas y pares de accionamientos
        // ------------------------------------------------------------
        float t_1 = t_r(r10, r10n1a, qp[0], b1);
        float t_2 = t_r(r21, r20n2a, qp[1], b2);
        float t_3 = t_r(r32, r30n3a, qp[2], b3);
        float t_4 = t_r(r43, r40n4a, qp[3], b4);
        float t_5 = t_r(r54, r50n5a, qp[4], b5);
        float t_6 = t_r(r65, r60n6a, qp[5], b6);

        float[] tau = new float[] {t_1, t_2, t_3, t_4, t_5, t_6};

        //Debug.Log("TAU is: " + tau[0] + "   " + tau[1] + "   " + tau[2] + "   " + tau[3] + "   " + tau[4] + "   " + tau[5]);

        return tau;
    }



    // Función que obtiene la nueva matriz de inercias 6x6 para la Dinámica Directa
    private static float[,] h6(float[] q = null, float[] m = null, float m7 = 0.5f, float[,] Iexter = null)
    {
        // H6    Matriz de momentos de inercia.
        // H = H6(Q, MASAEXT, INERCIAEXT) calcula la matriz de momentos de
        // inercia H 6x6 utilizando el tercer método de Walker y Orin.Q es
        // el vector 6x1 de variables articulares.  MASAEXT es la masa de la
        // carga externa.INERCIAEXT es la inercia de la carga externa.
        //
        // See also WALKERORIN6.

        float[,] h = new float[6, 6];

        // ------------------------------------------------------------
        // Parámetros de D-H del robot KUKA KR6 R900
        // ------------------------------------------------------------
        //float[] teta = new float[] { q[0], q[1] + Mathf.PI / 2f, q[2], q[3], q[4] + Mathf.PI, q[5] };
        float[] teta = new float[] { q[0], q[1], q[2], q[3], q[4], q[5] };
        //float[] d = new float[] { 0.4f, 0f, 0f, 0.42f, 0f, 0.08f };
        //float[] a = new float[] { 0.025f, 0.455f, 0.035f, 0f, 0f, 0f };
        //float[] alfa = new float[] { Mathf.PI / 2f, Mathf.PI, -Mathf.PI / 2f, Mathf.PI / 2f, Mathf.PI / 2f, 0f };
        // ------------------------------------------------------------
        // Matrices de Inercias Centroidales. (Kg - m ^ 2.)
        // ------------------------------------------------------------
        float[,] J = {
            { 0.0084f, 0.0000f, 0.0000f },
            { 0.0000f, 0.0064f, 0.0000f },
            { 0.0000f, 0.0000f, 0.0084f },
            { 0.0078f, 0.0000f, 0.0000f },
            { 0.0000f, 0.0021f, 0.0000f },
            { 0.0000f, 0.0000f, 0.0021f },
            { 0.0016f, 0.0000f, 0.0000f },
            { 0.0000f, 0.0462f, 0.0000f },
            { 0.0000f, 0.0000f, 0.0462f },
            { 0.0016f, 0.0000f, 0.0000f },
            { 0.0000f, 0.0016f, 0.0000f },
            { 0.0000f, 0.0000f, 0.0009f },
            { 0.0016f, 0.0000f, 0.0000f },
            { 0.0000f, 0.0016f, 0.0000f },
            { 0.0000f, 0.0000f, 0.0009f },
            { 0.0001f, 0.0000f, 0.0000f },
            { 0.0000f, 0.0001f, 0.0000f },
            { 0.0000f, 0.0000f, 0.0001f },
            { Iexter[0,0], Iexter[0,1], Iexter[0,2] },
            { Iexter[1,0], Iexter[1,1], Iexter[1,2] },
            { Iexter[2,0], Iexter[2,1], Iexter[2,2] }
        };
        // Vector Z0.
        float[,] z0 = new float[,] { { 0f }, { 0f}, { 1f} };
        // Condiciones de Carga Externa.
        float[] M = new float[] { 0f, 0f, 0f, 0f, 0f, 0f, m7 };
        float[,] cj_1j = zeros(3, 1);
        float[,] Ej_1j = Iexter;

        for (int j = 5; j >= 0; j--)
        {
            // Constante para sacar la Inercia J.
            int k = (j * 3);
            // Vectores p y s.
            float[,] p = new float[,] { { a[j] }, { d[j] * Mathf.Sin(alfa[j]) }, { d[j] * Mathf.Cos(alfa[j]) } };
            float[,] s = Matriz_Mul_Escalar(p, -0.5f);
            // Matrices de transformacion.
            float[,] aj_1j = dh(teta[j], alfa[j]);
            float[,] ajj_1 = tras(aj_1j);
            // Centroide e Inercia de los elementos anteriores.
            float[,] cjjM1 = cj_1j;
            float[,] EjjM1 = Ej_1j;
            // Masa de todos los elementos anteriores.
            M[j] = M[j + 1] + m[j];
            // Nuevo centroide.
            float[,] cjj = Matriz_Div_Escalar(Matriz_Sum_Matriz(Matriz_Mul_Escalar(Matriz_Sum_Matriz(s, p), m[j]), Matriz_Mul_Escalar(Matriz_Sum_Matriz(cjjM1, p), M[j + 1])), M[j]);
            cj_1j = Matriz_Mul_Matriz(aj_1j, cjj);
            // Distancia de traslado de las inercias.
            float[,] p1 = Matriz_Rest_Matriz(Matriz_Sum_Matriz(cjjM1, p), cjj);
            float[,] d1 = Matriz_Rest_Matriz(Matriz_Mul_Escalar(eye(3), dot_vector_x3(p1, p1)), Matriz_Mul_Matriz(p1, tras(p1))); 
            float[,] p2 = Matriz_Rest_Matriz(Matriz_Sum_Matriz(s, p), cjj);
            float[,] d2 = Matriz_Rest_Matriz(Matriz_Mul_Escalar(eye(3), dot_vector_x3(p2, p2)), Matriz_Mul_Matriz(p2, tras(p2)));
            // Nueva Inercia.
            Ej_1j = Matriz_Mul_Matriz(Matriz_Mul_Matriz(aj_1j, Matriz_Sum_Matriz(Matriz_Sum_Matriz(Matriz_Sum_Matriz(EjjM1, Matriz_Mul_Escalar(d1, M[j + 1])), Matriz_Extraer_Rango(J, k, 3, 0, 3)), Matriz_Mul_Escalar(d2, m[j]))), ajj_1);
            // Fuerza y par de los elementos j hasta N.
            float[,] Fj_1j = cross_vec_x3(z0, Matriz_Mul_Escalar(cj_1j, M[j]));
            float[,] Nj_1j = Matriz_Mul_Matriz(Ej_1j, z0);
            // Elemento j.Componente H(j, j).
	        float[,] fi_1i = Fj_1j;
            float[,] ni_1i = Matriz_Sum_Matriz(Nj_1j, cross_vec_x3(cj_1j, Fj_1j));
            h[j, j] = ni_1i[2, 0];
            // Elementos j - 1 hasta 0.Componentes H(0:j - 1, j)
            int i = j - 1;
            while (i >= 0)
            {
                // Vector p.
                p = new float[,] { { a[i] }, { d[i] * Mathf.Sin(alfa[i]) }, { d[i] * Mathf.Cos(alfa[i]) } };
                // Matrices de rotacion.
                float[,] ai_1i = dh(teta[i], alfa[i]);
                float[,] aii_1 = tras(ai_1i);
                // Fuerza y par del anterior elemento.
                float[,] fiiM1 = fi_1i;
                float[,] niiM1 = ni_1i;
                // Fuerza y par de este elemento.
                fi_1i = Matriz_Mul_Matriz(ai_1i, fiiM1);
                ni_1i = Matriz_Mul_Matriz(ai_1i, Matriz_Sum_Matriz(niiM1, cross_vec_x3(p, fiiM1)));
                // Componente H(i, j).
                h[i, j] = ni_1i[2, 0];
                // H es simetrica.
                h[j, i] = h[i, j];
                i--;
            }
        }

        //for (int a = 0; a < 6; a++)
        //{
        //    Debug.Log("H(" + a + "): " + h[a, 0] + "   " + h[a, 1] + "   " + h[a, 2] + "   " + h[a, 3] + "   " + h[a, 4] + "   " + h[a, 5]);
        //}

        return h;
    }


    // Función para calcular la Dinámica Directa del robot KUKA KR6 R900
    public static float[] walkerorin6(float[] q = null, float[] qp = null, float[] tau = null, float g = 9.8f, float[] m = null, float m7 = 0.5f, float[,] Iexter = null)
    {
        // WALKERORIN6   Tercer método de Walker &Orin.
        // QPP = WALKERORIN6(Q, QP, TAU, MASAEXT, INERCIAEXT) calcula la dinámica
        // directa del robot de 6GDL devolviendo el vector 6x1 que representa la
        // aceleración de cada articulación utilizando el tercer método de Walker y Orin.
        // Q es el vector 6x1 de variables articulares. QP es el vector 6x1 que
        // representa la velocidad de cada articulación.TAU es el vector 6x1
        // que representa el par de entrada a cada articulación. MASAEXT es
        // la masa de la carga externa.INERCIAEXT es la inercia de la carga externa.
        //
        // See also NEWTONEULER6, H6.

        float[] qpp = new float[] { 0f, 0f, 0f, 0f, 0f, 0f };
        // Se calcula el vector b.
        float[] n_tau = newtoneuler6(q, qp, qpp, g, m, m7, Iexter);
        // Se calcula la matriz de momentos de inercia H. 
        float[,] H = h6(q, m, m7, Iexter);
        //Ajusto los vectores en matrices para la multiplicación
        float[,] Tau = new float[6, 1];
        float[,] n_Tau = new float[6, 1];
        for (int i = 0; i < 6; i++)
        {
            Tau[i, 0] = tau[i];
            n_Tau[i, 0] = n_tau[i];
        }
        // Se calcula el vector de aceleración de cada articulación.
        float[,] n_qpp = Matriz_Mul_Matriz(INV(H), Matriz_Rest_Matriz(Tau, n_Tau));

        //Debug.Log("QPP is: " + n_qpp[0, 0] + "   " + n_qpp[1, 0] + "   " + n_qpp[2, 0] + "   " + n_qpp[3, 0] + "   " + n_qpp[4, 0] + "   " + n_qpp[5, 0]);

        return new float[] { n_qpp[0,0], n_qpp[1, 0], n_qpp[2, 0], n_qpp[3, 0], n_qpp[4, 0], n_qpp[5, 0] };
    }


    // Function to obtain the Cofactor of matrix
    private static float[,] getCfactor(float[,] M, int p, int q)
    {
        int row = M.GetLength(0);  // Filas
        int col = M.GetLength(1);  // Columnas
        float[,] t = new float[row - 1, col - 1];

        if (row == col)
        {
            int i = 0, j = 0;
            for (int r = 0; r < row; r++)
            {
                for (int c = 0; c < col; c++) //Copy only those elements which are not in given row r and column c: 
                {
                    if ((r != p) && (c != q))
                    {
                        t[i, j++] = M[r, c]; //If row is filled increase r index and reset c index
                        if (j == (col - 1))
                        {
                            j = 0; i++;
                        }
                    }
                }
            }
        }
        else
            Debug.LogError("getCfactor: input matrix is not NxN size.");

        return t;
    }

    // Function to find determinant of matrix
    public static float DET(float[,] M) 
    {
        float D = 0f;
        int row = M.GetLength(0);  // Filas
        int col = M.GetLength(1);  // Columnas

        if (row == col)
        {
            if (row == 1)
            {
                D = M[0, 0];
            }
            else
            {
                float[,] t = new float[row, col]; //store cofactors
                int s = 1; //store sign multiplier 
                // To Iterate each element of first row
                for (int f = 0; f < col; f++) 
                {
                    //For Getting Cofactor of M[0, f] do 
                    t = getCfactor(M, 0, f); 
                    D += s * M[0, f] * DET(t);
                    s = -s;
                }
            }
        }
        else
            Debug.LogError("DET: input matrix is not nxn size.");

        return D;
    }

    //Function to find adjoint matrix 
    public static float[,] ADJ(float[,] M)
    {
        int row = M.GetLength(0);  // Filas
        int col = M.GetLength(1);  // Columnas
        float[,] adj = new float[row, col];

        if (row == col)
        {
            if (row == 1)
            {
                adj[0, 0] = 1;
            }
            else
            {
                int s = 1;
                float[,] t = new float[row, col];
                for (int i=0; i<row; i++) 
                {
                    for (int j=0; j<row; j++) 
                    {
                        //To get cofactor of M[i][j]
                        t = getCfactor(M, i, j);
                        s = ((i+j)%2==0)? 1: -1; //sign of adj[j][i] positive if sum of row and column indexes is even.
                        adj[j, i] = (s) * (DET(t)); //Interchange rows and columns to get the transpose of the cofactor matrix
                    }
                }
            }
        }
        else
            Debug.LogError("ADJ: input matrix is not nxn size.");

        return adj;
    }

    // Función que obtiene la Inversa de una Matriz M
    public static float[,] INV(float[,] M)
    {
        int row = M.GetLength(0);  // Filas
        int col = M.GetLength(1);  // Columnas
        float[,] inv = new float[row, col];

        if (row == col)
        {
            float det = DET(M);
            if (det != 0)
            {
                float[,] adj = ADJ(M);
                for (int i=0; i<row; i++)
                {
                    for (int j = 0; j < row; j++)
                    {
                        inv[i,j] = adj[i,j] / det;
                    }
                }
            }
            else
                Debug.LogError("INV: Can't find its inverse.");
        }
        else
            Debug.LogError("ADJ: input matrix is not nxn size.");

        return inv;
    }

}
    