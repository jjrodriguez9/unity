﻿using UnityEngine;
using ChartAndGraph;
using Photon.Pun;

public class PlottingKUKA : MonoBehaviourPun
{
    private float[] error_XYZ_A6;
    [SerializeField]
    private float Ts = 0.01f; // Tiempo de muestreo en Unity
    [SerializeField]
    private GraphChart graph;
    private float lastTime = 0f;
    [SerializeField]
    private MemoryONE_KUKA memoryONE_KUKA;
    [SerializeField]
    private PhotonView PV;
    [SerializeField]
    private int _distance = 50;
    [SerializeField]
    private PanelControlKUKA panelControl;

    private void Start()
	{
        if (!DataInfo.dataInfo.singlePractice)
        {
            PV = GetComponent<PhotonView>();
            panelControl= GameObject.Find("PanelControlHandle").GetComponent<PanelControlKUKA>();
            Inicializate();
        }
        else
        {
            Inicializate();
        }
    }

    private void Inicializate()
    {
        if (graph == null) // the ChartGraph info is obtained via the inspector
            return;
        graph.DataSource.StartBatch(); // calling StartBatch allows changing the graph data without redrawing the graph for every change
        graph.DataSource.ClearCategory("z"); // clear the "Player 1" category. this category is defined using the GraphChart inspector
        graph.DataSource.ClearCategory("y"); // clear the "Player 1" category. this category is defined using the GraphChart inspector
        graph.DataSource.ClearCategory("x"); // clear the "Player 1" category. this category is defined using the GraphChart inspector
        graph.DataSource.EndBatch(); // finally we call EndBatch , this will cause the GraphChart to redraw itself
        memoryONE_KUKA = GameObject.Find("MemoryHandle").GetComponent<MemoryONE_KUKA>();
        error_XYZ_A6 = new float[3];
    }
    void FixedUpdate()
    {
        error_XYZ_A6 = memoryONE_KUKA.Error_XYZ_A6;
        if (!DataInfo.dataInfo.singlePractice)
        {
            if (PV.IsMine)
            {
                if (Vector3.Distance(panelControl.transform.position, GameSetup.gameSetup._player.transform.position) < _distance)
                {
                    PV.RPC("multiGrafics", RpcTarget.All, error_XYZ_A6[0], error_XYZ_A6[1], error_XYZ_A6[2]);
                }
            }
        }
        else
        {
            singleGrafics();
        }
    }
    private void singleGrafics()
    {
        float time = Time.time;
        if (lastTime + 0.4f < time)
        {
            lastTime = time;
            graph.DataSource.AddPointToCategoryRealtime("z",time, error_XYZ_A6[2]); // each time we call AddPointToCategory 
            graph.DataSource.AddPointToCategoryRealtime("y",time, error_XYZ_A6[1]); // each time we call AddPointToCategory 
            graph.DataSource.AddPointToCategoryRealtime("x",time, error_XYZ_A6[0]); // each time we call AddPointToCategory 
        }
    }
    [PunRPC]
    private void multiGrafics(float x,float y, float z)
    {
        //Debug.LogError("x: " + x + " y: " + y + " z: " + z);
        float time = Time.time;
        if (lastTime + 0.4f < time)
        {
            lastTime = time;
            graph.DataSource.AddPointToCategoryRealtime("z", time, z); // each time we call AddPointToCategory 
            graph.DataSource.AddPointToCategoryRealtime("y", time, y); // each time we call AddPointToCategory 
            graph.DataSource.AddPointToCategoryRealtime("x", time, x); // each time we call AddPointToCategory 
        }
    }
}
