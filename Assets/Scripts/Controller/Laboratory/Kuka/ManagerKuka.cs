﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ManagerKuka : MonoBehaviour
{
    [SerializeField]
    private GameObject structure;
    // Start is called before the first frame update
    void Awake()
    {
        if (SceneManager.GetActiveScene().buildIndex == MultiplayerSetting.multiplayerSetting.multiplayerScene)
        {
            structure = gameObject.transform.Find("Mecanica").gameObject;
            structure.transform.Find("Edificación").gameObject.SetActive(false);
        }
    }
}
