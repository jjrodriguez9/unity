﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using UnityEngine.SceneManagement;

public class PanelControlKUKA : MonoBehaviourPun
{
    [SerializeField]
    private Transform _player;
    [SerializeField]
    private GameObject _robot;
    [SerializeField]
    private int _distance = 5;
    [SerializeField]
    private DatosKUKA datosKUKA;
    [SerializeField]
    private SingleLaboratory singleLaboratory;
    [SerializeField]
    private PhotonView PV;
    // Start is called before the first frame update
    void Start()
    {
        if (!DataInfo.dataInfo.singlePractice)
        {
            PV = GetComponent<PhotonView>();
            if (PV.IsMine)
            {
                Inicializate();
            }
            else
            {
                _player = GameSetup.gameSetup._player.transform;
            }
        }
        else
        {
            Inicializate();
        }
    }
    private void Inicializate()
    {
        datosKUKA = GameObject.Find("KUKA_Execution").GetComponent<DatosKUKA>();
        if (SceneManager.GetActiveScene().buildIndex == 4)
        {
            singleLaboratory = GameObject.Find("SingleLaboratory").GetComponent<SingleLaboratory>();
            _player = singleLaboratory._player.transform;
        }
        else if (SceneManager.GetActiveScene().buildIndex == MultiplayerSetting.multiplayerSetting.multiplayerScene)
        {
            _player = GameSetup.gameSetup._player.transform;
        }
    }
    // Update is called once per frame
    void FixedUpdate()
    {
        if (!DataInfo.dataInfo.singlePractice)
        {
            controllerKuka();
            datosKUKA._PV.RPC("label", RpcTarget.All);
        }
        else
        {
            
            controllerKuka();
            label();
        }
    }
    
    private void label()
    {
        datosKUKA.labelValue[0].text = datosKUKA.sliderX.value.ToString();
        datosKUKA.labelValue[1].text = datosKUKA.sliderY.value.ToString();
        datosKUKA.labelValue[2].text = datosKUKA.sliderZ.value.ToString();
    }
    private void controllerKuka()
    {
        
        if (Vector3.Distance(transform.position, _player.position) < _distance)
        {
            // Change Setpoint X
            if (Input.GetButton("active_x"))
            {
                float value = Input.GetAxis("slider_move");
                if (!DataInfo.dataInfo.singlePractice)
                {
                    PV.RPC("valueX", RpcTarget.All,value);
                }
                else
                {
                    valueX(value);
                }
            }
            // Change Setpoint Y
            if (Input.GetButton("active_y"))
            {
                float value = Input.GetAxis("slider_move");
                if (!DataInfo.dataInfo.singlePractice)
                {
                    PV.RPC("valueY", RpcTarget.All, value);
                }
                else
                {
                    valueY(value);
                }
            }
            // Change Setpoint Z
            if (Input.GetButton("active_z"))
            {
                float value = Input.GetAxis("slider_move");
                if (!DataInfo.dataInfo.singlePractice)
                {
                    PV.RPC("valueZ", RpcTarget.All, value);
                }
                else
                {
                    valueZ(value);
                }
            }
        }
    }
    [PunRPC]
    private void valueX(float val)
    {
        if (val < 0)
        {
            datosKUKA.sliderX.value = datosKUKA.sliderX.value - 0.01f;
        }
        else if (val > 0)
        {
            datosKUKA.sliderX.value = datosKUKA.sliderX.value + 0.01f;
        }
    }
    [PunRPC]
    private void valueY(float val)
    {
        if (val < 0)
        {
            datosKUKA.sliderY.value = datosKUKA.sliderY.value - 0.01f;
        }
        else if (val > 0)
        {
            datosKUKA.sliderY.value = datosKUKA.sliderY.value + 0.01f;
        }
    }
    [PunRPC]
    private void valueZ(float val)
    {
        if (val < 0)
        {
            datosKUKA.sliderZ.value = datosKUKA.sliderZ.value - 0.01f;
        }
        else if (val > 0)
        {
            datosKUKA.sliderZ.value = datosKUKA.sliderZ.value + 0.01f;
        }
    }
}
