﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Photon.Pun;

public class Movimiento : MonoBehaviourPun
{
    // Para el movimiento de la valvula de carga y valvula de control
    [SerializeField]
    private Transform valvulaTR;
    [SerializeField]
    private Transform posicionadorTR;
    [SerializeField]
    private Slider sliderValvula; // Slider de la valvula de carga
    [SerializeField]
    private Slider sliderSP;  // Slider SetPoint
    [SerializeField]
    private Slider sliderCV_Manual;  // Slider CV Manual
    public float carga, sp, pv, cv, cvManual;   // Para que los valores se puedan leer en otros scripts

    // Datos del panel
    [SerializeField]
    private TextMeshProUGUI SPtext, PVtext, CVtext;
    // Datos del transmisor
    [SerializeField]
    private Text PITtext, cPITtext;
    // Datos de la valvula
    [SerializeField]
    private Text valvText;
    // Datos de PV , CV Manual
    [SerializeField]
    private TextMeshProUGUI cvManualText;
    [SerializeField]
    private TextMeshProUGUI pvManualText;
    // Para animacion del vapor 
    [SerializeField]
    private ParticleSystem vapor;
    // Para activar sonido Compresor
    [SerializeField]
    private AudioSource sourceCompresor;
    [SerializeField]
    private AudioClip audioCOmpresor;
    // Para activar sonido Selector
    [SerializeField]
    private AudioSource sourceSelector;
    [SerializeField]
    private AudioClip audioSelector;
    // Para movimiento del selector en dos posiciones
    [SerializeField]
    private Transform selector;
    [SerializeField]
    private int i;
    //Alarma en bajo, en alto y funcionamiento correcot
    [SerializeField]
    private Image LL, HH, ON;
    //Accionar valvula de alivio
    [SerializeField]
    private GameObject fuga;
    [SerializeField]
    private PhotonView PV;
    public PhotonView _PV { get { return PV; } }

    CANAL_DE_COMUNICACION_M_U canal_De_Comunicacion;

    void Start()
    {
        if (!DataInfo.dataInfo.singlePractice)
        {
            PV = GetComponent<PhotonView>();
            Inicializate();
        }
        else
        {
            Inicializate();
        }
    }
    private void Inicializate()
    {
        i = 0;
        selector.transform.localEulerAngles = new Vector3(5.625f, 0, 0); //Posicion inicial del selector
        sourceCompresor.Stop(); // Sonido inicial del compresor
        sourceSelector.Stop();// Sonido inicial del selector

        LL.GetComponent<Image>().color = new Color32(255, 255, 255, 255); // Color inicial cuando no esta activada la alarma
        HH.GetComponent<Image>().color = new Color32(255, 255, 255, 255); // Color inicial cuando no esta activada la alarma
        ON.GetComponent<Image>().color = new Color32(255, 255, 255, 255); // Color inicial cuando no esta activado el led
        fuga.SetActive(false); // Fuga desactivada inicialmente
        canal_De_Comunicacion = FindObjectOfType<CANAL_DE_COMUNICACION_M_U>();
    }
    void Update()
    {

        calculetePresion();
    }

    private void calculetePresion()
    {
        sp = (sliderSP.value);                   // Se toma el valor del slider del SetPoint
        carga = (sliderValvula.value) * 100f;    // Se toma el valor del slider de la carga
        valvulaTR.transform.localEulerAngles = new Vector3(0f, carga * 1.8f, 0f);  // Movimiento valvula de carga
        posicionadorTR.transform.localPosition = new Vector3(-0.0241f, -0.02353f + (cv / 3300), 0.0002f); // Movimiento valvula de control
        vapor.startSize = Mathf.Lerp(0.2f, 2.3f, pv / 100f); // Movimiento particulas con respecto a CV
        // Condicion para activar el sonido en el entorno del compresor
        soundCompresor(pv);
        SPtext.text = sp.ToString("F2");// Mostrar valor transformado a string con 2 decimales
        PVtext.text = pv.ToString("F2");
        pvManualText.text = pv.ToString("F2");
        CVtext.text = cv.ToString("F2");
        PITtext.text = pv.ToString("F2");
        cPITtext.text = ((-20f / 47f) + (16f / 47f) * pv).ToString("F2");
        valvText.text = cv.ToString("F2");

        cvManual = sliderCV_Manual.value; //Leer el valor del slider Manual;
        cvManualText.text = cvManual.ToString("F2");

        if (Input.GetKeyDown(KeyCode.M))
        {
            i++;
            sourceSelector.PlayOneShot(audioSelector);
        }
        Manual(i);
        Automatic(i);
        if (i == 2)
        {
            i = 0;
        }

        ///Condiciones de funcionamiento de alarmas
        Validatealert(pv);
    }

    private void soundCompresor(float pv)
    {
        // Condicion para activar el sonido en el entorno del compresor
        if (pv > 0.1f)
        {
            sourceCompresor.PlayOneShot(audioCOmpresor);
        }
        else
        {
            sourceCompresor.Stop();
        }
    }

    private void Validatealert(float pv)
    {
        ///Condiciones de funcionamiento de alarmas
        if (pv < 15)  // En Bajo
        {
            LL.GetComponent<Image>().color = new Color32(193, 78, 78, 255);
        }
        else
        {
            LL.GetComponent<Image>().color = new Color32(255, 255, 255, 255);
        }
        if (pv > 50)  //En Alto 
        {
            HH.GetComponent<Image>().color = new Color32(193, 78, 78, 255);
            fuga.SetActive(true);
        }
        else
        {
            HH.GetComponent<Image>().color = new Color32(255, 255, 255, 255);
            fuga.SetActive(false);
        }
        if (pv >= 15 && pv <= 50)// funcionamiento normal
        {
            ON.GetComponent<Image>().color = new Color32(84, 207, 19, 255);
        }
        else
        {
            ON.GetComponent<Image>().color = new Color32(255, 255, 255, 255);
        }
    }

    private void Manual(int i)
    {
        if (i == 0) // CONTROL MANUAL
        {
            selector.transform.localEulerAngles = new Vector3(5.625f, 0, 0); // Posicion del selector en manual
            cv = cvManual;
            canal_De_Comunicacion.ModeloManual(cv);
        }
    }

    private void Automatic(int i)
    {
        if (i == 1) // CONTROL AUTOMATICO
        {
            selector.transform.localEulerAngles = new Vector3(-44.898F, 0, 0); // Posicion del selector en automatico
            cv = canal_De_Comunicacion.cv_l;
            canal_De_Comunicacion.ModeloAutomatico(cv);
        }
    }


}
