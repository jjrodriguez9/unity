﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;
using Photon.Pun;

public class CANAL_DE_COMUNICACION_M_U : MonoBehaviourPun
{
    // Abrir memorias DLL
    //[DllImport(@"C:\sm\smClient.dll")]
    //private static extern int abrirMemoria(string nombre, int tipo);
    // Leer datos DLL
    //[DllImport(@"C:\sm\smClient.dll")]
    //private static extern float readFloatValue(string nombre, int posicion);
    // Escribir datos DLL
    //[DllImport(@"C:\sm\smClient.dll")]
    //private static extern float writeFloatValue(string nombre, int posicion, float valor);
    [SerializeField]
    private float sp_e, load_e, pv_e;   //Datos Enviados 
    public float cv_l, i_l;           //Datos recibidos 
    private Memoryshare memoryshare;
    //Parámetros del sistema de presion
    float k1;   // Resistencia al flujo de la valvula de control
    float k2;   // Resistencia al flujo de la valvula de carga
    float T; // Temperatura
    float V; // Volumen
    float R; //resistencia
    float qi_phi, pt, po, pi, To;
    float ts;
    Movimiento movimiento;
    [SerializeField]
    private PhotonView PV;
    public PhotonView _PV { get { return PV; } }

    void Start()
    {
        if (!DataInfo.dataInfo.singlePractice)
        {
            PV = GetComponent<PhotonView>();
            Inicializate();
        }
        else
        {
            Inicializate();
        }
    }
    private void Inicializate()
    {
        memoryshare = new Memoryshare();
        memoryshare.openMemoryShare("Memoria0", 2);
        memoryshare.createMemoriShare("Memoria0", 50, 2);
        for (int i = 0; i < 50; i++)
        {
            memoryshare.Setfloat("Memoria0", 0f, i);
        }
        k1 = 0.000001f;
        k2 = 0.000001f;
        T = 400f;
        V = 10f;
        R = 8314f;
        pi = 2000f;
        po = 1600f;
        pt = 1800f;
        ts = 0.1f;

        movimiento = FindObjectOfType<Movimiento>(); // Obtener los valores del script movimiento
    }
    void Update()
    {
        To = Time.deltaTime;
        if (To >= ts)
        {
            calculetekuka();
            To = 0;
        }
    }
    private void calculetekuka()
    {
        // Datos recibidos desde matlab
        cv_l = memoryshare.Getfloat("Memoria0", 1);
        qi_phi = cv_l;

        load_e = movimiento.carga;
        sp_e = movimiento.sp;
        movimiento.pv = pv_e;
        //movimiento.cv = cv_l;
    }
    public void ModeloManual(float qi_phi)
    {
        //*********** MODELO MATEMATICO DE PRESION **********\\
        pv_e = (R * T / V) * ((k1 * (Mathf.Sqrt((pi + qi_phi) * ((pi + qi_phi) - pt)))) - (k2 * (Mathf.Sqrt(pt * (pt - (po + load_e))))));
    }

    public void ModeloAutomatico(float qi_phi)
    {
        //*********** MODELO MATEMATICO DE PRESION **********\\
        pv_e = (R * T / V) * ((k1 * (Mathf.Sqrt((pi + qi_phi) * ((pi + qi_phi) - pt)))) - (k2 * (Mathf.Sqrt(pt * (pt - (po + load_e))))));

        // Datos enviados desde Unity
        memoryshare.Setfloat("Memoria0", sp_e, 2);
        memoryshare.Setfloat("Memoria0", load_e, 3);
        memoryshare.Setfloat("Memoria0", pv_e, 4);
    }
}
