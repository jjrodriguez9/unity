﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FPC2 : MonoBehaviour {

	private Vector3 x,y,z;
    [SerializeField]
    private Camera camara;
    [SerializeField]
    private Animator animator;
    [SerializeField]
    private Transform cam1, cam2;
    private int i = 0;

	void Start () {
		x = cam1.transform.localPosition;
		camara.transform.localPosition = x;
	}

	void Update ()
	{
		if (Input.GetKeyDown (KeyCode.Q))
			i++;

		if (i == 1) {
			x = cam2.transform.localPosition;
			camara.transform.localPosition = x;
		}
		if (i == 2) {
			x = cam1.transform.localPosition;
			camara.transform.localPosition = x;
		}
		if (i == 2)
			i = 0;


		if (Input.GetKey (KeyCode.W)) {
			animator.SetBool ("Camina", true);
		} else {
			animator.SetBool ("Camina", false);
		}

		if (Input.GetKey (KeyCode.S)) {
			animator.SetBool ("CaminaA", true);
		} else {
			animator.SetBool ("CaminaA", false);
		}

		if (Input.GetKey (KeyCode.D)) {
			animator.SetBool ("CaminaD", true);
		} else {
			animator.SetBool ("CaminaD", false);
		}

		if (Input.GetKey (KeyCode.A)) {
			animator.SetBool ("CaminaI", true);
		} else {
			animator.SetBool ("CaminaI", false);
		}

		if (Input.GetKey (KeyCode.LeftArrow)) {
			animator.SetBool ("RotI1", true);
			transform.Rotate (0, -1.7f, 0);
		} else {
			animator.SetBool ("RotI1", false);
		}

		if (Input.GetKey (KeyCode.RightArrow)) {
			animator.SetBool ("RotD1", true);
			transform.Rotate (0, 1.7f, 0);
		} else {
			animator.SetBool ("RotD1", false);
		}

		if (Input.GetKey (KeyCode.UpArrow)) {
			camara.transform.Rotate (-0.8f, 0, 0);
		}
		
		if (Input.GetKey (KeyCode.DownArrow)) {
			camara.transform.Rotate (0.8f, 0, 0);
		}			
	}
}


