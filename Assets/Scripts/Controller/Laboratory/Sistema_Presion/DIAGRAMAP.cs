﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class DIAGRAMAP : MonoBehaviourPun
{
    [SerializeField]
    private Renderer tanque, valvulacarga, valvulacontrol, control, posicionador, transmisor;
    [SerializeField]
    private Material tanqueNormal, tanqueP_ID, valvulacargaNormal, valvulacargaP_ID;
    [SerializeField]
    private Material valvulacontrolNormal, valvulacontrolP_ID, controlNormal, controlP_ID;
    [SerializeField]
    private Material posicionadorNormal, posicionadorP_ID, transmisorNormal, transmisorP_ID;

    int tanqueNum = 0, valvulacargaNum = 0, valvulacontrolNum = 0, controlNum = 0, posicionadorNum = 0, transmisorNum = 0;
    [SerializeField]
    private PhotonView PV;
    public PhotonView _PV { get { return PV; } }

    void Start()
    {
        if (!DataInfo.dataInfo.singlePractice)
        {
            PV = GetComponent<PhotonView>();
            Inicializate();
        }
        else
        {
            Inicializate();
        }
    }
    private void Inicializate()
    {
        tanque.material = tanqueNormal;
        valvulacarga.material = valvulacargaNormal;
        valvulacontrol.material = valvulacontrolNormal;
        control.material = controlNormal;
        posicionador.material = posicionadorNormal;
        transmisor.material = transmisorNormal;
    }
    void Update()
    {
        validate();
    }

    private void validate()
    {
        // tanque
        if (tanqueNum == 0)
        {
            tanque.material = tanqueNormal;
        }
        if (tanqueNum == 1)
        {
            tanque.material = tanqueP_ID;
        }
        if (tanqueNum == 2)
        {
            tanqueNum = 0;
        }
        // valvula de carga
        if (valvulacargaNum == 0)
        {
            valvulacarga.material = valvulacargaNormal;
        }
        if (valvulacargaNum == 1)
        {
            valvulacarga.material = valvulacargaP_ID;
        }
        if (valvulacargaNum == 2)
        {
            valvulacargaNum = 0;
        }
        // valvula de control
        if (valvulacontrolNum == 0)
        {
            valvulacontrol.material = valvulacontrolNormal;
        }
        if (valvulacontrolNum == 1)
        {
            valvulacontrol.material = valvulacontrolP_ID;
        }
        if (valvulacontrolNum == 2)
        {
            valvulacontrolNum = 0;
        }
        // PLC
        if (controlNum == 0)
        {
            control.material = controlNormal;
        }
        if (controlNum == 1)
        {
            control.material = controlP_ID;
        }
        if (controlNum == 2)
        {
            controlNum = 0;
        }
        // posicionador
        if (posicionadorNum == 0)
        {
            posicionador.material = posicionadorNormal;
        }
        if (posicionadorNum == 1)
        {
            posicionador.material = posicionadorP_ID;
        }
        if (posicionadorNum == 2)
        {
            posicionadorNum = 0;
        }
        // transmisor
        if (transmisorNum == 0)
        {
            transmisor.material = transmisorNormal;
        }
        if (transmisorNum == 1)
        {
            transmisor.material = transmisorP_ID;
        }
        if (transmisorNum == 2)
        {
            transmisorNum = 0;
        }
    }
    public void Tanque()
    {
        tanqueNum++;
    }
    public void ValvulaCarga()
    {
        valvulacargaNum++;
    }
    public void ValvulaControl()
    {
        valvulacontrolNum++;
    }
    public void Control()
    {
        controlNum++;
    }
    public void Posicionador()
    {
        posicionadorNum++;
    }
    public void Transmisor()
    {
        transmisorNum++;
    }
}

