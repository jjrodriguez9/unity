﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;
using TMPro;
using UnityEngine.UI;

public class Caudal : MonoBehaviour
{
    //[DllImport(@"D:\Respaldos Ultimos\Documents\Memorias_compartidas\Memorias_azules\Memorias Compartidas\Memory\SMclientAll\smClient\Debug\smClient.dll")]
    //private static extern int abrirMemoria(string nombre, int tipo);

    //[DllImport(@"D:\Respaldos Ultimos\Documents\Memorias_compartidas\Memorias_azules\Memorias Compartidas\Memory\SMclientAll\smClient\Debug\smClient.dll")]
    //private static extern float readFloatValue(string nombre, int posicion);

    //[DllImport(@"D:\Respaldos Ultimos\Documents\Memorias_compartidas\Memorias_azules\Memorias Compartidas\Memory\SMclientAll\smClient\Debug\smClient.dll")]
    //private static extern float writeFloatValue(string nombre, int posicion, float valor);

    public float sp, pv, load;   //Datos Enviados
    public float cv, t;          //Datos Leidos
    [SerializeField]
    private Text SPText, PVText, CVText, LoadText;
    [SerializeField]
    private Slider sliderSP, sliderLoad;
    [SerializeField]
    private Text FIT, cFIT;

    [SerializeField]
    private Transform valvula;
    [SerializeField]
    private Transform rotametro;
    [SerializeField]
    private Material Liquido;
    [SerializeField]
    private AudioSource sourceBomba;
    [SerializeField]
    private AudioClip audioBomba;
    
    private Memoryshare memoryshare;
    private EventPractice eventPractice; //instancio la clase realizar el json para el envio al api
    private DataEvent dataEvent; //instancio la clase para almacenar los datos

    void Start()
    {
        Inicializate();
    }

    void Update()
    {
        Calculo_Caudal();
    }
    private void Inicializate()
    {
        memoryshare = new Memoryshare();
        memoryshare.openMemoryShare("Memoria0", 2);
        memoryshare.createMemoriShare("Memoria0", 50, 2);
        for (int i = 0; i < 50; i++)
        {
            memoryshare.Setfloat("Memoria0", 0f, i);
        }
        sliderSP.value = 0.25f;
        sliderLoad.value = 0f;

        t = memoryshare.Getfloat("Memoria0", 0);
        cv = memoryshare.Getfloat("Memoria0", 1);

        memoryshare.Setfloat("Memoria0", 0, 0);
        memoryshare.Setfloat("Memoria0", 0, 1);
        memoryshare.Setfloat("Memoria0", 0, 2);
        memoryshare.Setfloat("Memoria0", 0, 3);
        memoryshare.Setfloat("Memoria0", 0, 4);

        //Liquido.SetFloat("_FillAmount", Mathf.Lerp(-0.5001f, 2.68f, (0 / 100f)));
        Liquido.SetFloat("_FillAmount", Mathf.Lerp(-2.0995f, 1.0995f, (0 / 100f)));

        Stop_Inicialice();
    }

   
    public void Play()
    {
        sourceBomba.volume = pv / 100f;
        sourceBomba.PlayOneShot(audioBomba);
    }

    public void Stop()
    {
        sourceBomba.volume = 0f;
        sourceBomba.Stop();
    }
    public void Stop_Inicialice()
    {
        sourceBomba.Stop();
    }
    public void Modelo(float t, float cv)
    {
        //f = ((1.296) / (1.24 * s + 1)) * exp(-0.0733 * s)
        //u1 = 1 - (162 * exp(733 / 12400 - (25 * x) / 31)) / 155 % 1
        double ecua = 1f - ((162f * Mathf.Exp(733f / 12400f - (25f * t) / 31f)) / 155f);
        pv = (float)((cv * ecua) - load);
        if (pv < 0)
        {
            pv = 0;
        }
        if (pv > 100)
        {
            pv = 100;
        }
    }

    public void Calculo_Caudal()
    {
        sp = sliderSP.value * 100f;
        load = sliderLoad.value * 50f;

        SPText.text = sp.ToString("F2") + " Lit/Min";
        PVText.text = pv.ToString("F2") + " Lit/Min";
        CVText.text = cv.ToString("F2");
        LoadText.text = load.ToString("F2") + " %";

        if (pv > 0)
        {
            FIT.text = (pv).ToString("F2");
            cFIT.text = (((8f / 25f) * (pv)) + 4f).ToString("F2");
            Play();
        }
        else
        {
            FIT.text = (0f).ToString("F2");
            cFIT.text = (4f).ToString("F2");
            Stop();
        }

        t = memoryshare.Getfloat("Memoria0", 0);
        cv = memoryshare.Getfloat("Memoria0", 1);

        Modelo(t, cv);

        memoryshare.Setfloat("Memoria0", sp, 2);
        memoryshare.Setfloat("Memoria0", pv, 3);
        memoryshare.Setfloat("Memoria0", load, 4);


        valvula.transform.localEulerAngles = new Vector3(0f, load * (-1.8f), 0f);
        rotametro.transform.localPosition = new Vector3(-1.2561f, 4.275f + (pv * (0.635f / 100f)), 3.958f);
        //Liquido.SetFloat("_FillAmount", Mathf.Lerp(-0.5001f, 2.68f,(pv/100f)));
        Liquido.SetFloat("_FillAmount", Mathf.Lerp(-2.0995f, 1.0995f, (pv / 100f)));
    }
    //public void guardar()
    //{
    //    dataEvent = new DataEvent();
    //    eventPractice = new EventPractice();
    //    dataEvent.Sp.Add(sp);
    //    dataEvent.Accontrol.Add();
    //    dataEvent.Out.Add();
    //    eventPractice._id = DataInfo.dataInfo._practiceJoind._id;
    //    eventPractice.id_estudiante = DataInfo.dataInfo._authentication._id;
    //    eventPractice.eventos.Add(dataEvent);
    //    string json = JsonUtility.ToJson(eventPractice); //convirtindo a tipo json
    //    StartCoroutine(requestHttp.store(requestHttp.getStorePractice(), json, typeRequest.getEventPractice()));
    //    old_x = sp_XYZ_A6[0];
    //    old_y = sp_XYZ_A6[1];
    //    old_z = sp_XYZ_A6[2];
    //    //Debug.Log("entre a guardad");
    //}
}
