﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ChartAndGraph;
using Photon.Pun;

public class Graph3 : MonoBehaviourPun
{
    [SerializeField]
    private GraphChart Graph;

    [SerializeField]
    private int TotalPoints = 10;

    [SerializeField]
    private float timeToUpdate = 1.0f;

    [SerializeField]
    private PhotonView PV;

    private float lastX = 0f;
    private float lastTime = 0f;

    private double error;

    public float Capacity;

    Caudal caudal;

    // Start is called before the first frame update
    void Start()
    {
        if (!DataInfo.dataInfo.singlePractice)
        {
            PV = GetComponent<PhotonView>();
            Inicializate();
        }
        else
        {
            Inicializate();
        }

        
    }

    public void Inicializate()
    {
        Graph.DataSource.AutomaticVerticallView = false;
        Graph.DataSource.VerticalViewSize = Capacity;

        if (Graph == null) // the ChartGraph info is obtained via the inspector
            return;
        float x = 3f * TotalPoints;
        Graph.DataSource.StartBatch(); // calling StartBatch allows changing the graph data without redrawing the graph for every change
        Graph.DataSource.ClearCategory("Player 1"); // clear the "Player 1" category. this category is defined using the GraphChart inspector
                                                    //Graph.DataSource.ClearCategory("Player 2"); // clear the "Player 2" category. this category is defined using the GraphChart inspector
                                                    //Graph.DataSource.ClearCategory("Player 3"); // clear the "Player 2" category. this category is defined using the GraphChart inspector

        for (int i = 0; i < TotalPoints; i++)  //add random points to the graph
        {
            Graph.DataSource.AddPointToCategory("Player 1", System.DateTime.Now - System.TimeSpan.FromSeconds(x), 0.0f); // each time we call AddPointToCategory 
                                                                                                                         //Graph.DataSource.AddPointToCategory("Player 2", System.DateTime.Now - System.TimeSpan.FromSeconds(x), 0.0f); // each time we call AddPointToCategory 
                                                                                                                         //Graph.DataSource.AddPointToCategory("Player 3", System.DateTime.Now - System.TimeSpan.FromSeconds(x), 0.0f); // each time we call AddPointToCategory 
            x -= Random.value * 3f;
            lastX = x;
        }

        Graph.DataSource.EndBatch(); // finally we call EndBatch , this will cause the GraphChart to redraw itself

        caudal = FindObjectOfType<Caudal>();
    }

    // Update is called once per frame
    private void FixedUpdate()
    {
        if (!DataInfo.dataInfo.singlePractice)
        {
            if (PV.IsMine)
            {
                error = caudal.sp - caudal.pv;
                PV.RPC("MultiGrafics", RpcTarget.All, error);
            }
        }
        else
        {
            SingleGraphics();
        }
    }

    public void SingleGraphics()
    {
        error = caudal.sp - caudal.pv;

        float time = Time.time;
        if (lastTime + timeToUpdate < time)
        {
            lastTime = time;
            lastX += Random.value * 3f;

            Graph.DataSource.AddPointToCategoryRealtime("Player 1", System.DateTime.Now, error, 1f); // each time we call AddPointToCategory 
                                                                                                     //Graph.DataSource.AddPointToCategoryRealtime("Player 2", System.DateTime.Now, 0, 1f); // each time we call AddPointToCategory
                                                                                                     //Graph.DataSource.AddPointToCategoryRealtime("Player 3", System.DateTime.Now, 0, 1f); // each time we call AddPointToCategory
        }
    }


    public void MultiGraphics(float error)
    {
        
        float time = Time.time;
        if (lastTime + timeToUpdate < time)
        {
            lastTime = time;
            lastX += Random.value * 3f;

            Graph.DataSource.AddPointToCategoryRealtime("Player 1", System.DateTime.Now, error, 1f); // each time we call AddPointToCategory 
                                                                                                     //Graph.DataSource.AddPointToCategoryRealtime("Player 2", System.DateTime.Now, 0, 1f); // each time we call AddPointToCategory
                                                                                                     //Graph.DataSource.AddPointToCategoryRealtime("Player 3", System.DateTime.Now, 0, 1f); // each time we call AddPointToCategory
        }
    }
}
