﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Valvula : MonoBehaviour

{
    public Transform valvula;
    public Slider sli;
    public float carga;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
        
    }

    public void Giro()
    {
        // 09. Grip-1-solid1
        carga = (sli.value) * 100f;
        valvula.transform.localEulerAngles = new Vector3(0f, carga * (-1.8f), 0f);
    }
}
