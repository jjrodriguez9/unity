﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NewBehaviourScript : MonoBehaviour
{
    //Vector3 pos;
    public Text Text1;
    public Slider slider1;
    public Transform pos;
    public float valor;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Movimiento();        
        
    }
    public void Movimiento()
    {
        valor = (slider1.value);
        Text1.text = valor.ToString();
        pos.transform.position = new Vector3(-1.2561f, 4.275f + valor, 3.958f);
    }
}
