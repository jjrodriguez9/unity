﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;
using System;
using NVIDIA;
using NVIDIA.Flex;
using UnityEngine.UI;
using System.Collections.Specialized;
using System.Security.Cryptography;

public class comunicacion : MonoBehaviour
{
    public bool Generar_Perturbacion_electrica;
    public bool Generar_Perturbacion_Mangera;
    public bool Generar_Perturbacion_Sensor;
    public float Perturbacion_Electrica;
    public float Perturbacion_Sensor;
    private Memoryshare memoryshare;

    [SerializeField]
    private float Area1;

    [SerializeField]
    private float Area2;

    [SerializeField]
    private float ho1;

    [SerializeField]
    private float ho2;

    [SerializeField]
    private float valve1;

    [SerializeField]
    private float valve2;

    [SerializeField]
    private float valve3;

    [SerializeField]
    private float k1;

    [SerializeField]
    private float k2;

    [SerializeField]
    private float k3;

    [SerializeField]
    private float h1;

    [SerializeField]
    private float h2;

    private float h1ant;
    private float h2ant;
    private float hpp;
    private float hpp2;
    private float g;
    
    public Text ValA1;
    public Text ValA2;
    public Text ValA3;
    public Text ValH1;
    public Text ValH2;

    public Transform llave2;
    public Transform llave3;
    public Transform flujo1;
    public Transform flujo2;

    public Transform Barras;
    public Transform Barras2;

    public GameObject Particula;

    public Transform Altura1;
    public Transform Altura2;

    public float Fmulti1;
    public float Fmulti2;
    
    float To;
    float clock;
    float vo1 = 0.000f;
    float vo1aux = 0.000f;
    float vo2aux = 0.000f;
    float vi1;
    float vt1;
    float vo2 = 0.000f;
    float vi2;
    float vt2;
    float ts = 0.001f;
    float a;
    float b;
    float cper = 0;

    void Start()
    {
        memoryshare = new Memoryshare();
        memoryshare.freeMemoriShare();
        memoryshare.createMemoriShare("Project", 50, 2);
        memoryshare.openMemoryShare("Project", 2);
        memoryshare.Setfloat("Project",0, 2);
        memoryshare.Setfloat("Project",0,3);
        h1ant = ho1;
        h2ant = ho2;
        g = 9.8f;

    }

    void Update()
    {

        To += Time.deltaTime;
        clock += Time.deltaTime;
        memoryshare.Setfloat("Project", clock,1);
        valve1 = memoryshare.Getfloat("Project", 4);
        valve2 = memoryshare.Getfloat("Project", 5);
        valve3 = memoryshare.Getfloat("Project", 6);

        if (Generar_Perturbacion_electrica==true)
        {
            valve1 = valve1 + Perturbacion_Electrica * UnityEngine.Random.Range(-1,1);
            valve2 = valve2 + Perturbacion_Electrica * UnityEngine.Random.Range(-1, 1);
        }
        if(Generar_Perturbacion_Mangera==true) {
            cper = 0.1f * UnityEngine.Random.Range(0, 1);
            Particula.SetActive(true);
        }
        else
        {
            cper = 0;
            Particula.SetActive(false);
        }
        if (Generar_Perturbacion_Sensor == true)
        {
            a = Perturbacion_Sensor * UnityEngine.Random.Range(-1, 1);
            b = Perturbacion_Sensor * UnityEngine.Random.Range(-1, 1);
        }
        else
        {
            a = 0;
            b= 0;
        }


        hpp = (k1 * valve1 - k2 * valve2 * (float)Math.Sqrt(2 * g * h1ant)) / Area1;
        h1 = hpp * To + h1ant;
        if (h1 < 0.0001)
        {
            h1 = 0.00001f;
        }

        float hs1 = h1 + a;
        float hs2 = h2 + b;

        memoryshare.Setfloat("Project", hs1,2);
        hpp2 = (k2 * valve2 * (float)Math.Sqrt(2 * g * h1) - k3 * valve3 * (float)Math.Sqrt(2 * g * h2ant)) / Area2 ;
        h2 = hpp2 * To + h2ant - cper;
        if (h2 < 0.0001)
        {
            h2 = 0.00001f;
        }


        memoryshare.Setfloat("Project",hs2,3);
        h1ant = h1;
        h2ant = h2;

        if (h1 > 1)
        {

        }
        else
        {
            Altura1.localPosition = new Vector3(Altura1.localPosition.x, -781f + h1 * Fmulti1*0.7f, Altura1.localPosition.z);
            Altura1.localScale = new Vector3(Altura1.localScale.x, 10 + h1 * Fmulti2, Altura1.localScale.z);
        }


        if (h2 > 1)
        {

        }
        else
        {
            Altura2.localPosition = new Vector3(Altura1.localPosition.x, -781f + h2 * Fmulti1 * 0.7f, Altura1.localPosition.z);
            Altura2.localScale = new Vector3(Altura1.localScale.x, 10 + h2 * Fmulti2, Altura1.localScale.z);            
        }
        flujo1.localScale = new Vector3(100 * valve1, 900, 100 * valve1);
        flujo2.localScale = new Vector3(100 * valve2, 900, 100 * valve2);
        llave2.rotation = Quaternion.Euler(0,-90*valve2, 0);
        llave3.rotation = Quaternion.Euler(0, -90 *valve2, 0);

        ValA1.text = "% Apertura1: " + valve1.ToString();
        ValA2.text = "% Apertura2: " + valve2.ToString();
        ValA3.text = "% Apertura3: " + valve3.ToString();
        ValH1.text = "Altura Tanque 1: "  + h1;
        ValH2.text = "Altura Tanque 2: " + h2;
        Barras.localScale = new Vector3(Barras.localScale.x, 1.1f * h1, Barras.localScale.z);
        Barras2.localScale = new Vector3(Barras2.localScale.x, 1.1f * h2, Barras2.localScale.z);

        Barras2.localPosition = new Vector3(Barras2.localPosition.x, 2.5f + 1.1f * h2 / 2, Barras2.localPosition.z);
        Barras.localPosition = new Vector3(Barras.localPosition.x, 1.14f + 1.1f * h1 / 2, Barras.localPosition.z);

        Debug.Log(clock);
        To = 0;
    }
}
