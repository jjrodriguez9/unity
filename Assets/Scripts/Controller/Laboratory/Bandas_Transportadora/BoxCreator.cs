﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using System.IO;

public class BoxCreator : MonoBehaviourPun
{
    [SerializeField]
    private GameObject referencia;
    [SerializeField]
    private GameObject _boxPrefab;
    public float _timeUpdate = 1f;
    private bool start;
    private float To = 0f;
    [SerializeField]
    private PhotonView PV;
    // Update is called once per frame
    void Start()
    {
        if (!DataInfo.dataInfo.singlePractice)
        {
            PV = GetComponent<PhotonView>();
            //Inicializate();
        }
    }
    void Update()
    {
        if (!DataInfo.dataInfo.singlePractice)
        {
            if (PV.IsMine)
            {
                dataUpdate();
            }
        }
        else
        {
            dataUpdate();
        }
    }
    private void Inicializate()
    {
        if (!start)
        {
            Debug.Log("empezar");
            StartCoroutine(wait(10));
        }
    }

    private void dataUpdate()
    {
        if (Input.GetButtonDown("active_create"))
        {
            if (!start)
            {
                start = true;
            }
            else
            {
                start = false;
            }
        }
        if (start)
        {
            To = To + Time.deltaTime;
            if (To >= _timeUpdate)
            {
                if (!DataInfo.dataInfo.singlePractice)
                {
                    PV.RPC("createBox", RpcTarget.All);
                }
                else
                {
                    createBox();
                }
            }
        }
    }
    [PunRPC]
    private void createBox()
    {
        if (!DataInfo.dataInfo.singlePractice)
        {
            PhotonNetwork.InstantiateRoomObject(Path.Combine("Laboratory/Bandas_Transportadora", "CardBox"), referencia.transform.position, referencia.transform.rotation);
        }
        else
        {
            Instantiate(_boxPrefab, referencia.transform.position, referencia.transform.rotation);
        }
        To = 0f;
    }
    IEnumerator wait(int seconds)
    {
        Debug.Log("tiempo de espera");
        yield return new WaitForSeconds(seconds);
        Debug.Log("luego de los tiempo: " + seconds);
        Instantiate(_boxPrefab);
        start = true;
    }
}
