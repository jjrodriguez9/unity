﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;
using Photon.Pun;

public class MemoryONE_Bandas : MonoBehaviourPun
{
    private readonly int totalMem = 50;
    public float[] Current_RPM { get => _current_RPM; set => _current_RPM = value; }
    public float[] SP_RPM { get => _sp_RPM; set => _sp_RPM = value; }
    public float[] Error_RPM { get => _error_RPM; set => _error_RPM = value; }
    public float[] Controller_RPM { get => _controller_RPM; set => _controller_RPM = value; }
    public float Close_App { get => _close_App; set => _close_App = value; }
    
    private Memoryshare memoryshare;
    private float _close_App;
    private float[] _controller_RPM;
    private float[] _error_RPM;
    private float[] _sp_RPM;
    private float[] _current_RPM;
    
    private PhotonView PV;
    private void Awake()
    {
        _current_RPM = new float[3] { 0f, 0f, 0f };
        _sp_RPM = new float[3] { 0f, 0f, 0f };
        _error_RPM = new float[3] { 0f, 0f, 0f };
        _controller_RPM = new float[3] { 0f, 0f, 0f };
        _close_App = 0f;
    }

    // Use this for initialization
    void Start()
    {
        if (!DataInfo.dataInfo.singlePractice)
        {
            PV = GetComponent<PhotonView>();
            Inicializate();
        }
        else
        {
            Inicializate();
        }
    }
    private void Inicializate()
    {
        memoryshare = new Memoryshare();
        memoryshare.createMemoriShare("MemoriaONE", 50, 2);
        memoryshare.openMemoryShare("MemoriaONE", 2);

        for (int i = 0; i < totalMem; i++)
        {
            memoryshare.Setfloat("MemoriaONE", 0f, i);
        }
    }
    // Update is called once per frame
    void Update()
    {
        if (!DataInfo.dataInfo.singlePractice)
        {
            if (PV.IsMine)
            {
                dataUpdate();
            }
        }
        else
        {
            dataUpdate();
        }
    }
    private void dataUpdate()
    {
        int pos_mem;
        int i = 0;
        int le = _error_RPM.Length;
        int lc = _controller_RPM.Length;

        pos_mem = 0;
        foreach (float val in _current_RPM)
        {
            memoryshare.Setfloat("MemoriaONE", val, pos_mem);
            pos_mem++;
        }

        pos_mem = 3;
        foreach (float val in _sp_RPM)
        {
            memoryshare.Setfloat("MemoriaONE", val, pos_mem);
            pos_mem++;
        }

        pos_mem = 6;
        memoryshare.Setfloat("MemoriaONE", _close_App, pos_mem);



        pos_mem = 7;
        for (i = 0; i < le; i++)
        {
            _error_RPM[i] = memoryshare.Getfloat("MemoriaONE", pos_mem);
            pos_mem++;
        }

        pos_mem = 10;
        for (i = 0; i < lc; i++)
        {
            _controller_RPM[i] = memoryshare.Getfloat("MemoriaONE", pos_mem);
            pos_mem++;
        }
    }
    // Update is called once per frame
    void OnApplicationQuit()
    {
        _close_App = 1f;
        memoryshare.Setfloat("MemoriaONE",_close_App,6);
    }


    void OnDestroy()
    {
        _close_App = 1f;
        memoryshare.Setfloat("MemoriaONE",_close_App,6);
    }
}