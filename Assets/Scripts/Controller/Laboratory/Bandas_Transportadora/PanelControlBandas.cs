﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using UnityEngine.SceneManagement;
using TMPro;

public class PanelControlBandas : MonoBehaviourPun
{
    [SerializeField]
    private Transform _player;
    [SerializeField]
    private GameObject _banda1;
    [SerializeField]
    private GameObject _banda2;
    [SerializeField]
    private GameObject _banda3;
    [SerializeField]
    private Slider _sliderRPM1;
    [SerializeField]
    private Slider _sliderRPM2;
    [SerializeField]
    private Slider _sliderRPM3;
    [SerializeField]
    private TMP_Text[] labelValue; //declarando la asignacion del valores de entrada(Slider) para su funcionamiento 
    [SerializeField]
    private int _distance = 5;
    [SerializeField]
    private SingleLaboratory singleLaboratory;
    [SerializeField]
    private PhotonView PV;
    // Start is called before the first frame update
    void Start()
    {
        if (!DataInfo.dataInfo.singlePractice)
        {
            PV = GetComponent<PhotonView>();
            if (PV.IsMine)
            {
                Inicializate();
            }
            else
            {
                _player = GameSetup.gameSetup._player.transform;
            }
        }
        else
        {
            Inicializate();
        }
    }
    private void Inicializate()
    {
        if (SceneManager.GetActiveScene().buildIndex == 4)
        {
            singleLaboratory = GameObject.Find("SingleLaboratory").GetComponent<SingleLaboratory>();
            _player = singleLaboratory._player.transform;
        }
        else if (SceneManager.GetActiveScene().buildIndex == MultiplayerSetting.multiplayerSetting.multiplayerScene)
        {
            _player = GameSetup.gameSetup._player.transform;
        }
    }
    // Update is called once per frame
    void FixedUpdate()
    {
        if (!DataInfo.dataInfo.singlePractice)
        {
            controllerbanda_transporte();
            PV.RPC("label", RpcTarget.All);
        }
        else
        {
            controllerbanda_transporte();
            label();
        }
    }
    [PunRPC]
    private void label()
    {
        labelValue[0].text = _sliderRPM1.value.ToString();
        labelValue[1].text = _sliderRPM2.value.ToString();
        labelValue[2].text = _sliderRPM3.value.ToString();
    }
    private void controllerbanda_transporte()
    {
        if ((Vector3.Distance(_banda1.transform.position, _player.position) < _distance) || (Vector3.Distance(_banda2.transform.position, _player.position) < _distance) || (Vector3.Distance(_banda3.transform.position, _player.position) < _distance))
        {
            // Change Setpoint RPM 1
            if (Input.GetButton("active_x"))
            {
                float value = Input.GetAxis("slider_move");
                if (!DataInfo.dataInfo.singlePractice)
                {
                    PV.RPC("valueX", RpcTarget.All, value);
                }
                else
                {
                    valueX(value);
                }
            }
            // Change Setpoint RPM 2"
            if (Input.GetButton("active_y"))
            {
                float value = Input.GetAxis("slider_move");
                if (!DataInfo.dataInfo.singlePractice)
                {
                    PV.RPC("valueY", RpcTarget.All, value);
                }
                else
                {
                    valueY(value);
                }
            }
            // Change Setpoint RPM 3
            if (Input.GetButton("active_z"))
            {
                float value = Input.GetAxis("slider_move");
                if (!DataInfo.dataInfo.singlePractice)
                {
                    PV.RPC("valueZ", RpcTarget.All, value);
                }
                else
                {
                    valueZ(value);
                }
            }
        }
    }

    [PunRPC]
    private void valueX(float val)
    {
        if (val < 0)
        {
            _sliderRPM1.value = _sliderRPM1.value + 1f;
        }
        else if (val > 0)
        {
            _sliderRPM1.value = _sliderRPM1.value - 1f;
        }
    }
    [PunRPC]
    private void valueY(float val)
    {
        if (val < 0)
        {
            _sliderRPM2.value = _sliderRPM2.value + 1f;
        }
        else if (val > 0)
        {
            _sliderRPM2.value = _sliderRPM2.value - 1f;
        }
    }
    [PunRPC]
    private void valueZ(float val)
    {
        if (val < 0)
        {
            _sliderRPM3.value = _sliderRPM3.value + 1f;
        }
        else if (val > 0)
        {
            _sliderRPM3.value = _sliderRPM3.value - 1f;
        }
    }
}
