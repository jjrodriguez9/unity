﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
public class BoxParticlePlayer : MonoBehaviourPun
{
    [SerializeField]
    private ParticleSystem _particleSystem;
    [SerializeField]
    private AudioSource _audioSource;
    [SerializeField]
    private PhotonView PV;
    void Start()
    {
        if (!DataInfo.dataInfo.singlePractice)
        {
            PV = GetComponent<PhotonView>();
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (!DataInfo.dataInfo.singlePractice)
        {
            PV.RPC("sound", RpcTarget.All, other.tag);
        }
        else
        {
            sound(other.tag);
        }
    }

    [PunRPC]
    private void sound(string other)
    {
        if (other.Equals("Floor"))
        {
            _particleSystem.Play();
            _audioSource.Play();
        }
    }
}
