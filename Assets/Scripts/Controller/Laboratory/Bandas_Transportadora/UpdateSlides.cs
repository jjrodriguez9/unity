﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpdateSlides : MonoBehaviour
{
    private Slider _slider;
    //private Text _text;




    // Start is called before the first frame update
    void Start()
    {
        _slider = GetComponent<Slider>();  
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        foreach (Text txt in GetComponentsInChildren<Text>())
        {
            if (txt.gameObject.name == "Value")
            {
                txt.text = _slider.value.ToString(".0");
            }
        }
    }
}
