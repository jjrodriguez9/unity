﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Photon.Pun;
public class DeliveredBoxDestroyer : MonoBehaviourPun
{
    [SerializeField]
    private TMP_Text _deliveredBoxNum;
    [SerializeField]
    private PhotonView PV;
    public PhotonView _PV { get { return PV; } }
    void Start()
    {
        if (!DataInfo.dataInfo.singlePractice)
        {
            PV = GetComponent<PhotonView>();
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (!DataInfo.dataInfo.singlePractice)
        {
            PV.RPC("delete", RpcTarget.All, other.tag);
        }
        else
        {
            delete(other.tag);
        }
        if (other.tag.Equals("CardBox"))
        {
            if (!DataInfo.dataInfo.singlePractice)
            {
                //Destroy(other.gameObject, .5f);
                StartCoroutine(wait(other));
            }
            else
            {
                Destroy(other.gameObject, .5f);
            }
        }
    }
    [PunRPC]
    private void delete(string other)
    {
        if (other.Equals("CardBox"))
        {
            int n = int.Parse(_deliveredBoxNum.text);
            _deliveredBoxNum.text = (n + 1).ToString();
        }
    }
    IEnumerator wait(Collider other)
    {
        yield return new WaitForSeconds(1);
        PhotonNetwork.Destroy(other.gameObject);
    }
}
