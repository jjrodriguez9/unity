﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Manager_Bandas : MonoBehaviour
{
    [SerializeField]
    private GameObject structure;
    void Awake()
    {
        if (SceneManager.GetActiveScene().buildIndex == MultiplayerSetting.multiplayerSetting.multiplayerScene)
        {
            structure = gameObject.transform.Find("Galpón").gameObject.transform.Find("Galpon").gameObject ;
            structure.SetActive(false);
        }
    }
}
