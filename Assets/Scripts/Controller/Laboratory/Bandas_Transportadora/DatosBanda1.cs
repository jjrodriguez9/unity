﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Photon.Pun;
using ChartAndGraph;
public class DatosBanda1 : MonoBehaviourPun
{
    // Parameters along with the step out waveform
    private float wmax = 6200.85f;  // rad/s
    private float Imax = 30.0f;       // A
    // Electromagnetic Voltage Constant, Kb(V* s/rad)
    private float Kb = 12.6f;
    // Motor Torque constant, Kt(N* m/A)
    private float Kt = 8.15f;
    // Motor Friction constant, Kf(N* m / rad/s)
    private float Kf = 0.515f;
    // Total Armature resistance, Ra(ohms)
    private float Ra = 12.7f;
    // Total Armature inductance, La(H)
    private float La = 0.380f;
    // Armature moment of inertia, Jo(Kg * m ^ 2)
    private float Jo = 0.473f;
    // Voltaje Motor Applied in V
    private float Va = 120f;
    // Ratio measured from motor axis to band, r(m)
    private float r = 0.25f;
    // Mass of the objet to be transported over the band, M (Kg)
    private float M = 0f;
    // Sampling time
    [SerializeField]
    private float Ts = 0.001f;
    [SerializeField]
    private float _initialMassBand = 18f;
    //[SerializeField]
    //private Graphic _graph;
    [SerializeField]
    private GraphChart graph;
    private float lastTime = 0f;
    [SerializeField]
    private Slider _sp_RPM;
    [SerializeField]
    private Text _text_RPM;
    [SerializeField]
    private TMP_Text _mass_Band;
    [SerializeField]
    private float _longitudBanda = 2f;
    [SerializeField]
    private float _animationFactor = 0.4f;

    private Animation _bandaAnimation;

    private int pos_vec = 0;
    private float i_a;
    private float w;
    private float sp_rpm;
    private float controller;
    private float error;
    private float current_rpm;

    private float To;
    private float Ti;
    private float T_banda;
    [SerializeField]
    private MemoryONE_Bandas memoryONE_Bandas;
    [SerializeField]
    private PhotonView PV;
    public PhotonView _PV { get { return PV; } }
    // Start is called before the first frame update
    void Start()
    {
        if (!DataInfo.dataInfo.singlePractice)
        {
            PV = GetComponent<PhotonView>();
            Inicializate();
        }
        else
        {
            Inicializate();
        }
    }
    private void Inicializate()
    {
        memoryONE_Bandas = GameObject.Find("MemoryHandle").GetComponent<MemoryONE_Bandas>();
        i_a = 0f;
        w = 0f;
        current_rpm = 0f;
        M = _initialMassBand;
        sp_rpm = _sp_RPM.value;
        _bandaAnimation = GetComponent<Animation>();
        _bandaAnimation.Play("Banda1");
        _bandaAnimation["Banda1"].speed = 0;
        if (graph == null) // the ChartGraph info is obtained via the inspector
            return;
        graph.DataSource.StartBatch(); // calling StartBatch allows changing the graph data without redrawing the graph for every change
        graph.DataSource.ClearCategory("z"); // clear the "Player 1" category. this category is defined using the GraphChart inspector
        graph.DataSource.ClearCategory("y"); // clear the "Player 1" category. this category is defined using the GraphChart inspector
        graph.DataSource.ClearCategory("x"); // clear the "Player 1" category. this category is defined using the GraphChart inspector
        graph.DataSource.EndBatch(); // finally we call EndBatch , this will cause the GraphChart to redraw itself
    }
    // Update is called once per frame
    void Update()
    {
        To = To + Time.deltaTime;

        if (To >= Ts)
        {
            if (!DataInfo.dataInfo.singlePractice)
            {
                if (PV.IsMine)
                {
                    calculatebanda();
                }
            }
            else
            {
                calculatebanda();
            }
            To = 0f;        
        }
        
    }
    private void calculatebanda()
    {
        /*
            * RECUPERACIÓN DE VALORES DESDE LA MEMORIA COMPARTIDA
            */
        // Recuperación de la salida del controlador (Recordar que la posición del vector depende del número de Banda n-1).
        controller = memoryONE_Bandas.Controller_RPM[pos_vec];
        // Recuperación del valor de error calculado en Matlab
        error = memoryONE_Bandas.Error_RPM[pos_vec];

        /*
         * APLICACIÓN DEL MODELO DINÁMICO DE LA BANDA TRANSPORTADORA
         */
        // Ejecuto el modelo usando el espacio de estados de la Banda Transportadora. Se debe usar además el valor del controlador como entrada al Voltaje de armadura Va
        Va = controller;
        float den = (Jo + 2 * Mathf.Pow(r, 2) * M);
        float dw = -(Kf / den) * w + (Kt / den) * i_a;
        float di_a = -(Kb / La) * w - (Ra / La) * i_a + (Va / La);
        // Actualizo los siguientes estados para Velocidad Angular de la banda (w) y Corriente de Armadura del motor (i_a)
        w = w + dw * Ts;
        i_a = i_a + di_a * Ts;


        // VErificar que los valores máximos no sean sobrepasados
        if (i_a > Imax)
            i_a = Imax;
        if (w > wmax)
            w = wmax;
        /*
         * CONVERSIONES A RPM Y VELOCIDAD DE ANIMACIÓN
         */
        // Se convierte la Velocidad Angular a valores de RPM
        current_rpm = (w * 60f) / (2 * Mathf.PI);
        // Convierto  velocidad angular al factor de velocidad de la animación
        float speed = (w * _animationFactor) / Mathf.PI;

        /*
         * ACTUALIZAR LAS ANIMACIONES Y VISUALIZACIONES
         */
        _bandaAnimation["Banda1"].speed = speed;
        if (!DataInfo.dataInfo.singlePractice)
        {
            PV.RPC("RPMsped", RpcTarget.All, current_rpm.ToString());
            PV.RPC("multiGrafics", RpcTarget.All,sp_rpm, error, current_rpm);
        }
        else
        {
            RPMsped(current_rpm.ToString());
            singleGrafics();
        }
        //_graph.AddPoint(current_rpm, 0);
        //_graph.AddPoint(sp_rpm, 1);
        //_graph.AddPoint(error, 2);

        /*
         * PREPARO LOS VALORES PARA ENVIAR A MATLAB
         */
        // Consulto el valor de Set Point para las RPM
        sp_rpm = _sp_RPM.value;
        memoryONE_Bandas.SP_RPM[pos_vec] = sp_rpm;
        memoryONE_Bandas.Current_RPM[pos_vec] = current_rpm;
    }
    
    private void singleGrafics()
    {
        float time = Time.time;
        if (lastTime + 0.4f < time)
        {
            lastTime = time;
            graph.DataSource.AddPointToCategoryRealtime("z", time, current_rpm); // each time we call AddPointToCategory 
            graph.DataSource.AddPointToCategoryRealtime("y", time, error); // each time we call AddPointToCategory 
            graph.DataSource.AddPointToCategoryRealtime("x", time, sp_rpm); // each time we call AddPointToCategory 
        }
    }
    [PunRPC]    
    private void multiGrafics(float x, float y, float z)
    {
        float time = Time.time;
        if (lastTime + 0.4f < time)
        {
            lastTime = time;
            graph.DataSource.AddPointToCategoryRealtime("z", time, z); // each time we call AddPointToCategory 
            graph.DataSource.AddPointToCategoryRealtime("y", time, y); // each time we call AddPointToCategory 
            graph.DataSource.AddPointToCategoryRealtime("x", time, x); // each time we call AddPointToCategory 
        }
    }
    [PunRPC]
    private void RPMsped(string rpm)
    {
        _text_RPM.text = rpm;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "CardBox")
        {
            M = M + 5;  // Aumenta el peso de la caja que entra a la banda
            if (!DataInfo.dataInfo.singlePractice)
            {
                PV.RPC("masa", RpcTarget.All, M.ToString());
            }
            else
            {
                masa(M.ToString());
            }
        }

        if (other.gameObject.tag == "SensorBanda")
        {
            Ti = Time.time;            
        }
    }
    [PunRPC]
    private void masa(string masa)
    {
        _mass_Band.text = masa;
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "CardBox")
        {
            M = M - 5;  // Desminuye el peso de la caja que sale de la banda
            if (!DataInfo.dataInfo.singlePractice)
            {
                PV.RPC("masa", RpcTarget.All, M.ToString());
            }
            else
            {
                masa(M.ToString());
            }
        }

        if (other.gameObject.tag == "SensorBanda")
        {
            T_banda = Time.time - Ti;
            float vel_lineal = _longitudBanda / T_banda;
            float vel_angular = vel_lineal / r;
            float rpm_val = (vel_angular * 60f) / (2 * Mathf.PI);
            //Debug.Log("RPM Actual: " + rpm_val);
        }
    }
}
