﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ChartAndGraph;
using Photon.Pun;

public class PlottingUR3 : MonoBehaviourPun
{
	private float[] error_XYZ_A6;
	[SerializeField]
    private float Ts = 0.01f; // Tiempo de muestreo en Unity
	[SerializeField]
	private GraphChart graph;
	private float lastTime = 0f;
	[SerializeField]
	private MemoryONE_UR3 memoryONE_UR3;
	[SerializeField]
	private PhotonView PV;
	[SerializeField]
	private int _distance = 50;
	[SerializeField]
	private PanelControl panelControl;
	private void Start()
	{
		if (!DataInfo.dataInfo.singlePractice)
		{
			PV = GetComponent<PhotonView>();
			panelControl = GameObject.Find("PanelControlHandle").GetComponent<PanelControl>();
			Inicializate();
		}
		else
		{
			Inicializate();
		}
	}
	private void Inicializate()
    {
		if (graph == null) // the ChartGraph info is obtained via the inspector
			return;
		graph.DataSource.StartBatch(); // calling StartBatch allows changing the graph data without redrawing the graph for every change
		graph.DataSource.ClearCategory("z"); // clear the "Player 1" category. this category is defined using the GraphChart inspector
		graph.DataSource.ClearCategory("y"); // clear the "Player 1" category. this category is defined using the GraphChart inspector
		graph.DataSource.ClearCategory("x"); // clear the "Player 1" category. this category is defined using the GraphChart inspector
		graph.DataSource.EndBatch(); // finally we call EndBatch , this will cause the GraphChart to redraw itself
		memoryONE_UR3 = GameObject.Find("MemoryHandle").GetComponent<MemoryONE_UR3>();
		error_XYZ_A6 = new float[3] { 0f, 0f, 0f };
	}
	// FixedUpdate is called every 0.02 seconds (default)
	void FixedUpdate()
	{
		error_XYZ_A6 = memoryONE_UR3.Error_XYZ_A6;
		if (!DataInfo.dataInfo.singlePractice)
		{
			if (PV.IsMine)
			{
				if (Vector3.Distance(panelControl.transform.position, GameSetup.gameSetup._player.transform.position) < _distance)
				{
					PV.RPC("multiGrafics", RpcTarget.All, error_XYZ_A6[0], error_XYZ_A6[1], error_XYZ_A6[2]);
				}
			}
		}
		else
		{
			singleGrafics();
		}
	}

	private void singleGrafics()
	{
		float time = Time.time;
		if (lastTime + 0.4f < time)
		{
			lastTime = time;
			//Debug.Log("sa: " + error_XYZ_A6[0] + " saw: " + error_XYZ_A6[1] + " qw: " + error_XYZ_A6[2]);
			graph.DataSource.AddPointToCategoryRealtime("x", error_XYZ_A6[0], time); // each time we call AddPointToCategory 
			graph.DataSource.AddPointToCategoryRealtime("y", error_XYZ_A6[1], time); // each time we call AddPointToCategory 
			graph.DataSource.AddPointToCategoryRealtime("z", error_XYZ_A6[2], time); // each time we call AddPointToCategory 
		}
	}
	[PunRPC]
	private void multiGrafics(float x, float y, float z)
	{
		float time = Time.time;
		if (lastTime + 0.4f < time)
		{
			lastTime = time;
			graph.DataSource.AddPointToCategoryRealtime("z", time, z); // each time we call AddPointToCategory 
			graph.DataSource.AddPointToCategoryRealtime("y", time, y); // each time we call AddPointToCategory 
			graph.DataSource.AddPointToCategoryRealtime("x", time, x); // each time we call AddPointToCategory 
		}
	}
}
