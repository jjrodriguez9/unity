﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using UnityEngine.SceneManagement;

public class PanelControl : MonoBehaviourPun
{
    [SerializeField]
    private Transform _player;
    [SerializeField]
    private GameObject _robot;
    [SerializeField]
    private int _distance = 5;
    [SerializeField]
    private DatosUR3 datosUR3;
    [SerializeField]
    private SingleLaboratory singleLaboratory;
    [SerializeField]
    private PhotonView PV;

    // Start is called before the first frame update
    void Start()
    {
        if (!DataInfo.dataInfo.singlePractice)
        {
            PV = GetComponent<PhotonView>();
            if (PV.IsMine)
            {
                Inicializate();
            }
            else
            {
                _player = GameSetup.gameSetup._player.transform;
            }
        }
        else
        {
            Inicializate();
        }
    }
    private void Inicializate()
    {
        datosUR3 = GameObject.Find("UR3_Execution").GetComponent<DatosUR3>();
        if (SceneManager.GetActiveScene().buildIndex == 4)
        {
            singleLaboratory = GameObject.Find("SingleLaboratory").GetComponent<SingleLaboratory>();
            _player = singleLaboratory._player.transform;
        }
        else if (SceneManager.GetActiveScene().buildIndex == MultiplayerSetting.multiplayerSetting.multiplayerScene)
        {
            _player = GameSetup.gameSetup._player.transform;
        }
    }
    // Update is called once per frame
    void FixedUpdate()
    {
        if (!DataInfo.dataInfo.singlePractice)
        {
            controllerUR3();
            datosUR3._PV.RPC("label", RpcTarget.All);
        }
        else
        {
            controllerUR3();
            label();
        }
        
    }
    private void label()
    {
        datosUR3.labelValue[0].text = datosUR3.sliderX.value.ToString();
        datosUR3.labelValue[1].text = datosUR3.sliderY.value.ToString();
        datosUR3.labelValue[2].text = datosUR3.sliderZ.value.ToString();
    }
    private void controllerUR3()
    {
        if (Vector3.Distance(transform.position, _player.position) < _distance)
        {
            // Change Setpoint X
            if (Input.GetButton("active_x"))
            {
                float value = Input.GetAxis("slider_move");
                if (!DataInfo.dataInfo.singlePractice)
                {
                    PV.RPC("valueX", RpcTarget.All, value);
                }
                else
                {
                    valueX(value);
                }
            }
            // Change Setpoint Y
            if (Input.GetButton("active_y"))
            {
                float value = Input.GetAxis("slider_move");
                if (!DataInfo.dataInfo.singlePractice)
                {
                    PV.RPC("valueY", RpcTarget.All, value);
                }
                else
                {
                    valueY(value);
                }
            }
            // Change Setpoint Z
            if (Input.GetButton("active_z"))
            {
                float value = Input.GetAxis("slider_move");
                if (!DataInfo.dataInfo.singlePractice)
                {
                    PV.RPC("valueZ", RpcTarget.All, value);
                }
                else
                {
                    valueZ(value);
                }
            }
        }
    }
    [PunRPC]
    private void valueX(float val)
    {
        if (val < 0)
        {
            datosUR3.sliderX.value = datosUR3.sliderX.value - 0.01f;
        }
        else if (val > 0)
        {
            datosUR3.sliderX.value = datosUR3.sliderX.value + 0.01f;
        }
    }
    [PunRPC]
    private void valueY(float val)
    {
        if (val < 0)
        {
            datosUR3.sliderY.value = datosUR3.sliderY.value - 0.01f;
        }
        else if (val > 0)
        {
            datosUR3.sliderY.value = datosUR3.sliderY.value + 0.01f;
        }
    }
    [PunRPC]
    private void valueZ(float val)
    {
        if (val < 0)
        {
            datosUR3.sliderZ.value = datosUR3.sliderZ.value - 0.01f;
        }
        else if (val > 0)
        {
            datosUR3.sliderZ.value = datosUR3.sliderZ.value + 0.01f;
        }
    }
}
