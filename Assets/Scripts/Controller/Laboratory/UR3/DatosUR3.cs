﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Photon.Pun;

public class DatosUR3 : MonoBehaviourPun
{
    [SerializeField]
    private GameObject _gameObjectA1;
    [SerializeField]
    private GameObject _gameObjectA2;
    [SerializeField]
    private GameObject _gameObjectA3;
    [SerializeField]
    private GameObject _gameObjectA4;
    [SerializeField]
    private GameObject _gameObjectA5;
    [SerializeField]
    private GameObject _gameObjectA6;
    [SerializeField]
    private Slider _sliderX; //declarando la asignacion del parametro de entrada(Slider) para su funcionamiento 
    public Slider sliderX { set { this._sliderX = value; } get { return _sliderX; } }
    [SerializeField]
    private Slider _sliderY; //declarando la asignacion del parametro de entrada(Slider) para su funcionamiento 
    public Slider sliderY { set { this._sliderY = value; } get { return _sliderY; } }
    [SerializeField]
    private Slider _sliderZ; //declarando la asignacion del parametro de entrada(Slider) para su funcionamiento 
    public Slider sliderZ { set { this._sliderZ = value; } get { return _sliderZ; } }
    [SerializeField]
    private TMP_Text[] _labelValue; //declarando la asignacion del valores de entrada(Slider) para su funcionamiento 
    public TMP_Text[] labelValue { set { this._labelValue = value; } get { return _labelValue; } }
    [SerializeField]
    private AudioSource _audioSource;
    [SerializeField]
    private ParticleSystem _smokeParticle;
    private RequestHttp requestHttp; //Intanciando la clase para realizar los metodos de peticion y la url a utilizar
    private TypeRequest typeRequest; //Intanciando la clase para realizar el tipo de peticion que se requiere
    private EventPractice eventPractice; //instancio la clase realizar el json para el envio al api
    private DataEvent dataEvent; //instancio la clase para almacenar los datos
    private float old_x, old_y, old_z;
    private float[,] Jac;
    private float[,] DPos_A6;

    private float Ts; // Tiempo de muestreo en Unity
    private float[] current_XYZ_A6;
    private float[] sp_XYZ_A6;
    private float[] current_Q;
    private float close_App;
    private float[] error_XYZ_A6;
    private float[] error_QP;


    private float[] current_QP;
    private float[] controller_Q;
    private float[] controller_QP;
    private float[] controller_QPP;
    private float mass_exter;
    private float[,] Iexter;
    private float g;
    private float[] m;
    private float[] current_QPP;
    private float[] older_QPP;
    private float[] older_QP;

    private float To;
    [SerializeField]
    private MemoryONE_UR3 memoryONE_UR3;
    [SerializeField]
    private PhotonView PV;
    public PhotonView _PV { get { return PV; } }

    private int pos_index; //posicion de la memoria compartida


    // Start is called before the first frame update
    void Start()
    {
        if (!DataInfo.dataInfo.singlePractice)
        {
            PV = GetComponent<PhotonView>();
            Inicializate();
        }
        else
        {
            Inicializate();
        }
    }
    private void Inicializate()
    {
        memoryONE_UR3 = GameObject.Find("MemoryHandle").GetComponent<MemoryONE_UR3>();
        requestHttp = new RequestHttp(); //realizacion de la instancia para utilizar las peticiones hacia el api;
        typeRequest = new TypeRequest(); //realizacion de la instancia para obtener el rol o el tipo de requeste que se requiere hacer al api
        current_XYZ_A6 = new float[3] { 0f, 0f, 0f };
        sp_XYZ_A6 = new float[3] { 0f, 0f, 0f };
        current_Q = new float[6] { 0f, 0f, 0f, 0f, 0f, 0f };
        current_QP = new float[6] { 0f, 0f, 0f, 0f, 0f, 0f };
        current_QPP = new float[6] { 0f, 0f, 0f, 0f, 0f, 0f };
        older_QPP = new float[6] { 0f, 0f, 0f, 0f, 0f, 0f };
        older_QP = new float[6] { 0f, 0f, 0f, 0f, 0f, 0f };
        error_XYZ_A6 = new float[3] { 0f, 0f, 0f };
        error_QP = new float[6] { 0f, 0f, 0f, 0f, 0f, 0f };
        controller_Q = new float[6] { 0f, 0f, 0f, 0f, 0f, 0f };
        controller_QP = new float[6] { 0f, 0f, 0f, 0f, 0f, 0f };
        controller_QPP = new float[6] { 0f, 0f, 0f, 0f, 0f, 0f };

        current_Q = Get_CurrentQ_UR3(); //in rads
        current_XYZ_A6 = MyFunctionsUR3.Get_DKinematic_UR3(current_Q); //Inicialización de coordenas XYZ del efector final del robot KUKA en función de los angulos de articulaciones
        // Cargo en las slides del SP las coordenadas actuales
        _sliderX.value = current_XYZ_A6[0];
        _sliderY.value = current_XYZ_A6[1];
        _sliderZ.value = current_XYZ_A6[2];
        // Capturo los valores de SP en el vector sp_A6
        sp_XYZ_A6 = Get_SP_UR3();
        // Indico que la aplicion esta abierta (1)
        close_App = 0f;

        // Envio los nuevos valores a ser almacenados en la memoria compartida
        memoryONE_UR3.Current_XYZ_A6 = current_XYZ_A6;
        memoryONE_UR3.Current_Q = current_Q;
        memoryONE_UR3.Current_QP = current_QP;
        memoryONE_UR3.Sp_XYZ_A6 = sp_XYZ_A6;
        memoryONE_UR3.Close_App = close_App;

        Ts = 0.1f; //Temporizador del muestreo

        // Vectores Recuperados de la memoria compartida
        error_XYZ_A6 = memoryONE_UR3.Error_XYZ_A6;
        error_QP = memoryONE_UR3.Error_QP;
        controller_QP = memoryONE_UR3.Controller_QP;

        // Inicializo valores requeridos para la Dinámica
        mass_exter = 0.5f;
        Iexter = MyFunctionsUR3.Matriz_Mul_Escalar(MyFunctionsUR3.eye(3), 0.05f);
        g = 0.0f;
        m = new float[] { 1.98f, 3.4445f, 1.437f, 0.871f, 0.805f, 0.261f };

        _audioSource.Play();
        _audioSource.Pause();
    }

    // Update is called once per frame
    void Update()
    {
        To = To + Time.deltaTime;

        if (To >= Ts)
        {
            if (!DataInfo.dataInfo.singlePractice)
            {
                if (PV.IsMine)
                {
                    calculeteUR3();
                    validateSave();
                }
            }
            else
            {
                calculeteUR3();
                validateSave();
            }
            
            To = 0f;
        }
    }
    private void validateSave()
    {
        if (sp_XYZ_A6[0] != old_x && sp_XYZ_A6[1] != old_y && sp_XYZ_A6[2] != old_z)
        {
            guardar();
        }
        else if (sp_XYZ_A6[0] != old_x && sp_XYZ_A6[1] == old_y && sp_XYZ_A6[2] == old_z)
        {
            guardar();
        }
        else if (sp_XYZ_A6[0] == old_x && sp_XYZ_A6[1] != old_y && sp_XYZ_A6[2] == old_z)
        {
            guardar();
        }
        else if (sp_XYZ_A6[0] == old_x && sp_XYZ_A6[1] == old_y && sp_XYZ_A6[2] != old_z)
        {
            guardar();
        }
        else if (sp_XYZ_A6[0] == old_x && sp_XYZ_A6[1] != old_y && sp_XYZ_A6[2] != old_z)
        {
            guardar();
        }
        else if (sp_XYZ_A6[0] != old_x && sp_XYZ_A6[1] == old_y && sp_XYZ_A6[2] != old_z)
        {
            guardar();
        }
        else if (sp_XYZ_A6[0] != old_x && sp_XYZ_A6[1] != old_y && sp_XYZ_A6[2] == old_z)
        {
            guardar();
        }
    }
    // Función que recupera los valores de posicíones angulares del modelo 3D KUKA
    float[] Get_CurrentQ_UR3()
    {
        float q1 = (_gameObjectA1.transform.localEulerAngles.y * Mathf.PI) / 180.0f;
        float q2 = ((_gameObjectA2.transform.localEulerAngles.z * Mathf.PI)) / 180.0f - Mathf.PI / 2f;
        float q3 = (_gameObjectA3.transform.localEulerAngles.z * Mathf.PI) / 180.0f;
        float q4 = (_gameObjectA4.transform.localEulerAngles.z * Mathf.PI) / 180.0f - Mathf.PI / 2f;
        float q5 = ((_gameObjectA5.transform.localEulerAngles.y * Mathf.PI)) / 180.0f;
        float q6 = (_gameObjectA6.transform.localEulerAngles.z * Mathf.PI) / 180.0f;

        return new float[] { q1, q2, q3, q4, q5, q6 };
    }
    [PunRPC]
    // Función que acualiza los valores de posicíones angulares del modelo 3D KUKA
    void Update_UR3(float[] delta_Q)
    {
        // Rotate Articulation 1
        _gameObjectA1.transform.Rotate(0f, (delta_Q[0] * 180f) / Mathf.PI, 0f);
        // Rotate Articulation 2
        _gameObjectA2.transform.Rotate(0f, 0f, (delta_Q[1] * 180f) / Mathf.PI);
        // Rotate Articulation 3
        _gameObjectA3.transform.Rotate(0f, 0f, (delta_Q[2] * 180f) / Mathf.PI);
        // Rotate Articulation 4
        _gameObjectA4.transform.Rotate(0f, 0f, (delta_Q[3] * 180f) / Mathf.PI);
        // Rotate Articulation 5
        _gameObjectA5.transform.Rotate(0f, (delta_Q[4] * 180f) / Mathf.PI, 0f);
        // Rotate Articulation 6
        _gameObjectA6.transform.Rotate(0f, 0f, (delta_Q[5] * 180f) / Mathf.PI);
    }

    // Función para leer del valor de Set Point especificado por el usuario
    public float[] Get_SP_UR3()
    {
        float spx = _sliderX.value;
        float spy = _sliderY.value;
        float spz = _sliderZ.value;

        return new float[3] { spx, spy, spz };
    }
    [PunRPC]
    private void calculeteUR3()
    {
        // Se recupera el vector de controladores.
        controller_QP = memoryONE_UR3.Controller_QP;
        // Vector de errores para XYZ Recuperados de la momoria compartida
        error_XYZ_A6 = memoryONE_UR3.Error_XYZ_A6;
        // Vector de errores de QP ecuperados de la memoria compartida
        error_QP = memoryONE_UR3.Error_QP;

        float ISE = Mathf.Pow(error_XYZ_A6[0], 2) + Mathf.Pow(error_XYZ_A6[1], 2) + Mathf.Pow(error_XYZ_A6[2], 2);
        if (ISE > 0.005)
        {
            if (!DataInfo.dataInfo.singlePractice)
            {
                PV.RPC("UnSounUR3", RpcTarget.All);
            }
            else
            {
                UnSounUR3();
            }
        }
        else
        {
            if (!DataInfo.dataInfo.singlePractice)
            {
                PV.RPC("SoundUR3", RpcTarget.All);
            }
            else
            {
                SoundUR3();
            }
        }

        /******************************************************
         * PREPARAR LOS DATOS QUE VIENEN DESDE MATLAB PARA INGRESARLOS AL MODELO DINÁMICO DEL ROBOT
         ******************************************************/
        current_Q = Get_CurrentQ_UR3();
        // Obtengo la nueva Qpp a partir de la salida del controlador
        controller_QPP = MyFunctionsUR3.Vector_Div_Escalar(MyFunctionsUR3.Vector_Rest_Vector(controller_QP, current_QP), Ts);
        controller_Q = MyFunctionsUR3.Vector_Sum_Vector(current_Q, MyFunctionsUR3.Vector_Mul_Escalar(controller_QP, Ts));
        /**************************************************
           APLICACIÓN DEL MODELO DINÁMICO DEL UR3
        **************************************************/
        // Dinámica Inversa
        float[] Tau = MyFunctionsUR3.newtoneuler6(current_Q, controller_QP, controller_QPP, g, m, mass_exter, Iexter);
        // Dinámica Directa            
        current_QPP = MyFunctionsUR3.walkerorin6(current_Q, controller_QP, Tau, g, m, mass_exter, Iexter);


        // Actualizazión de la posición angular de cada una de las articulaciones
        current_QP = MyFunctionsUR3.Vector_Mul_Escalar(current_QPP, Ts);
        float[] delta_Q = MyFunctionsUR3.Vector_Mul_Escalar(current_QP, Ts);
        current_Q = MyFunctionsUR3.Vector_Sum_Vector(current_Q, delta_Q);

        for (pos_index = 0; pos_index < 6; pos_index++)
        {
            while (current_Q[pos_index] > (2f * Mathf.PI))
            {
                current_Q[pos_index] = current_Q[pos_index] - (2f * Mathf.PI);
            }
            while (current_Q[pos_index] < (-2f * Mathf.PI))
            {
                current_Q[pos_index] = current_Q[pos_index] + (2f * Mathf.PI);
            }
        }


        /*
        * AQUÍ SE DEBE HACER EL CONTROL DEL ANGULO MÁXIMO PARA CADA ARTÍCULACIÓN
        * SI ES MAYOR LANZAR LOS ERRORES Y ALERTAS
        */
        // Controlamos A1: +360°>Q1>-360°
        if (Mathf.Abs(Mathf.Sin(current_Q[0])) > (2f * Mathf.PI))
        {
            if (!DataInfo.dataInfo.singlePractice)
            {
                PV.RPC("Brun", RpcTarget.All);
            }
            else
            {
                Brun();
            }
        }
        // Controlamos A2: +360°>Q2>-360°
        if (Mathf.Abs(Mathf.Sin(current_Q[1])) > (2f * Mathf.PI))
        {
            if (!DataInfo.dataInfo.singlePractice)
            {
                PV.RPC("Brun", RpcTarget.All);
            }
            else
            {
                Brun();
            }
        }
        // Controlamos A3: +360°>Q3>-360°
        if (Mathf.Abs(Mathf.Sin(current_Q[2])) > (2f * Mathf.PI))
        {
            if (!DataInfo.dataInfo.singlePractice)
            {
                PV.RPC("Brun", RpcTarget.All);
            }
            else
            {
                Brun();
            }
        }
        // Controlamos A4: +360°>Q1>-360°
        if (Mathf.Abs(Mathf.Sin(current_Q[3])) > (2f * Mathf.PI))
        {
            if (!DataInfo.dataInfo.singlePractice)
            {
                PV.RPC("Brun", RpcTarget.All);
            }
            else
            {
                Brun();
            }
        }
        // Controlamos A5: +360°>Q5>-360°
        if (Mathf.Abs(Mathf.Sin(current_Q[4])) > (2f * Mathf.PI))
        {
            if (!DataInfo.dataInfo.singlePractice)
            {
                PV.RPC("Brun", RpcTarget.All);
            }
            else
            {
                Brun();
            }
        }
        // Controlamos A6: +360°>Q6>-360°
        if (Mathf.Abs(Mathf.Sin(current_Q[5])) > (2f * Mathf.PI))
        {
            if (!DataInfo.dataInfo.singlePractice)
            {
                PV.RPC("Brun", RpcTarget.All);
            }
            else
            {
                Brun();
            }
        }


        /*************************************
         * ACTUALIZAMOS LAS POSICIONES ANGULARES DEL ROBOT UR3
         ***************************************/
        // Update the 3D model in Game View
        if (!DataInfo.dataInfo.singlePractice)
        {
            PV.RPC("Update_UR3", RpcTarget.All, delta_Q);
        }
        else
        {
            Update_UR3(delta_Q);
        }
        /********************************************
        // Actualizo los valores para enviar a Matlab
        ********************************************/
        current_Q = Get_CurrentQ_UR3();
        current_XYZ_A6 = MyFunctionsUR3.Get_DKinematic_UR3(current_Q);

        // Update the Effector Position
        sp_XYZ_A6 = Get_SP_UR3();


        //Compartir los nuevos valores calculados
        // Envio los nuevos valores a ser almacenados en la memoria compartida
        memoryONE_UR3.Current_XYZ_A6 = current_XYZ_A6;
        memoryONE_UR3.Current_Q = current_Q;
        memoryONE_UR3.Current_QP = current_QP;
        memoryONE_UR3.Sp_XYZ_A6 = sp_XYZ_A6;
    }
    [PunRPC]
    private void Brun()
    {
        _smokeParticle.Emit(1);
    }
    [PunRPC]
    private void UnSounUR3()
    {
        _audioSource.UnPause();
    }
    [PunRPC]
    private void SoundUR3()
    {
        _audioSource.Pause();
    }
    public void guardar()
    {
        dataEvent = new DataEvent();
        eventPractice = new EventPractice();
        dataEvent.Sp.Add(sp_XYZ_A6[0]);
        dataEvent.Sp.Add(sp_XYZ_A6[1]);
        dataEvent.Sp.Add(sp_XYZ_A6[2]);
        dataEvent.Accontrol.Add(current_QP[0]);
        dataEvent.Accontrol.Add(current_QP[1]);
        dataEvent.Accontrol.Add(current_QP[2]);
        dataEvent.Out.Add(current_Q[0]);
        dataEvent.Out.Add(current_Q[1]);
        dataEvent.Out.Add(current_Q[2]);
        dataEvent.Out.Add(current_Q[3]);
        dataEvent.Out.Add(current_Q[4]);
        dataEvent.Out.Add(current_Q[5]);
        dataEvent.Error.Add(error_XYZ_A6[0]);
        dataEvent.Error.Add(error_XYZ_A6[1]);
        dataEvent.Error.Add(error_XYZ_A6[2]);
        eventPractice._id = DataInfo.dataInfo._practiceJoind._id;
        eventPractice.id_estudiante = DataInfo.dataInfo._authentication._id;
        eventPractice.eventos.Add(dataEvent);
        string json = JsonUtility.ToJson(eventPractice); //convirtindo a tipo json
        StartCoroutine(requestHttp.store(requestHttp.getStorePractice(), json, typeRequest.getEventPractice()));
        old_x = sp_XYZ_A6[0];
        old_y = sp_XYZ_A6[1];
        old_z = sp_XYZ_A6[2];
    }
    [PunRPC]
    private void label()
    {
        _labelValue[0].text = System.Convert.ToString(_sliderX.value);
        _labelValue[1].text = System.Convert.ToString(_sliderY.value);
        _labelValue[2].text = System.Convert.ToString(_sliderZ.value);
    }
}
