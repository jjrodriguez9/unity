﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ManagerUR3 : MonoBehaviour
{
    [SerializeField]
    private GameObject structure;
    // Start is called before the first frame update
    void Awake()
    {
        if (SceneManager.GetActiveScene().buildIndex == MultiplayerSetting.multiplayerSetting.multiplayerScene)
        {
            structure = gameObject.transform.Find("Ambiente").gameObject;
            structure.transform.Find("libros").gameObject.SetActive(false);
            structure.transform.Find("librero").gameObject.SetActive(false);
            structure.transform.Find("arboles").gameObject.SetActive(false);
            structure.transform.Find("luces").gameObject.SetActive(false);
            structure.transform.Find("aulas+libres").gameObject.SetActive(false);
            structure.transform.Find("mesa central").gameObject.SetActive(false);
        }
    }
}
