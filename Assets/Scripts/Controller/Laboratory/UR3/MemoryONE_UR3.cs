﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;
using Photon.Pun;

public class MemoryONE_UR3 : MonoBehaviourPun
{
    private readonly int totalMem = 50;
    public float[] Current_XYZ_A6 { get => _current_XYZ_A6; set => _current_XYZ_A6 = value; }
    public float[] Sp_XYZ_A6 { get => _sp_XYZ_A6; set => _sp_XYZ_A6 = value; }
    public float[] Error_QP { get => _error_QP; set => _error_QP = value; }
    public float[] Error_XYZ_A6 { get => _error_XYZ_A6; set => _error_XYZ_A6 = value; }
    public float[] Current_Q { get => _current_Q; set => _current_Q = value; }
    public float[] Current_QP { get => _current_QP; set => _current_QP = value; }
    public float[] Controller_QP { get => _controller_QP; set => _controller_QP = value; }
    public float Close_App { get => _close_App; set => _close_App = value; }
    private Memoryshare memoryshare;
    private float _close_App;
    private float[] _controller_QP;
    private float[] _current_QP;
    private float[] _current_Q;
    private float[] _error_XYZ_A6;
    private float[] _error_QP;
    private float[] _sp_XYZ_A6;
    private float[] _current_XYZ_A6;
    private PhotonView PV;
    private void Awake()
    {
        _current_XYZ_A6 = new float[3] { 0f, 0f, 0f };
        _sp_XYZ_A6 = new float[3] { 0f, 0f, 0f };
        _current_Q = new float[6] { 0f, 0f, 0f, 0f, 0f, 0f };
        _current_QP = new float[6] { 0f, 0f, 0f, 0f, 0f, 0f };
        _error_XYZ_A6 = new float[3] { 0f, 0f, 0f };
        _error_QP = new float[6] { 0f, 0f, 0f, 0f, 0f, 0f };
        _controller_QP = new float[6] { 0f, 0f, 0f, 0f, 0f, 0f };
        _close_App = 0f;
    }
    // Use this for initialization
    void Start()
    {
        if (!DataInfo.dataInfo.singlePractice)
        {
            PV = GetComponent<PhotonView>();
            Inicializate();
        }
        else
        {
            Inicializate();
        }
    }
    private void Inicializate()
    {
        memoryshare = new Memoryshare();
        memoryshare.createMemoriShare("MemoriaONE",50,2);
        memoryshare.openMemoryShare("MemoriaONE",2);
        for (int i = 0; i < totalMem; i++)
        {
            memoryshare.Setfloat("MemoriaONE", 0f, i);
        }

    }
    // Update is called once per frame
    void Update()
    {
        if (!DataInfo.dataInfo.singlePractice)
        {
            if (PV.IsMine)
            {
                dataUpdate();
            }
        }
        else
        {
            dataUpdate();
        }
    }
    private void dataUpdate()
    {
        int pos_mem;
        int i = 0;
        int le = _error_XYZ_A6.Length;
        int lc = _controller_QP.Length;

        pos_mem = 0;
        foreach (float val in _current_XYZ_A6)
        {
            memoryshare.Setfloat("MemoriaONE", val, pos_mem);
            pos_mem++;
        }

        pos_mem = 3;
        foreach (float val in _current_Q)
        {
            memoryshare.Setfloat("MemoriaONE", val, pos_mem);
            pos_mem++;
        }

        pos_mem = 9;
        foreach (float val in _current_QP)
        {
            memoryshare.Setfloat("MemoriaONE", val, pos_mem);
            pos_mem++;
        }

        pos_mem = 16;
        foreach (float val in _sp_XYZ_A6)
        {
            memoryshare.Setfloat("MemoriaONE", val, pos_mem);
            pos_mem++;
        }

        pos_mem = 19;
        memoryshare.Setfloat("MemoriaONE", _close_App, pos_mem);



        pos_mem = 20;
        for (i = 0; i < le; i++)
        {
            _error_XYZ_A6[i] = memoryshare.Getfloat("MemoriaONE", pos_mem);
            pos_mem++;
        }

        pos_mem = 23;
        for (i = 0; i < lc; i++)
        {
            _controller_QP[i] = memoryshare.Getfloat("MemoriaONE", pos_mem);
            pos_mem++;
        }
    }
    // Update is called once per frame
    void OnApplicationQuit()
    {
        _close_App = 1f;
        memoryshare.Setfloat("MemoriaONE", _close_App, 19);
    }


    void OnDestroy()
    {
        _close_App = 1f;
        memoryshare.Setfloat("MemoriaONE", _close_App, 19);
    }
}