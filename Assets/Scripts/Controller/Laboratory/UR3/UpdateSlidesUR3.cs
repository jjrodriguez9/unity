﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpdateSlidesUR3 : MonoBehaviour
{
    [SerializeField]
    private Slider _sliderX;
    [SerializeField]
    private Slider _sliderY;
    [SerializeField]
    private Slider _sliderZ;
    [SerializeField]
    private Text _textX;
    [SerializeField]
    private Text _textY;
    [SerializeField]
    private Text _textZ;




    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        _textX.text = _sliderX.value.ToString();
        _textY.text = _sliderY.value.ToString();
        _textZ.text = _sliderZ.value.ToString();
    }
}
