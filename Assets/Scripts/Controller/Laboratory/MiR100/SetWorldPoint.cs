﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetWorldPoint : MonoBehaviour
{

    public GameObject _spherePoint;

    public Vector3[] PointsTrack;

    public SetWorldPoint instance;

    private int index = 0;

    // Start is called before the first frame update
    void Start()
    {
        Inicializate();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonUp(1))  // if right mouse button is released
        {
            Vector3 mouse = Input.mousePosition;
            Ray castPoint = Camera.main.ScreenPointToRay(mouse);
            RaycastHit hit;
            if (Physics.Raycast(castPoint, out hit, Mathf.Infinity))
            {
                Instantiate(_spherePoint, hit.point, Quaternion.identity);
                // Save the 3D Point
                PointsTrack[index] = hit.point;
                index++;
                if (index >= 100)
                {
                    index = 0;
                }
            }
        }
    }

    private void Inicializate()
    {
        PointsTrack = new Vector3[100];
        index = 0;
        instance = this;
    }
}
