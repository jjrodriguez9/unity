﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;   

public class PanelControlMiR100 : MonoBehaviourPun
{
    [SerializeField]
    private Slider _sliderVelocity;
    [SerializeField]
    private Toggle _toggleMode;
    private PhotonView PV;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (Input.GetButton("active_x"))
        {
            float value = Input.GetAxis("slider_move");
            if (!DataInfo.dataInfo.singlePractice)
            {
                PV.RPC("valueX", RpcTarget.All, value);
            }
            else
            {
                valueX(value);
            }
        }
        // Change Setpoint Y
        if (Input.GetButton("active_y"))
        {
            float value = Input.GetAxis("slider_move");
            if (!DataInfo.dataInfo.singlePractice)
            {
                PV.RPC("valueY", RpcTarget.All, value);
            }
            else
            {
                valueY(value);
            }
        }
    }
    [PunRPC]
    private void valueX(float val)
    {
        if (val < 0)
        {
            _sliderVelocity.value = _sliderVelocity.value - 0.01f;
        }
        else if (val > 0)
        {
            _sliderVelocity.value = _sliderVelocity.value + 0.01f;
        }
    }
    [PunRPC]
    private void valueY(float val)
    {
        _toggleMode.isOn = !_toggleMode.isOn;
    }
}
