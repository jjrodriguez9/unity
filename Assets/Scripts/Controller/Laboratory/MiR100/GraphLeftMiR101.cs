﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ChartAndGraph;

public class GraphLeftMiR101 : MonoBehaviour
{    
    private GraphChart _graphRight;
    private float xTime;
    private float time;
    private MemoryONE_MiR100 memoryONE_MiR100;

    // Start is called before the first frame update
    void Start()
    {
        Inicializate();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        time += Time.deltaTime;

        if (time > 1f)
        {
            xTime += time;
            // Update Right Wheel Graph            
            _graphRight.DataSource.AddPointToCategoryRealtime("Player 1", xTime, memoryONE_MiR100.SP_RL[1], 1f); // each time we call AddPointToCategory 
            _graphRight.DataSource.AddPointToCategoryRealtime("Player 2", xTime, memoryONE_MiR100.Current_RL[1], 1f); // each time we call AddPointToCategory
            time = 0f;
        }
    }

    private void Inicializate()
    {
        memoryONE_MiR100 = GameObject.Find("MemoryHandle").GetComponent<MemoryONE_MiR100>();
        _graphRight = GetComponent<GraphChart>();
        if (_graphRight != null) // the ChartGraph info is obtained via the inspector
        {
            xTime = 0f;
            // Clear the Right Wheel Chart
            _graphRight.DataSource.ClearCategory("Player 1"); // clear the "SP Right Wheel" category. this category is defined using the GraphChart inspector
            _graphRight.DataSource.ClearCategory("Player 2"); // clear the "Current Right Wheel" category. this category is defined using the GraphChart inspector
        }

        time = 0f;
        xTime = 0f;
    }
}
