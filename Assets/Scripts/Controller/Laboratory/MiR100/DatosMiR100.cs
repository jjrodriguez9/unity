﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ChartAndGraph;
using UnityEngine.UI;

public class DatosMiR100 : MonoBehaviour
{
    
    private Vector3 torsion;
    
    [SerializeField]
    private float Ts = 0.02f;
    private float To;
    [SerializeField]
    private GameObject _spherePoint;
    [SerializeField]
    private Toggle _kinematicMode;
    [SerializeField]
    private Slider _linealVelocity;
    [SerializeField]
    private ParticleSystem _smokeParticle;
    [SerializeField]
    private float _maxVelWheels = 5f;
    [SerializeField]
    private AudioSource _audioSource;

    private Vector2[] PointsTrack;    
    private int points = 0;        

    private float current_x;
    private float current_z;
    private float current_q;
    private float current_qpr;
    private float current_qpl;
    private float current_qppr;
    private float current_qppl;
    private float xp;
    private float zp;
    private float qp;
    private float[] SP_wrl;

    private float[] controller_RL;
    private float[] error_RL;    

    // Parametros del Modelo Dinámico de la MiR100
    // Motores DC
    private float Kt = 0.8f; // Motor Torque constant, Kt(N* m/A)
    private float Ra = 12f;  // Resistencia del estator del motor(Ohmios)
    private float Kf = 1.2f; // Constante de Rozamiento Viscoso(N* m / rad/s)
    private float Kb = 5.6f; // Constante de tensión electromagnética(V / rad /s)
    private float r = 0.1f;  // Relación del Par Motor 10:1
    private float Io = 0.0473f; // Momento de Incercia de la armadura del motor(N* m*s^2/rad)
    // Controlador PD incluido en la Plataforma
    private float Kp = 5.1f;  // Ganancia proporcional
    private float Kd = 0.45f; // Ganancia diferencial
    // Plataforma
    private float m = 40.0f;  // Masa del MiR100 en Kg
    private float R = 0.22f;  // Radio de las ruedas en m
    private float L = 0.6f;   // Distancia axial de ruedas del robot en m
    private float Ic = 0.62f;  // Momento de inercia de la misma plataforma(N* m*s^2/rad)

    private MemoryONE_MiR100 memoryONE_MiR100;

    // Start is called before the first frame update
    void Start()
    {
        Inicializate();
    }

    // Update is called once per frame
    void Update()
    {
        To = To + Time.deltaTime;

        if (To >= Ts)
        {
            /*
             * REcibo los valores de control desde Matlab
             */
            controller_RL = memoryONE_MiR100.Controller_RL;
            error_RL = memoryONE_MiR100.Error_RL;

            float qpr_ref = controller_RL[0];
            float qpl_ref = controller_RL[1];

            /*
             * Aplico el modelo dinámico de la plataforma MiR100: Enfoque Euler-Lagrange
             */
             // Conformación de los terminos de la matriz M
            float A = ((m * Mathf.Pow(R, 2)) / 4f) + ((Ic * Mathf.Pow(R, 2)) / Mathf.Pow(L, 2)) + Io;
            float B = ((m * Mathf.Pow(R, 2)) / 4f) - ((Ic * Mathf.Pow(R, 2)) / Mathf.Pow(L, 2));
            float M11 = ((A * Ra) + (Kd * Kt)) / (Kp * Kt);
            float M12 = ((B * Ra) + (Kd * Kt)) / (Kp * Kt);
            float M21 = M12;
            float M22 = M11;            
            float C11 = (Kf * Ra) / (Kp * Kt) + Kb / (r * Kp) + 1;
            float C22 = C11;
            // Conformación de las matrics del Modelo Dinámico inverso
            float D11 = M11 / (M11 * M22 - M12 * M21);
            float D12 = M21 / (M11 * M22 - M12 * M21);
            float D21 = M12 / (M11 * M22 - M12 * M21);
            float D22 = M22 / (M11 * M22 - M12 * M21);

            float E11 = (C11 * M11) / (M11 * M22 - M12 * M21);
            float E12 = (C11 * M21) / (M11 * M22 - M12 * M21);
            float E21 = (C22 * M12) / (M11 * M22 - M12 * M21);
            float E22 = (C22 * M22) / (M11 * M22 - M12 * M21);

            float new_qppr = (D11 * qpr_ref + D12 * qpl_ref) - (E11 * current_qpr + E12 * current_qpl);
            float new_qppl = (D21 * qpr_ref + D22 * qpl_ref) - (E21 * current_qpr + E22 * current_qpl);

            // Obtengo la nueva velocidad angular de las ruedas a partir de la aceleración angular
            current_qpr = current_qpr + (new_qppr - current_qppr) * Ts;
            current_qpl = current_qpl + (new_qppl - current_qppl) * Ts;
            // Guardo las nuevas aceleraciones angulares de las ruedas
            current_qppr = new_qppr;
            current_qppl = new_qppl;


            // Check for Limit of Wheel Velocity
            if ((Mathf.Abs(current_qpr) >= _maxVelWheels) || (Mathf.Abs(current_qpl) >= _maxVelWheels))
            {
                _smokeParticle.Emit(1);
            }
            // Check for MiR100 Movement for Audio
            if ((Mathf.Abs(current_qpr) == 0f) && (Mathf.Abs(current_qpl) >= 0f))
            {
                _audioSource.Pause();
            }
            else
            {
                _audioSource.UnPause();
            }



            /*
                * Aplico el modelo cinemático directo de la Plataforma MiR100
                */

            // Calcular las velocidades de desplazamiento lineal en X y Y, y velocidad angular de rotación
            float arg_xp = (R / 2f) * (current_qpr * Mathf.Cos(current_q) + current_qpl * Mathf.Cos(current_q));
            xp = (float)(System.Math.Round(arg_xp, 2));
            float arg_zp = (R / 2f) * (current_qpr * Mathf.Sin(current_q) + current_qpl * Mathf.Sin(current_q));
            zp = (float)(System.Math.Round(arg_zp, 2));
            float arg_qp = (R / L) * (current_qpr - current_qpl);
            qp = (float)(System.Math.Round(arg_qp, 2));

            // Calcular la nueva posición X y Z, y nueva orientación.
            current_x = current_x + (xp * Ts);
            current_z = current_z + (zp * Ts);
            current_q = current_q + (qp * Ts);

            

            // Actualizar la nueva posición de la platafoma en el ambiente
            transform.position = new Vector3(current_x, transform.position.y, current_z);
            // Actualizar la rotación de la plataforma en el ambiente
            torsion = Vector3.up * ((-current_q * 180f) / Mathf.PI);
            transform.localEulerAngles = torsion;
            

            /*
             * ENVIO LOS DATOS A MATLAB
             */

            // Captura las coordenadas actuales de la plataforma
            current_x = transform.position.x;
            current_z = transform.position.z;
            current_q = -(transform.eulerAngles.y * Mathf.PI) / 180f;  // rads            
            if (current_q > Mathf.PI)
                current_q = current_q - (2 * Mathf.PI);
            if (current_q < -Mathf.PI)
                current_q = current_q + (2 * Mathf.PI);

            // Aplico el modelo de Cinemática Inversa de la Plataforma para obtener el nuevo Set Point de la Velocidad Angular de las llantas.
            float[] inv_kinemaic_input = Get_Kinematic_Error(PointsTrack[0].x, PointsTrack[0].y, current_x, current_z, current_q);

            if (_kinematicMode.isOn == false)
            {
                if (Mathf.Abs(inv_kinemaic_input[2]) > 0.02)
                    SP_wrl = Inverse_Kinematics(0f, 0f, inv_kinemaic_input[2], current_q, R, L);
                else
                    SP_wrl = Inverse_Kinematics(inv_kinemaic_input[0], inv_kinemaic_input[1], inv_kinemaic_input[2], current_q, R, L);
            }
            else
            {
                SP_wrl = Inverse_Kinematics(inv_kinemaic_input[0], inv_kinemaic_input[1], inv_kinemaic_input[2], current_q, R, L);
            }


            memoryONE_MiR100.Current_RL = new float[2] {current_qpr, current_qpl};

            memoryONE_MiR100.SP_RL = SP_wrl;

            To = 0f;            
        }





        if (Input.GetMouseButtonUp(1))  // if right mouse button is released
        {
            Vector3 mouse = Input.mousePosition;
            Ray castPoint = Camera.main.ScreenPointToRay(mouse);
            RaycastHit hit;
            if (Physics.Raycast(castPoint, out hit, Mathf.Infinity))
            {
                if (points < 100)
                {
                    Instantiate(_spherePoint, hit.point, Quaternion.identity);
                    // Save the 2D Point (x , y)
                    PointsTrack[points] = new Vector2(hit.point.x, hit.point.z);
                    points++;
                }
            }
        }

    }

    private void Inicializate()
    {
        To = 0f;
        memoryONE_MiR100 = GameObject.Find("MemoryHandle").GetComponent<MemoryONE_MiR100>();
        current_qpr = 0f;
        current_qpl = 0f;
        current_qppr = 0f;
        current_qppl = 0f;

        current_x = transform.position.x;
        current_z = transform.position.z;
        current_q = -(transform.eulerAngles.y * Mathf.PI) / 180f;  // rads
        if (current_q > Mathf.PI)
            current_q = current_q - (2 * Mathf.PI);
        if (current_q < -Mathf.PI)
            current_q = current_q + (2 * Mathf.PI);

        //Debug.Log("Initial X: " + current_x + " Initial Z: " + current_z + " Initial Q: " + current_q);

        PointsTrack = new Vector2[100];
        PointsTrack[0] = new Vector2(current_x, current_z);
        points = 0;

        SP_wrl = new float[2] { 0f, 0f };
        controller_RL = new float[2] { 0f, 0f };
        error_RL = new float[2] { 0f, 0f };

        _audioSource.Play();
        _audioSource.Pause();
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "TrackPoint")
        {
            Destroy(other.gameObject, .1f);
            if (points > 1)
            {
                int i = 0;
                while ((i + 1) <= (points))
                {
                    PointsTrack[i] = PointsTrack[i + 1];
                    i++;
                }
                points--;
            }
            else
            {
                PointsTrack[0] = new Vector2(current_x, current_z);
            }
        }
    }


    private float[] Get_Kinematic_Error(float spx, float spy, float curx, float cury, float curq)
    {
        // Calcular los diferenciales de X, Y y Q
        float dx = (spx - curx);
        float dy = (spy - cury);
        float arg_dq = 0f;
        float c = 0f;
        // Si el movimiento es sobre el Eje Y positivo
        if ((dx == 0) && (dy > 0))
            arg_dq = Mathf.PI / 2f;
        // Si el movimiento es sobre el Eje Y negativo
        if ((dx == 0) && (dy < 0))
            arg_dq = -Mathf.PI / 2f;
        // Si el movimiento es sobre el Eje X positivo
        if ((dx > 0) && (dy == 0))
            arg_dq = 0f;
        // Si el movimiento es sobre el Eje X negativo
        if ((dx < 0) && (dy == 0))
            arg_dq = Mathf.PI / 2f;
        // Si la plataforma no debe rotar
        if ((dx == 0) && (dy == 0))
            arg_dq = curq;
        // Si el movimiento es hacia en cuadrante I
        if ((dx > 0) && (dy > 0))
        {
            c = Mathf.Sqrt(Mathf.Pow(dx, 2) + Mathf.Pow(dy, 2));
            arg_dq = Mathf.Sin(dy / c);
        }
        // Si el movimiento es hacia en cuadrante II
        if ((dx < 0) && (dy > 0))
        {
            c = Mathf.Sqrt(Mathf.Pow(dx, 2) + Mathf.Pow(dy, 2));
            arg_dq = Mathf.PI - Mathf.Sin(dy / c);
        }
        // Si el movimiento es hacia en cuadrante III
        if ((dx < 0) && (dy < 0))
        {
            c = Mathf.Sqrt(Mathf.Pow(dx, 2) + Mathf.Pow(dy, 2));
            arg_dq = -Mathf.PI - Mathf.Sin(dy / c);
        }
        // Si el movimiento es hacia en cuadrante IV
        if ((dx > 0) && (dy < 0))
        {
            c = Mathf.Sqrt(Mathf.Pow(dx, 2) + Mathf.Pow(dy, 2));
            arg_dq = Mathf.Sin(dy / c);
        }

        float Vel = _linealVelocity.value;        

        dx = dx * Vel;
        dy = dy * Vel;
        float dq = (arg_dq - curq) * Vel;

        return new float[3] { dx, dy, dq};

    }

    private float[] Inverse_Kinematics(float xp, float yp, float qp, float q, float ratio, float length)
    {
        float wr = xp * (Mathf.Cos(q) / ratio) + yp * (Mathf.Sin(q) / ratio) + qp * (ratio / length);
        float wl = xp * (Mathf.Cos(q) / ratio) + yp * (Mathf.Sin(q) / ratio) - qp * (ratio / length);

        return new float[2] { wr, wl };
    }
}
