﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;


public class MemoryONE_MiR100 : MonoBehaviour
{
    private readonly int totalMem = 50;
    public float[] Current_RL { get => _current_RL; set => _current_RL = value; }
    public float[] SP_RL { get => _sp_RL; set => _sp_RL = value; }
    public float[] Error_RL { get => _error_RL; set => _error_RL = value; }
    public float[] Controller_RL { get => _controller_RL; set => _controller_RL = value; }
    public float Close_App { get => _close_App; set => _close_App = value; }

    private float _close_App;
    private float[] _controller_RL;
    private float[] _error_RL;
    private float[] _sp_RL;
    private float[] _current_RL;
    private Memoryshare memoryshare;

    // Use this for initialization
    void Start()
    {
        Inicializate();
    }

    // Update is called once per frame
    void Update()
    {
        Calculate();
    }

    private void Inicializate()
    {
        _current_RL = new float[2] { 0f, 0f };
        _sp_RL = new float[2] { 0f, 0f };
        _error_RL = new float[2] { 0f, 0f };
        _controller_RL = new float[2] { 0f, 0f };
        _close_App = 0f;
        memoryshare = new Memoryshare();
        memoryshare.freeMemoriShare();
        memoryshare.createMemoriShare("MemoriaONE", 50, 2);
        memoryshare.openMemoryShare("MemoriaONE", 2);

        for (int i = 0; i < totalMem; i++)
        {
            memoryshare.Setfloat("MemoriaONE", 0f, i);
        }
        //Debug.Log("Memory One Created.");
    }

    private void Calculate()
    {
        int pos_mem;
        int i = 0;
        int le = _error_RL.Length;
        int lc = _controller_RL.Length;

        pos_mem = 0;
        foreach (float val in _current_RL)
        {
            memoryshare.Setfloat("MemoriaONE", val, pos_mem);
            pos_mem++;
        }

        pos_mem = 5;
        foreach (float val in _sp_RL)
        {
            memoryshare.Setfloat("MemoriaONE", val, pos_mem);
            pos_mem++;
        }

        pos_mem = 7;
        memoryshare.Setfloat("MemoriaONE", _close_App, pos_mem);



        pos_mem = 8;
        for (i = 0; i < le; i++)
        {
            _error_RL[i] = memoryshare.Getfloat("MemoriaONE", pos_mem);
            pos_mem++;
        }

        pos_mem = 13;
        for (i = 0; i < lc; i++)
        {
            _controller_RL[i] = memoryshare.Getfloat("MemoriaONE", pos_mem);
            pos_mem++;
        }
    }
    // Update is called once per frame
    void OnApplicationQuit()
    {
        _close_App = 1f;
        memoryshare.Setfloat("MemoriaONE", _close_App, 7);
    }


    void OnDestroy()
    {
        _close_App = 1f;
        memoryshare.Setfloat("MemoriaONE", _close_App, 7);
    }
}