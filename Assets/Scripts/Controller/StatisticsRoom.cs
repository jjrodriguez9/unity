﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
using static TMPro.TMP_Dropdown;
using System.Collections.Generic;
using Photon.Pun;
using Photon.Realtime;
using System.Linq;
using UnityEngine.SceneManagement;
using System.Collections;

public class StatisticsRoom : MonoBehaviourPunCallbacks
{
    [SerializeField]
    private GameObject[] character; //objeto donde va estar el avatar de cada entidad
    [SerializeField]
    private GameObject Fondo; //objeto de fondo para visualizar
    [SerializeField]
    private TMP_Dropdown selectLaboratory; //select donde se carga los laboratorios
    private TypeRequest typeRequest; //Instanciando la clase para realizar el tipo de peticion que se requiere
    private RequestHttp requestHttp; //Instanciando la clase para realizar los metodos de peticion y la url a utilizar
    private OptionDataList optionDataList; //instancia para cargar los datos en select
    private int select; // selecion de laboratorio
    private System.DateTime fecha; //fecha 
    private bool list = false; //estado para validar la lista 
    [SerializeField]
    private PracticeListing listPractice; //Instancia la clase para realizar el cargado de los datos de la practica creadas y de la interfaz
    [SerializeField]
    private Transform content; //el contenido que se crea cada que hay una partida creada
    [SerializeField]
    private List<PracticeListing> practiceListing = new List<PracticeListing>(); //listado de las paratidas creadas como objetos
    private bool status, validate; //estatus que permite verificar para la union de la sala
    [SerializeField]
    private TMP_Text complete_name;
    [SerializeField]
    private GameObject panel, errors; //activacion del panel asignado
    [SerializeField]
    private TMP_Text text; //asignacion de un texto 
    [SerializeField]
    private TMP_Text error_selection;
    [SerializeField]
    private GameObject err_selection;
    [SerializeField]
    private GameObject roomController;
    // Start is called before the first frame update
    void Start()
    {
        status = false; //inicializando el estado
        requestHttp = new RequestHttp(); //realizacion de la instancia para utilizar las peticiones hacia el api;
        typeRequest = new TypeRequest(); //realizacion de la instancia para obtener el rol o el tipo de requeste que se requiere hacer al api
        optionDataList = new OptionDataList(); //opcion para cargar los laboratorios en el select 
        DataInfo.dataInfo.singlePractice = false;
        complete_name.text = DataInfo.dataInfo._authentication.nombre + " " + DataInfo.dataInfo._authentication.apellido;
        if (DataInfo.dataInfo._authentication.tipo_us.Equals(typeRequest.getStudent())) //validacion si es de tipo estudiante para cargar los objetos
        {
            Instantiate(roomController, Vector3.zero, Quaternion.identity);
            //MultiplayerSetting.multiplayerSetting.delayStart = true; //inicializando en la configuracion de photon para que empiece a unicer por medio del metodo player join
            select = 0; //selecion de la opcion
            Fondo.SetActive(true); //activando el fondo
            StartCoroutine(requestHttp.show(requestHttp.getListPractice(), typeRequest.getPractice())); //realizando la peticion de las partidas creadas la primera vez 
            Instantiate(character[1], character[1].transform.position, character[1].transform.rotation); //instanciando el avatar para esta dentro de la escena
            StartCoroutine(requestHttp.show(requestHttp.getLaboratory(), typeRequest.getLaboratory())); //realiza do la peticion para la obtencion de los laboratorios
            OptionData option = new OptionData(); //realizacion de la instancia para cargar los datos en select
            option.text = "Seleccione"; //colocando la opcion de seleccion en el select
            optionDataList.options.Add(option); //añadiendo a la lista las opciones
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        if (DataInfo.dataInfo._authentication.tipo_us.Equals(typeRequest.getStudent())) //verificando que sea de tipo de estudiante
        {
            if (!list) //validando si la list es falso para realizar la carga de datos una sola vez
            {
                if (DataInfo.dataInfo._laboratoryList.laboratory.Count != 0) //verificando que el tamaño de arreglo del laboratorio sea diferente de cero para cargar en el select
                {
                    foreach (Laboratory laboratory in DataInfo.dataInfo._laboratoryList.laboratory) //realizando un foreach para realizar la carga en el select 
                    {
                        OptionData option = new OptionData();//realizando el objeto para cargar en el select 
                        option.text = laboratory.name; //enviando el nombre que aparecera en el select
                        optionDataList.options.Add(option); //cargando las opciones en el select
                    }
                    selectLaboratory.AddOptions(optionDataList.options); //añadiendo las opciones dentro de la interfaz
                    list = true; //cambiando el estado para no realizar la actualizacion otra vez
                }
            }
            if (list)//verificando que el metodo sea verdadero una ves cargado para cojer los index del select
            {
                int index; //valor donde se almacenara el index del select 
                index = selectLaboratory.value; //tomando el index del select seleccionado
                if (index != 0)
                {
                    err_selection.SetActive(false);
                    error_selection.text = "";
                    selectLaboratory.GetComponent<Image>().sprite = Resources.Load<Sprite>("Sprites/UI/Texto");
                }
            }
            if (select == 0) //verificando que sea la seleccion de tipo 0
            {
                ListPractice(DataInfo.dataInfo._practiceList); // realizando la actualizacion de la pantalla para las partidas creadas o eliminadas dentro de la escena
                if (!status) //validando el status para realizar sea falso para realizar un peticion
                {
                    StartCoroutine(resfh(2));
                }
            }
            if (validate)
            {
                text.text = "Enviando"; // cambiando el texto 
                StartCoroutine(wait(3));
            }
        }
    }
    /**
     * metodo para la actualizacion del objeto en la pantalla segun los datos de la peticion de las partidas creadas
     */
    private void ListPractice(PracticeList practiceList) 
    {
        if (practiceListing.Count!= practiceList.practice.Count)//validando que el arreglo sea diferente al tamaño de objeto de la pantalla mostrados
        {
            foreach (Practice item in practiceList.practice) //realizando el movimiento de los datos para hacer la carga de los objetos en la pantalla
            {
                if (!practiceListing.Any(x => x._practice.tema == item.tema)){ //verificando si el objeto es ingual al tema para realizar la carga en la pantalla
                    PracticeListing listing = Instantiate(listPractice, content);//creando el objeto de la pantalla 
                    if (listing != null) //verificando que list sea diferente de null
                    {
                        listing.Practice_Listing(item); //cargando el objeto en la practica 
                        practiceListing.Add(listing); //cargando el objeto en la clase del objeto creado
                    }
                }
            }
        }
        if(practiceListing.Count> practiceList.practice.Count) //verificando que los objetos creados de la pantalla si es mayor que de los datos de la peticion para realizar la eliminacion de la interfaz
        {
            for (int i = 0; i < practiceListing.Count; i++) //recorriendo el objeto para realizar la eliminacion
            {
                if(!practiceList.practice.Any(x => x.tema == practiceListing[i]._practice.tema))//verificacion de que si alguno de los objeto no exista para realizar la eliminacion
                {
                    Destroy(practiceListing[i].gameObject); //destruyendo el objeto creado en pantalla
                    practiceListing.RemoveAt(i); //removiendo de la lista el objeto que no se encuentra ya en las peticion.
                }
            }
        }
    }
    /**
     * metodo para realizar el cambio de tiempo maximo y la peticion por medio de un switch
     */
    IEnumerator resfh(int seconds)
    {
        status = true; //cambio de estado para que entre al metodo else 
        yield return new WaitForSeconds(seconds);
        status = false; //cambio de estado para que entre al metodo else 
        StartCoroutine(requestHttp.show(requestHttp.getListPractice(), typeRequest.getPractice()));//realizando la peticion para las practicas creadas
    }

    IEnumerator wait(int seconds)
    {
        validate = false; //cambio de estado para que entre al metodo else 
        yield return new WaitForSeconds(seconds);
        if (string.IsNullOrEmpty(DataInfo.dataInfo._error.error_practica) && DataInfo.dataInfo._error.errors_array.Count == 0 && DataInfo.dataInfo.status == 200) //verifica que el status sea 200 que es exitoso
        {
            SceneManager.LoadScene(4); //cambiando a otra escena luego de conectarce
        }
        else if (DataInfo.dataInfo.status == 500)
        {
            errors.SetActive(true);
            Debug.Log(DataInfo.dataInfo._error.error_practica);
            text.text = "!Upss Ocurrio un error intente después de unos segundos"; // cambiando el texto 
        }
        else if (!string.IsNullOrEmpty(DataInfo.dataInfo._error.error_practica) && DataInfo.dataInfo.status == 200)
        {
            errors.SetActive(true);
            Debug.Log(DataInfo.dataInfo._error.error_practica);
            text.text = DataInfo.dataInfo._error.error_practica.ToString(); // cambiando el texto 
        }
        else
        {
            errors.SetActive(true);
            Debug.Log(DataInfo.dataInfo._error.error_practica);
            text.text = "!Upss Ocurrio un error intente despues de unos segundos"; // cambiando el texto
        }
    }

    public void CrearPractice()
    {
        int index = selectLaboratory.value;//toamando el valor del index del select
        if (index == 0)
        {
            err_selection.SetActive(true);
            error_selection.text = "Seleccione un laboratorio";
            selectLaboratory.GetComponent<Image>().sprite = Resources.Load<Sprite>("Sprites/UI/Texto_error");
        }
        else
        {
            panel.SetActive(true); //activando el panel de carga
            Practice practice = new Practice(); //realizando la inicializacion de la clase
            Person person = new Person(); //realizando la inicializacion de la clase
            person._id = DataInfo.dataInfo._authentication._id; //cargando el id del usuario logeado
            person.nombre = DataInfo.dataInfo._authentication.nombre; //cargando el nombre del usuario logeado
            person.apellido = DataInfo.dataInfo._authentication.apellido; //cargando el apellido del usuario logeado
            practice.tema = "Practicando"; //tomando el tema ingreasado en el input
            practice.id_lab = DataInfo.dataInfo._laboratoryList.laboratory[index - 1]._id; //toamdnod el id del laboratorio en el index del select
            practice.status = false; // dando el estado true para que aparesca en la busquedas de las practicas creadas
            practice.docente.Add(person); //añadiendo el objeto del docente
            string json = JsonUtility.ToJson(practice); //creando el json
            DataInfo.dataInfo.singlePractice = true;
            StartCoroutine(requestHttp.store(requestHttp.getPractice(), json, typeRequest.getPractice()));//enviando la informacion por medio de la peticion para la creacion de la sala
            validate = true;
        }
    }

    public void ActiveSubmit()
    {
        errors.SetActive(false);
        panel.SetActive(false); //activando el panel de carga
        DataInfo.dataInfo._error = null;
        DataInfo.dataInfo.status = 0;
    }
}
