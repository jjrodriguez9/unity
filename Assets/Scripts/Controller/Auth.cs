﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Photon.Pun;
using UnityEngine.SceneManagement;
using System.Collections;
using Photon.Realtime;

public class Auth : MonoBehaviourPunCallbacks, IConnectionCallbacks
{
    private RequestHttp requestHttp; //Intanciando la clase para realizar los metodos de peticion y la url a utilizar
    private TypeRequest typeRequest; //Intanciando la clase para realizar el tipo de peticion que se requiere
    [SerializeField]
    private TMP_InputField user; //input para la toma de informacion del usuario 
    [SerializeField]
    private TMP_InputField pwd; //input para la toma de informacion del password
    [SerializeField]
    private TMP_Text error_user, error_pwd;
    [SerializeField]
    private GameObject err_user, err_pwd;
    [SerializeField]
    private GameObject panel,errors; //activacion del panel asignado
    [SerializeField]
    private TMP_Text text; //asignacion de un texto 
    private bool correct = true, validate = false, connect=false;
    private int intent = 0;
    public override void OnConnectedToMaster() //Callback function for when the first connection is established successfully.
    {
        //Debug.Log("Conectando al servidor " + PhotonNetwork.CloudRegion + " server!");
        PhotonNetwork.AutomaticallySyncScene = true; //Makes it so whatever scene the master client has loaded is the scene all other clients will load
        text.text = "Conectando"; //cambiando el texto si se conecto al servidor de photon
        connect = true;
    }

    // Start is called before the first frame update
    void Start()
    {
        requestHttp = new RequestHttp(); //realizacion de la instancia para utilizar las peticiones hacia el api;
        typeRequest = new TypeRequest(); //realizacion de la instancia para obtener el rol o el tipo de requeste que se requiere hacer al api
        text.text = "Conectanto con el servidor"; //colocando el texto de conexcion hacia el servidor de photon
        intent = 0;
    }
    public virtual void OnFailedToConnectToPhoton(DisconnectCause cause)
    {
        Debug.Log("fallo");
        Debug.Log(cause);
    }

    // Update is called once per frame
    void Update()
    {
        if (!string.IsNullOrEmpty(user.text.Trim()) && !string.IsNullOrEmpty(pwd.text.Trim()))
        {
            err_user.SetActive(false);
            err_pwd.SetActive(false);
            error_user.text = "";
            error_pwd.text = "";
            user.GetComponent<Image>().sprite = Resources.Load<Sprite>("Sprites/UI/Texto");
            pwd.GetComponent<Image>().sprite = Resources.Load<Sprite>("Sprites/UI/Texto");
        }
        else if (!string.IsNullOrEmpty(user.text.Trim()) && string.IsNullOrEmpty(pwd.text.Trim()))
        {
            err_user.SetActive(false);
            error_user.text = "";
            user.GetComponent<Image>().sprite = Resources.Load<Sprite>("Sprites/UI/Texto");
        }
        else if (string.IsNullOrEmpty(user.text.Trim()) && !string.IsNullOrEmpty(pwd.text.Trim()))
        {
            err_pwd.SetActive(false);
            error_pwd.text = "";
            pwd.GetComponent<Image>().sprite = Resources.Load<Sprite>("Sprites/UI/Texto");
        }
        if (!correct) //verificando si no existio ninfun error con la peticion para empesar con la autentificacion para realizar la conexion con el servidor de photon
        {
            text.text = "Autentificando"; //cambiando el texto si se conecto al servidor de photon
            if (!validate)
            {
                StartCoroutine(wait(3));
            }
        }
    }
    public void Authentication() //metodo para realizar el envio de los datos al iniciar sesion
    {
        if (string.IsNullOrEmpty(user.text.Trim()) && string.IsNullOrEmpty(pwd.text.Trim()))
        {
            err_user.SetActive(true);
            err_pwd.SetActive(true);
            error_user.text="El usuario es requerido";
            error_pwd.text = "La contraseña es requerida";
            user.GetComponent<Image>().sprite = Resources.Load<Sprite>("Sprites/UI/Texto_error");
            pwd.GetComponent<Image>().sprite = Resources.Load<Sprite>("Sprites/UI/Texto_error");
        }else if(string.IsNullOrEmpty(user.text.Trim()) && !string.IsNullOrEmpty(pwd.text.Trim()))
        {
            err_user.SetActive(true);
            error_user.text = "El usuario es requerido";
            user.GetComponent<Image>().sprite = Resources.Load<Sprite>("Sprites/UI/Texto_error");
        }
        else if (!string.IsNullOrEmpty(user.text.Trim()) && string.IsNullOrEmpty(pwd.text.Trim()))
        {
            err_pwd.SetActive(true);
            error_pwd.text = "La contraseña es requerida";
            pwd.GetComponent<Image>().sprite = Resources.Load<Sprite>("Sprites/UI/Texto_error");
        }
        else
        {
            panel.SetActive(true); //activando el panel de carga
            User authentication = new User(); //declarando la entidad user para el armado del json que se requiere para la peticion
            authentication.usuario = user.text.Trim(); //asignado el dato al atributo
            authentication.password = pwd.text.Trim(); //asignado el dato al atributo
            string json = JsonUtility.ToJson(authentication); //convirtindo a tipo json
            //Debug.Log(json);
            StartCoroutine(requestHttp.store(requestHttp.getAuth(), json, typeRequest.getAuth())); //realizando la peticiones para la api.
            correct = false;
            GameObject.Find("Button").GetComponent<Button>().enabled=false;
        }
    }

    public void ActiveSubmit()
    {
        GameObject.Find("Button").GetComponent<Button>().enabled = true;
        errors.SetActive(false);
        panel.SetActive(false); //activando el panel de carga
        validate = false;
        intent = 0;
        DataInfo.dataInfo._error = null;
        DataInfo.dataInfo.status = 0;
    }

    IEnumerator wait(int seconds)
    {
        validate = true;
        correct = true; // cambiando el estado para que no se repita de nuevo la configuracion.
        yield return new WaitForSeconds(seconds);
        if (string.IsNullOrEmpty(DataInfo.dataInfo._error.error_user) && DataInfo.dataInfo._error.errors_array.Count == 0 && DataInfo.dataInfo.status == 200) //verifica que el status sea 200 que es exitoso
        {
            text.text = "Conectando con el servidor"; // cambiando el texto 
            PhotonNetwork.PhotonServerSettings.AppSettings.FixedRegion = "sa"; //colocando a que region debe conectar
            PhotonNetwork.ConnectUsingSettings();//cargando las configuraciones dentro del servidor
            StartCoroutine(wait_one(2));
            correct = true; // cambiando el estado para que no se repita de nuevo la configuracion.
        }else if(DataInfo.dataInfo.status == 500)
        {
            errors.SetActive(true);
            //Debug.Log(DataInfo.dataInfo._error.error_user);
            text.text = "!Upss Ocurrió un error intente después de unos segundos"; // cambiando el texto 
        }
        else if (string.IsNullOrEmpty(DataInfo.dataInfo._error.error_user) && DataInfo.dataInfo.status == 0)
        {
            //Debug.Log("wait: "+intent);
            if (intent == 3)
            {
                //Debug.Log("denuevo request");
                User authentication = new User(); //declarando la entidad user para el armado del json que se requiere para la peticion
                authentication.usuario = user.text.Trim(); //asignado el dato al atributo
                authentication.password = pwd.text.Trim(); //asignado el dato al atributo
                string json = JsonUtility.ToJson(authentication); //convirtindo a tipo json
                StartCoroutine(requestHttp.store(requestHttp.getAuth(), json, typeRequest.getAuth())); //realizando la peticiones para la api.
                intent = 0;
            }
            StartCoroutine(wait(3));
            intent++;
        }
        else if (!string.IsNullOrEmpty(DataInfo.dataInfo._error.error_user) && DataInfo.dataInfo.status == 200)
        {
            errors.SetActive(true);
            //Debug.Log(DataInfo.dataInfo._error.error_user);
            text.text = DataInfo.dataInfo._error.error_user.ToString(); // cambiando el texto 
        }
        else
        {
            errors.SetActive(true);
            //Debug.Log(DataInfo.dataInfo._error.error_user);
            text.text = "!Upss Ocurrio un error intente después de unos segundos"; // cambiando el texto
        }
    }
    IEnumerator wait_one(int seconds)
    {
        yield return new WaitForSeconds(seconds);
        if (PhotonNetwork.IsConnected && connect)
        {
            SceneManager.LoadScene(1); //cambiando a otra escena luego de conectarce
        }
        else
        {
            //Debug.Log("no se conecto");
            PhotonNetwork.PhotonServerSettings.AppSettings.FixedRegion = "sa"; //colocando a que region debe conectar
            PhotonNetwork.ConnectUsingSettings();//cargando las configuraciones dentro del servidor 
            StartCoroutine(wait_one(2));
        }
    }
}
