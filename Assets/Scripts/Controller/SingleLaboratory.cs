﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;
using UnityEngine.SceneManagement;

public class SingleLaboratory : MonoBehaviour
{
    private int posicion;
    [SerializeField]
    private GameObject[] prefabLaboratory;
    [SerializeField]
    private GameObject[] prefabIndustrial;
    [SerializeField]
    private GameObject[] character;
    [SerializeField]
    private GameObject player;
    public GameObject _player { get { return player; } }
    private Vector3 vector = new Vector3(0, 1, 0);
    private TypeRequest typeRequest;//Instanciando la clase para realizar el tipo de peticion que se requiere
    private RequestHttp requestHttp;//Instanciando la clase para realizar los metodos de peticion y la url a utilizar
    // Start is called before the first frame update
    void Start()
    {
        requestHttp = new RequestHttp();//realizacion de la instancia para utilizar las peticiones hacia el api;
        typeRequest = new TypeRequest();//realizacion de la instancia para obtener el rol o el tipo de requeste que se requiere hacer al api
        foreach (Laboratory laboratory in DataInfo.dataInfo._laboratoryList.laboratory) //realizando un foreach para realizar la carga en el select 
        {
            if (laboratory._id == DataInfo.dataInfo._practiceJoind.id_lab)
            {
                if (typeRequest.getNormal().Equals(laboratory.tipo_lab))
                {
                    Instantiate(prefabLaboratory[laboratory.position], Vector3.zero, Quaternion.identity);//instancia el laboratorio mediante el arreglo asignado la poscion es la que elije el laboraotrio por lo cual en el arreglo debe de estar en la posicion que marque en la api y el arreglo
                }
                else if (typeRequest.getIndustrial().Equals(laboratory.tipo_lab))
                {
                    Instantiate(prefabIndustrial[laboratory.position], Vector3.zero, Quaternion.identity);//instancia el laboratorio mediante el arreglo asignado la poscion es la que elije el laboraotrio por lo cual en el arreglo debe de estar en la posicion que marque en la api y el arreglo
                }
                else
                {
                    SceneManager.LoadScene(MultiplayerSetting.multiplayerSetting.menuScene);
                }
            }
        }
        player = Instantiate(character[1], vector, Quaternion.identity);
    }
    void OnApplicationQuit()
    {
        StartCoroutine(requestHttp.show(requestHttp.getLogout(), typeRequest.getLogout()));//enviando la informacion por medio de la peticion para la creacion de la sala
    }
}
