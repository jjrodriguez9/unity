﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterSetup : MonoBehaviour
{
    public static CharacterSetup characterSetup; //realizando la clase de tipo statica para mantener la informacion
    public Transform[] spawPoints; //arreglo de los puntos de referencia de los avatar
    private void OnEnable()
    {
        if (CharacterSetup.characterSetup == null) //verificando que el objeto sea nullo
        {
            CharacterSetup.characterSetup = this; //inicializando con si mismo
        }
    }
}
