﻿using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameSetup : MonoBehaviourPunCallbacks
{
    public static GameSetup gameSetup;//realizando la clase de tipo statica para mantener la informacion
    public Transform[] spawPoints; //arreglo de los puntos de referencia de los avatar en el lobby y referencia de los laboratorios
    [SerializeField]
    private GameObject player;
    public GameObject _player { get { return player; } set { this.player = value; } }
    private void OnEnable()
    {
        if (GameSetup.gameSetup == null)//verificando que el objeto sea nullo
        {
            GameSetup.gameSetup = this; //inicializando con si mismo
        }
    }

    public void DisconnectPlayer()
    {
        Destroy(PhotonRoom.room.gameObject);
        MultiplayerSetting.multiplayerSetting.delayStart = false;
        StartCoroutine(DisconnectAndLoad());
    }
    public void StartPractice()
    {
        PhotonRoom.room.StartGame();
    }
    IEnumerator DisconnectAndLoad()
    {
        //PhotonNetwork.LeaveRoom();
        //yield return new WaitForSeconds(5);
        PhotonNetwork.Disconnect();
        while (PhotonNetwork.IsConnected)
        //while (PhotonNetwork.InRoom)
        {
            yield return null;
        }
        SceneManager.LoadScene(MultiplayerSetting.multiplayerSetting.menuScene);
    }
}
