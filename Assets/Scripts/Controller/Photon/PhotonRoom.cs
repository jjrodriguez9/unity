﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using System.IO;
using UnityEngine.SceneManagement;

public class PhotonRoom : MonoBehaviourPunCallbacks, IInRoomCallbacks
{
    //Room info
    public static PhotonRoom room; //creando clase statico para almacenar informacion
    private PhotonView PV; //instancia del photon view colocado en el objeto
    [SerializeField]
    private bool isGameLoaded; //variable boleano para verifiar si esta loegeado
    [SerializeField]
    private int currentScene; //valor de escena en que se encuentra
    //Player info
    Player[] photonPlayers; 
    public int playersInRoom; //valor de jugadores en sala
    public int myNumberInRoom; //valor del numero en la sala
    public int playerInGame; //valor de jugadores dentro del juego
    //Delayed start
    private bool readyToStart;//valor voleando para iniciar la partida
    public float startingTime; //valor de tiempo de inicio para dar inicio
    private float lessThanMaxPlayers; //valor minimo de jugadores
    private float timeToStart;//tiempo para empezar
    private Practice practice; //objeto de la practica

    private TypeRequest typeRequest; //Intanciando la clase para realizar el tipo de peticion que se requiere
    private RequestHttp requestHttp; //Intanciando la clase para realizar los metodos de peticion y la url a utilizar
    [SerializeField]
    private GameObject[] university;
    [SerializeField]
    private GameObject[] industrial;
    [SerializeField]
    private GameObject[] referencia_lobby;
    [SerializeField]
    private GameObject[] industrial_lobby;
    [SerializeField]
    private GameObject[] referencia_laboratory_espoch;
    [SerializeField]
    private GameObject[] referencia_laboratory_espe;
    [SerializeField]
    private GameObject[] referencia_character;
    [SerializeField]
    private GameObject[] industrial_laboratory;
    [SerializeField]
    private GameObject[] industrial_character;
    private string type;
    private void Awake()//funcion que se ejectua antes que el metodo start y poder crear el objeto en el proyecto
    {
        if (PhotonRoom.room == null)//verificar que el objeto se encuentre en nulo
        {
            PhotonRoom.room = this;//se asiga el objeto room  y this a punta si mismo.
        }
        else
        {
            if (PhotonRoom.room != this)//si dataInfo es diferente de si mismo procede a destruir.
            {
                Destroy(PhotonRoom.room.gameObject); //si dataInfo es diferente de si mismo procede a destruir.
                PhotonRoom.room = this;//se asiga el objeto room  y this a punta si mismo.
            }
        }
        DontDestroyOnLoad(this.gameObject);//crea el objeto pero que no sea destruido al momento de realizar un cambio de escena.
    }
    /**
     * metodo propio de photon para realizar los callbacktarget para el cambio de scena
     */
    public override void OnEnable()
    {
        base.OnEnable();
        PhotonNetwork.AddCallbackTarget(this);
        SceneManager.sceneLoaded += OnSceneFinishedLoading;
    }
    /**
     * metodo propio de photon para realizar los callbacktarget para el cambio de scena
     */
    public override void OnDisable()
    {
        base.OnDisable();
        PhotonNetwork.RemoveCallbackTarget(this);
        SceneManager.sceneLoaded -= OnSceneFinishedLoading;
    }
    // Start is called before the first frame update
    void Start()
    {
        practice = new Practice();//realizacion de la instancia de la entidad de la practica
        requestHttp = new RequestHttp();//realizacion de la instancia para utilizar las peticiones hacia el api;
        typeRequest = new TypeRequest();//realizacion de la instancia para obtener el rol o el tipo de requeste que se requiere hacer al api
        PV = GetComponent<PhotonView>(); //teniendo el photon view del objeto
        readyToStart = false; //inicializando el estado para inciar la practica
        lessThanMaxPlayers = startingTime; //tiempo de inciar el minimo de jugadores
        timeToStart = startingTime; //tiempo para  la iniciar
    }

    // Update is called once per frame
    void Update()
    {
        if (MultiplayerSetting.multiplayerSetting.delayStart) //verificando si el metodo delatstar se encuentra activo para ejectar la siguiente logica
        {
            if(playersInRoom == 1) //verificando si los jugadores en la sala es igual a uno para reiniciar el tiempo
            {
                RestartTimer(); //metodo que reinicia los valores
            }
            if (!isGameLoaded) //si no se encuentra logead activo para dar inicio a la practica
            {
                if (readyToStart)
                {
                    lessThanMaxPlayers = -Time.deltaTime;
                    timeToStart = lessThanMaxPlayers;
                }
                //Debug.Log("Display tiempo para comenzar de los jugadores " + timeToStart);
                if (timeToStart <= 0)
                {
                    StartGame();
                }
            }
        }
    }
    /**
     * metodo para realizar la union a la partida
     */
    public override void OnJoinedRoom()
    {
        base.OnJoinedRoom();
        //Debug.Log("master");
        photonPlayers = PhotonNetwork.PlayerList; //tamando a los jugadores en lista
        playersInRoom = photonPlayers.Length; //tomando el tamño de jugadores
        myNumberInRoom = playersInRoom; //valor de los jugadores que estan en sala
        PhotonNetwork.NickName = DataInfo.dataInfo._authentication.nombre + " " + DataInfo.dataInfo._authentication.apellido; //dandole el nombre al photon view
        if (MultiplayerSetting.multiplayerSetting.delayStart) //verificando si delat start cambio a true para ejecutar la siguiente logica
        {
            if (playersInRoom== MultiplayerSetting.multiplayerSetting.maxPlayer) //verificnaod en si el maximo de jugadores
            {
                readyToStart = true; //cambiando el valor de estado el inicio de la practica
                if (!PhotonNetwork.IsMasterClient) //verificando si master
                    return;
                PhotonNetwork.CurrentRoom.IsOpen = false; //cambiando el estado de la sala
                practice._id = DataInfo.dataInfo._practiceJoind._id; //tomando el id de la practica 
                practice.status = false; //cambiando el estado de la practica creada 
                string json = JsonUtility.ToJson(practice);//creando el json 
                StartCoroutine(requestHttp.store(requestHttp.getUpdatePractice(), json, typeRequest.getPractice())); //realizando la peticion al api para el cambio de estado
            }
        }
        else
        {
            StartLobby(); //metodo para llamar al lobby de la escena
        }
    }
    /**
     * metodo por donde se unen los demas usuarios a la sala creada 
     */
    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        base.OnPlayerEnteredRoom(newPlayer);
        Debug.Log("jugador");
        Debug.Log("un nuevo jugador se a unido a la sala");
        photonPlayers = PhotonNetwork.PlayerList; //tamando a los jugadores en lista
        playersInRoom++; //incrementando el valor de jugadore unidos
        //PhotonNetwork.NickName = DataInfo.dataInfo._authentication.nombre + " " + DataInfo.dataInfo._authentication.apellido; //dandole el nombre al photon view
        //Debug.Log("photon player: " + photonPlayers+" platerInroom: "+playersInRoom);
        if (MultiplayerSetting.multiplayerSetting.delayStart) //verificando si delat start cambio a true para ejecutar la siguiente logica
        {
            if (playersInRoom == MultiplayerSetting.multiplayerSetting.maxPlayer)//verificnaod en si el maximo de jugadores
            {
                readyToStart = true;//cambiando el estado para dar inicio a la partida
                if (!PhotonNetwork.IsMasterClient)
                    return;
                PhotonNetwork.CurrentRoom.IsOpen = false; //cambiando el estadi de la sala creada en el servidor de photon 
                DataInfo.dataInfo.startPractice = true; //cambiando el estado de la practica
                practice._id = DataInfo.dataInfo._practiceJoind._id; //tomando el id de la practica
                practice.status = false; //cambiando el estado de la practica
                string json = JsonUtility.ToJson(practice); //creando el json
                StartCoroutine(requestHttp.store(requestHttp.getUpdatePractice(), json, typeRequest.getPractice()));//realizando la peticion al api para el cambio de estado
            }
        }
        else
        {
            StartLobby(); //metodo para llamar al lobby de la escena
        }
    }
    /**
     * metodo para inicializar el lobby
     */
    void StartLobby()
    {
        if (!PhotonNetwork.IsMasterClient)
            return;
        MultiplayerSetting.multiplayerSetting.delayStart = true;//cambiando el estado para ejectuar la logica del inicio de practica
        PhotonNetwork.LoadLevel(MultiplayerSetting.multiplayerSetting.lobby); //cambiando a la escena de lobby
    }
    public void StartGame()
    {
        isGameLoaded = true;// activando el logeo
        if (!PhotonNetwork.IsMasterClient)//verificando si el master
            return;
        if (MultiplayerSetting.multiplayerSetting.delayStart)//verificando si delat start cambio a true para ejecutar la siguiente logica
        {
            PhotonNetwork.CurrentRoom.IsOpen = false;//cambiando el estado de la sala creada en el servidor de photones
            DataInfo.dataInfo.startPractice = true; //cambiando el valor de la practica
            practice._id = DataInfo.dataInfo._practiceJoind._id; //tomando el valor del id de la practica
            practice.status = false; //cambiando el estado de la practica
            string json = JsonUtility.ToJson(practice);//creando el json
            StartCoroutine(requestHttp.store(requestHttp.getUpdatePractice(), json, typeRequest.getPractice()));//realizando la peticion al api para el cambio de estado
        }
        PhotonNetwork.LoadLevel(MultiplayerSetting.multiplayerSetting.multiplayerScene);//cambiando a la escena donde se realiza la practica
    }
    /**
     * metodo para renicar el tiempo de inicio
     */
    void RestartTimer()
    {
        lessThanMaxPlayers = startingTime;//tomando el tiempo de inicio
        timeToStart = startingTime; //tomando el tiempo de inicio
        readyToStart = false; //cambiando el estado de la variable de inicio para la escena de la practica
    }
    /**
     * metodo para realizar el cambio de escena segun el index de la construccion
     */
    void OnSceneFinishedLoading(Scene scene, LoadSceneMode mode)
    {
        currentScene = scene.buildIndex;//tomando los index de la escena en construccion
        if (currentScene == MultiplayerSetting.multiplayerSetting.lobby) //verificando para la escena de lobby
        {
            RPC_CreatePlayer();//instanciando el photon network para instanciar el avatar
            //CreatePlayer();
        }
        else if(currentScene == MultiplayerSetting.multiplayerSetting.multiplayerScene)//verificando para la escena de practica
        {
            playerInGame++; //sumando jugadores
            isGameLoaded = true;//cambiando de valor logeo
            RPC_CreatePlayer(); //instanciando el photon network para instanciar el avatar
        }
    }
    /**
     * metodo para realizar la instancia de photon network que realiza la instancia del personaje o del laboratorio dependiendo de que escena se encuentra
     */
    private void RPC_CreatePlayer()
    {
        foreach (Laboratory laboratory in DataInfo.dataInfo._laboratoryList.laboratory) //realizando un foreach para realizar la carga en el select 
        {
            if (laboratory._id == DataInfo.dataInfo._practiceJoind.id_lab)//verfica que el id del laboratorio elejido sea el mismo del creado para cojer su posicion.
            {
                if (typeRequest.getNormal().Equals(laboratory.tipo_lab))
                {
                    //Debug.Log("if de tipo");
                    if (SceneManager.GetActiveScene().buildIndex == MultiplayerSetting.multiplayerSetting.lobby)
                    {
                        //Debug.Log("if de escena");
                        Instantiate(referencia_lobby[DataInfo.dataInfo._practiceJoind.universidad], Vector3.zero, Quaternion.identity);
                    }
                    else if (SceneManager.GetActiveScene().buildIndex == MultiplayerSetting.multiplayerSetting.multiplayerScene)
                    {
                        switch (DataInfo.dataInfo._practiceJoind.universidad)
                        {
                            case 0:
                                Instantiate(referencia_laboratory_espoch[laboratory.position], Vector3.zero, Quaternion.identity);
                                break;
                            case 1:
                                Instantiate(referencia_laboratory_espe[laboratory.position], Vector3.zero, Quaternion.identity);
                                break;
                        }
                        Instantiate(referencia_character[DataInfo.dataInfo._practiceJoind.universidad], Vector3.zero, Quaternion.identity);
                    }
                    //if (DataInfo.dataInfo._authentication.tipo_us.Equals(typeRequest.getTeacher()))
                    //{
                        switch (DataInfo.dataInfo._practiceJoind.universidad)
                        {
                            case 0:
                            if (GameObject.Find("fie") == null)
                            {
                                PhotonNetwork.Instantiate(Path.Combine("University/Espoch", "fie"), Vector3.zero, Quaternion.identity, 0);//instancia el photon network
                            }
                            break;
                            case 1:
                            if (GameObject.Find("Espe") == null)
                            {
                                PhotonNetwork.Instantiate(Path.Combine("University/Espe", "Espe"), Vector3.zero, Quaternion.identity, 0);//instancia el photon network
                            }
                            break;

                        }
                    //}
                }
                else if (typeRequest.getIndustrial().Equals(laboratory.tipo_lab))
                {
                    if (SceneManager.GetActiveScene().buildIndex == MultiplayerSetting.multiplayerSetting.lobby)
                    {
                        Instantiate(industrial_lobby[laboratory.position], Vector3.zero, Quaternion.identity);
                        switch (laboratory.position)
                        {
                            case 0:
                                if (GameObject.Find("industrial") == null)
                                {
                                    PhotonNetwork.Instantiate(Path.Combine("Laboratory/Prueba_Industrial/Macerado", "industrial"), Vector3.zero, Quaternion.identity, 0);//instancia el photon network
                                }
                                break;
                            case 1:
                                if (GameObject.Find("industrial") == null)
                                {
                                    PhotonNetwork.Instantiate(Path.Combine("Laboratory/Prueba_Industrial/Tanque_Serie", "industrial"), Vector3.zero, Quaternion.identity, 0);//instancia el photon network
                                }
                                break;
                        }
                    }
                    else if (SceneManager.GetActiveScene().buildIndex == MultiplayerSetting.multiplayerSetting.multiplayerScene)
                    {
                        Instantiate(industrial_laboratory[laboratory.position], Vector3.zero, Quaternion.identity);
                        Instantiate(industrial_character[laboratory.position], Vector3.zero, Quaternion.identity);
                    }
                }
                else
                {
                    SceneManager.LoadScene(MultiplayerSetting.multiplayerSetting.menuScene);
                }
                PhotonNetwork.Instantiate(Path.Combine("Characters", "PhotonNetworkPlayer"), transform.position, Quaternion.identity, 0);//instancia el photon network
            }
        }
        
    }
    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        base.OnPlayerLeftRoom(otherPlayer);
        Destroy(PV.gameObject);
        playerInGame--;
    }
    void OnApplicationQuit()
    {
        if (SceneManager.GetActiveScene().buildIndex == MultiplayerSetting.multiplayerSetting.lobby || SceneManager.GetActiveScene().buildIndex == MultiplayerSetting.multiplayerSetting.multiplayerScene)
        {
            StartCoroutine(requestHttp.show(requestHttp.getLogout(), typeRequest.getLogout()));//enviando la informacion por medio de la peticion para la creacion de la sala
        }
    }
}
