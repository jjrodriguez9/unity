﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using System.IO;
using UnityEngine.SceneManagement;

public class PhotonPlayer : MonoBehaviour
{
    private PhotonView PV; //retoma el photon view(es unico que crea un id unico dentro de la sala y que pertenece a cada avatar creado en la sala)
    public GameObject myAvatar; //objeto que tendra el avatar instanciado con el metodo de photon
    private int position;
    private TypeRequest typeRequest; //Instanciando la clase para realizar el tipo de peticion que se requiere
    // Start is called before the first frame update
    void Start()
    {
        typeRequest = new TypeRequest(); //realizacion de la instancia para obtener el rol o el tipo de requeste que se requiere hacer al api
        PV = GetComponent<PhotonView>(); //obteniendo el componente del photon view
        int spawnPicker = Random.Range(0, GameSetup.gameSetup.spawPoints.Length); //sacando random una posicion
        if (PV.IsMine) //verificando que solo sea propio para el
        {
            if (SceneManager.GetActiveScene().buildIndex == MultiplayerSetting.multiplayerSetting.multiplayerScene) //verficando en que escena se encuentra en la escena 3 para realizar la instancia de los laboratorios
            {
                if (PV.Owner.ActorNumber > 0)
                {
                    spawnPicker = PV.Owner.ActorNumber - 1;
                }
                else
                {
                    spawnPicker = PV.Owner.ActorNumber;
                }
                myAvatar = PhotonNetwork.Instantiate(Path.Combine("Characters/Male", "Male"), CharacterSetup.characterSetup.spawPoints[spawnPicker].position, CharacterSetup.characterSetup.spawPoints[spawnPicker].rotation, 0);  //instanciando el avatar tomando el id del photonview tomando asi las coordenadas para instancia en su posicion
                GameSetup.gameSetup._player = myAvatar;
                foreach (Laboratory laboratory in DataInfo.dataInfo._laboratoryList.laboratory) //realizando un foreach para realizar la carga en el select 
                {
                    if (laboratory._id == DataInfo.dataInfo._practiceJoind.id_lab)//verfica que el id del laboratorio elejido sea el mismo del creado para cojer su posicion.
                    {
                        position = laboratory.position; //posicion para utilizar la instancias de los laboratorios mediante el switch
                        if (typeRequest.getNormal().Equals(laboratory.tipo_lab))
                        {
                            switch (position) //verifica que tipo de peticion esta realizando el HMDI para instaciar el laboratorio el caso debe de ser destar dentro del case para que se instancie el objeto mediante el servicio de photon
                            {
                                case 0:
                                    PhotonNetwork.Instantiate(Path.Combine("Laboratory/Kuka", "Kuka_1"), GameSetup.gameSetup.spawPoints[spawnPicker].position, GameSetup.gameSetup.spawPoints[spawnPicker].rotation, 0); //instanciando el laboratorio tomando el id del photonview tomando asi las coordenadas para instancia en su posicion
                                    break;//termina el caso
                                case 1:
                                    PhotonNetwork.Instantiate(Path.Combine("Laboratory/UR3", "UR3_1"), GameSetup.gameSetup.spawPoints[spawnPicker].position, GameSetup.gameSetup.spawPoints[spawnPicker].rotation, 0); //instanciando el laboratorio tomando el id del photonview tomando asi las coordenadas para instancia en su posicion
                                    break;//termina el caso
                                case 2:
                                    PhotonNetwork.Instantiate(Path.Combine("Laboratory/Bandas_Transportadora", "Banda_Transportadoras"), GameSetup.gameSetup.spawPoints[spawnPicker].position, GameSetup.gameSetup.spawPoints[spawnPicker].rotation, 0); //instanciando el laboratorio tomando el id del photonview tomando asi las coordenadas para instancia en su posicion
                                    break;//termina el caso
                                case 3:
                                    PhotonNetwork.Instantiate(Path.Combine("Laboratory/Tanque_3D_Virtualizados", "Tanque_3D_Virtualizados_1"), GameSetup.gameSetup.spawPoints[spawnPicker].position, GameSetup.gameSetup.spawPoints[spawnPicker].rotation, 0); //instanciando el laboratorio tomando el id del photonview tomando asi las coordenadas para instancia en su posicion
                                    break;//termina el caso
                                default:
                                    SceneManager.LoadScene(MultiplayerSetting.multiplayerSetting.menuScene);
                                    break;
                            }
                        }
                        else if (typeRequest.getIndustrial().Equals(laboratory.tipo_lab))
                        {
                            switch (position) //verifica que tipo de peticion esta realizando el HMDI para instaciar el laboratorio el caso debe de ser destar dentro del case para que se instancie el objeto mediante el servicio de photon
                            {
                                case 0:
                                    PhotonNetwork.Instantiate(Path.Combine("Laboratory/Prueba_Industrial/Macerado", "industrial"), GameSetup.gameSetup.spawPoints[spawnPicker].position, GameSetup.gameSetup.spawPoints[spawnPicker].rotation, 0); //instanciando el laboratorio tomando el id del photonview tomando asi las coordenadas para instancia en su posicion
                                    break;//termina el caso
                                case 1:
                                    PhotonNetwork.Instantiate(Path.Combine("Laboratory/Prueba_Industrial/Tanque_Serie", "industrial"), GameSetup.gameSetup.spawPoints[spawnPicker].position, GameSetup.gameSetup.spawPoints[spawnPicker].rotation, 0); //instanciando el laboratorio tomando el id del photonview tomando asi las coordenadas para instancia en su posicion
                                    break;//termina el caso
                                default:
                                    SceneManager.LoadScene(MultiplayerSetting.multiplayerSetting.menuScene);
                                    break;
                            }
                        }
                        else
                        {
                            SceneManager.LoadScene(MultiplayerSetting.multiplayerSetting.menuScene);
                        }
                    }
                }
                
            }
            else
            {
                myAvatar = PhotonNetwork.Instantiate(Path.Combine("Characters/Male", "Male"), GameSetup.gameSetup.spawPoints[spawnPicker].position, GameSetup.gameSetup.spawPoints[spawnPicker].rotation, 0); //instanciando el avatar en la sala de lobby
                GameSetup.gameSetup._player = myAvatar;
            }
        }
    }

    private void Update()
    {
        //Debug.Log(PV.Owner);
        //Debug.Log(PhotonNetwork.NickName);
    }
}
