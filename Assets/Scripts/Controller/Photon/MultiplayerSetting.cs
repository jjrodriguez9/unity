﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultiplayerSetting : MonoBehaviour
{
    public static MultiplayerSetting multiplayerSetting; //realizando la clase de tipo statica para mantener la informacion
    public bool delayStart; //estado que verfica delaystart para otros metodos necesarios de photon
    public int maxPlayer; //maximos de player
    public int menuScene; //escena de menu
    public int lobby; //escena de lobby
    public int multiplayerScene; //escena de las practica
    /**
     * metodo que se ejectua antes de start 
     */
    private void Awake()
    {
        if (MultiplayerSetting.multiplayerSetting == null)//verificar que el objeto se encuentre en nulo
        {
            MultiplayerSetting.multiplayerSetting = this; //se asiga el objeto multiplayerSetting  y this a punta si mismo.
        }
        else
        {
            if (MultiplayerSetting.multiplayerSetting != this) //si multiplayerSetting es diferente de si mismo procede a destruir.
            {
                Destroy(this.gameObject); //Desgtruye el objeto.
            }
        }
        DontDestroyOnLoad(this.gameObject); //crea el objeto pero que no sea destruido al momento de realizar un cambio de escena
    }
}
