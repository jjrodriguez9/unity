﻿using UnityEngine;
using TMPro;
using static TMPro.TMP_Dropdown;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;

public class StatisticsRoomTeacher : MonoBehaviourPunCallbacks
{
    private TypeRequest typeRequest;//Instanciando la clase para realizar el tipo de peticion que se requiere
    private RequestHttp requestHttp;//Instanciando la clase para realizar los metodos de peticion y la url a utilizar
    private OptionDataList optionDataList;//instancia para cargar los datos en select
    [SerializeField]
    private GameObject Fondo;//objeto de fondo para visualizar
    [SerializeField]
    private GameObject[] character;//objeto donde va estar el avatar de cada entidad
    [SerializeField]
    private TMP_Dropdown selectLaboratory, selectUniversidad; //select donde se carga los laboratorios
    [SerializeField]
    private TMP_InputField[] inputField; //input donde saca la informacion llenada por el docente y poder crear la sala y guardar los datos
    private bool list = false; //validando el estado
    [SerializeField]
    private TMP_Text error_name, error_capacity, error_selection, error_universidad;
    [SerializeField]
    private GameObject err_name, err_capacity,err_selection, err_universidad;
    Practice practice;
    Person person;
    private bool correct = true;
    [SerializeField]
    private Button submit;
    [SerializeField]
    private GameObject panel, errors; //activacion del panel asignado
    [SerializeField]
    private TMP_Text text; //asignacion de un texto
    private bool connect = false;
    [SerializeField]
    private GameObject roomController;
    public override void OnConnectedToMaster() //Callback function for when the first connection is established successfully.
    {
        PhotonNetwork.AutomaticallySyncScene = true; //Makes it so whatever scene the master client has loaded is the scene all other clients will load
        connect = true;
        panel.transform.Find("Canvas").gameObject.transform.Find("Texto").GetComponent<TMP_Text>().text = "Conexion Existosa";
        panel.SetActive(false); //activando el panel de carga
    }

    private void Awake()
    {
        if (!PhotonNetwork.IsConnected)
        {
            panel.transform.Find("Canvas").gameObject.transform.Find("Texto").GetComponent<TMP_Text>().text = "Conectando"; // cambiando el textos
            panel.SetActive(true); //activando el panel de carga
            PhotonNetwork.PhotonServerSettings.AppSettings.FixedRegion = "sa"; //colocando a que region debe conectar
            PhotonNetwork.ConnectUsingSettings();//cargando las configuraciones dentro del servidor
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        requestHttp = new RequestHttp();//realizacion de la instancia para utilizar las peticiones hacia el api;
        typeRequest = new TypeRequest();//realizacion de la instancia para obtener el rol o el tipo de requeste que se requiere hacer al api
        optionDataList = new OptionDataList();//opcion para cargar los laboratorios en el select 
        if (DataInfo.dataInfo._authentication.tipo_us.Equals(typeRequest.getTeacher()) || DataInfo.dataInfo._authentication.tipo_us.Equals(typeRequest.getAdmin()))//validacion si es de tipo docente para cargar los objetos
        {
            Instantiate(roomController, Vector3.zero, Quaternion.identity);
            Fondo.SetActive(true); //activando el fondo
            Instantiate(character[1], character[1].transform.position, character[1].transform.rotation); //realizando la instancia del avatar
            StartCoroutine(requestHttp.show(requestHttp.getLaboratory(), typeRequest.getLaboratory()));//realizando la peticionde para los laboratorios 
            OptionData option = new OptionData();//realizacion de la instancia para cargar los datos en select
            option.text = "Seleccione";//colocando la opcion de seleccion en el select
            optionDataList.options.Add(option);//añadiendo a la lista las opciones
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (DataInfo.dataInfo._authentication.tipo_us.Equals(typeRequest.getTeacher()) || DataInfo.dataInfo._authentication.tipo_us.Equals(typeRequest.getAdmin()))//verificando que sea de tipo de estudiante
        {
            if (!list)//validando si la list es falso para realizar la carga de datos una sola vez
            {
                if (DataInfo.dataInfo._laboratoryList.laboratory.Count != 0)//verificando que el tamaño de arreglo del laboratorio sea diferente de cero para cargar en el select
                {
                    foreach (Laboratory laboratory in DataInfo.dataInfo._laboratoryList.laboratory)//realizando un foreach para realizar la carga en el select 
                    {
                        OptionData option = new OptionData();//realizando el objeto para cargar en el select 
                        option.text = laboratory.name;//enviando el nombre que aparecera en el select
                        optionDataList.options.Add(option);//cargando las opciones en el select
                    }
                    selectLaboratory.AddOptions(optionDataList.options);//añadiendo las opciones dentro de la interfaz
                    list = true;//cambiando el estado para no realizar la actualizacion otra vez
                }
            }

            if (list)//verificando que el metodo sea verdadero una ves cargado para cojer los index del select
            {
                int index;//valor donde se almacenara el index del select 
                index = selectLaboratory.value;//tomando el index del select seleccionado
                if (!string.IsNullOrEmpty(inputField[0].text.Trim()) && !string.IsNullOrEmpty(inputField[1].text.Trim()) && index != 0)
                {
                    err_name.SetActive(false);
                    err_capacity.SetActive(false);
                    err_selection.SetActive(false);
                    error_name.text = "";
                    error_capacity.text = "";
                    error_selection.text = "";
                    inputField[0].GetComponent<Image>().sprite = Resources.Load<Sprite>("Sprites/UI/Texto");
                    inputField[1].GetComponent<Image>().sprite = Resources.Load<Sprite>("Sprites/UI/Texto");
                    selectLaboratory.GetComponent<Image>().sprite = Resources.Load<Sprite>("Sprites/UI/Texto");
                }
                else if (!string.IsNullOrEmpty(inputField[0].text.Trim()) && string.IsNullOrEmpty(inputField[1].text.Trim()) && index == 0)
                {
                    err_name.SetActive(false);
                    error_name.text = "";
                    inputField[0].GetComponent<Image>().sprite = Resources.Load<Sprite>("Sprites/UI/Texto");
                }
                else if (!string.IsNullOrEmpty(inputField[0].text.Trim()) && !string.IsNullOrEmpty(inputField[1].text.Trim()) && index == 0)
                {
                    err_name.SetActive(false);
                    err_capacity.SetActive(false);
                    error_name.text = "";
                    error_capacity.text = "";
                    inputField[0].GetComponent<Image>().sprite = Resources.Load<Sprite>("Sprites/UI/Texto");
                    inputField[1].GetComponent<Image>().sprite = Resources.Load<Sprite>("Sprites/UI/Texto");
                }
                else if (string.IsNullOrEmpty(inputField[0].text.Trim()) && !string.IsNullOrEmpty(inputField[1].text.Trim()) && index == 0)
                {
                    err_capacity.SetActive(false);
                    error_capacity.text = "";
                    inputField[1].GetComponent<Image>().sprite = Resources.Load<Sprite>("Sprites/UI/Texto");
                }
                else if (string.IsNullOrEmpty(inputField[0].text.Trim()) && string.IsNullOrEmpty(inputField[1].text.Trim()) && index != 0)
                {
                    err_selection.SetActive(false);
                    error_selection.text = "";
                    selectLaboratory.GetComponent<Image>().sprite = Resources.Load<Sprite>("Sprites/UI/Texto");
                }
                else if (!string.IsNullOrEmpty(inputField[0].text.Trim()) && string.IsNullOrEmpty(inputField[1].text.Trim()) && index != 0)
                {
                    err_name.SetActive(false);
                    err_selection.SetActive(false);
                    error_name.text = "";
                    error_selection.text = "";
                    inputField[0].GetComponent<Image>().sprite = Resources.Load<Sprite>("Sprites/UI/Texto");
                    selectLaboratory.GetComponent<Image>().sprite = Resources.Load<Sprite>("Sprites/UI/Texto");
                }
                else if (string.IsNullOrEmpty(inputField[0].text.Trim()) && !string.IsNullOrEmpty(inputField[1].text.Trim()) && index != 0)
                {
                    err_capacity.SetActive(false);
                    err_selection.SetActive(false);
                    error_capacity.text = "";
                    error_selection.text = "";
                    inputField[1].GetComponent<Image>().sprite = Resources.Load<Sprite>("Sprites/UI/Texto");
                    selectLaboratory.GetComponent<Image>().sprite = Resources.Load<Sprite>("Sprites/UI/Texto");
                }
            }
            if (!correct) //verificando si no existio ninfun error con la peticion para empesar con la autentificacion para realizar la conexion con el servidor de photon
            {
                text.text = "Enviando"; //cambiando el texto si se conecto al servidor de photon
                StartCoroutine(wait(3));
            }
            //if (DataInfo.dataInfo.room)//verificando el status para la creacion de la sala en el servidor de photon
            //{
            //    Debug.Log("entre");
            //    RoomOptions roomOptions = new RoomOptions() { IsVisible = true, IsOpen = true, MaxPlayers = (byte)practice.capacidad };//cargando las diferente propiedades que requiere para la creacion de la sala
            //    PhotonNetwork.CreateRoom(practice.tema, roomOptions);//creando la sala en el servidor de photon
            //    DataInfo.dataInfo.room = false; //cambiando el estado para que solo se realize una sola vez la creacion de la sala
            //}
        }
    }
    /*
     * metodo para crear la sala
     */
    public void createRoom()
    {
        int index = selectLaboratory.value;//toamando el valor del index del select
        int index_universidad = selectUniversidad.value;//toamando el valor del index del select
        if (string.IsNullOrEmpty(inputField[0].text.Trim()) && string.IsNullOrEmpty(inputField[1].text.Trim()) && index==0)
        {
            err_name.SetActive(true);
            err_capacity.SetActive(true);
            err_selection.SetActive(true);
            error_name.text = "El nombre de la sala es requerido";
            error_capacity.text = "El número de estudiantes es requerida";
            error_selection.text = "Seleccione un laboratorio";
            inputField[0].GetComponent<Image>().sprite = Resources.Load<Sprite>("Sprites/UI/Texto_error");
            inputField[1].GetComponent<Image>().sprite = Resources.Load<Sprite>("Sprites/UI/Texto_error");
            selectLaboratory.GetComponent<Image>().sprite = Resources.Load<Sprite>("Sprites/UI/Texto_error");
        }
        else if (!string.IsNullOrEmpty(inputField[0].text.Trim()) && string.IsNullOrEmpty(inputField[1].text.Trim()) && index == 0)
        {
            err_capacity.SetActive(true);
            err_selection.SetActive(true);
            error_capacity.text = "El número de estudiantes es requerida";
            error_selection.text = "Seleccione un laboratorio";
            inputField[1].GetComponent<Image>().sprite = Resources.Load<Sprite>("Sprites/UI/Texto_error");
            selectLaboratory.GetComponent<Image>().sprite = Resources.Load<Sprite>("Sprites/UI/Texto_error");
        }
        else if (!string.IsNullOrEmpty(inputField[0].text.Trim()) && string.IsNullOrEmpty(inputField[1].text.Trim()) && index != 0)
        {
            err_capacity.SetActive(true);
            error_capacity.text = "El número de estudiante es requerida";
            inputField[1].GetComponent<Image>().sprite = Resources.Load<Sprite>("Sprites/UI/Texto_error");
        }
        else if (string.IsNullOrEmpty(inputField[0].text.Trim()) && !string.IsNullOrEmpty(inputField[1].text.Trim()) && index == 0)
        {
            err_name.SetActive(true);
            err_selection.SetActive(true);
            error_name.text = "El nombre de la sala es requerido";
            error_selection.text = "Seleccione un laboratorio";
            inputField[0].GetComponent<Image>().sprite = Resources.Load<Sprite>("Sprites/UI/Texto_error");
            selectLaboratory.GetComponent<Image>().sprite = Resources.Load<Sprite>("Sprites/UI/Texto_error");
        }
        else if (string.IsNullOrEmpty(inputField[0].text.Trim()) && !string.IsNullOrEmpty(inputField[1].text.Trim()) && index != 0)
        {
            err_name.SetActive(true);
            error_name.text = "El nombre de la sala es requerido";
            inputField[0].GetComponent<Image>().sprite = Resources.Load<Sprite>("Sprites/UI/Texto_error");
        }
        else if (string.IsNullOrEmpty(inputField[0].text.Trim()) && string.IsNullOrEmpty(inputField[1].text.Trim()) && index != 0)
        {
            err_name.SetActive(true);
            err_capacity.SetActive(true);
            error_name.text = "El nombre de la sala es requerido";
            error_capacity.text = "El número de estudiantes es requerida";
            inputField[0].GetComponent<Image>().sprite = Resources.Load<Sprite>("Sprites/UI/Texto_error");
            inputField[1].GetComponent<Image>().sprite = Resources.Load<Sprite>("Sprites/UI/Texto_error");
        }
        else if (string.IsNullOrEmpty(inputField[0].text.Trim()) && !string.IsNullOrEmpty(inputField[1].text.Trim()) && index != 0)
        {
            err_name.SetActive(true);
            error_name.text = "El nombre de la sala es requerido";
            inputField[0].GetComponent<Image>().sprite = Resources.Load<Sprite>("Sprites/UI/Texto_error");
        }
        else if (!string.IsNullOrEmpty(inputField[0].text.Trim()) && !string.IsNullOrEmpty(inputField[1].text.Trim()) && index == 0)
        {
            err_selection.SetActive(true);
            error_selection.text = "Seleccione un laboratorio";
            selectLaboratory.GetComponent<Image>().sprite = Resources.Load<Sprite>("Sprites/UI/Texto_error");
        }
        else if (string.IsNullOrEmpty(inputField[0].text.Trim()) && string.IsNullOrEmpty(inputField[1].text.Trim()) && index == 0 && index_universidad ==0 )
        { 
            err_name.SetActive(true);
            err_capacity.SetActive(true);
            err_selection.SetActive(true);
            err_universidad.SetActive(true);
            error_name.text = "El nombre de la sala es requerido";
            error_capacity.text = "El número de estudiantes es requerido";
            error_selection.text = "Seleccione un laboratorio";
            error_universidad.text = "Seleccione un laboratorio";
            inputField[0].GetComponent<Image>().sprite = Resources.Load<Sprite>("Sprites/UI/Texto_error");
            inputField[1].GetComponent<Image>().sprite = Resources.Load<Sprite>("Sprites/UI/Texto_error");
            selectLaboratory.GetComponent<Image>().sprite = Resources.Load<Sprite>("Sprites/UI/Texto_error");
            selectUniversidad.GetComponent<Image>().sprite = Resources.Load<Sprite>("Sprites/UI/Texto_error");
        }
        else
        {
            panel.SetActive(true); //activando el panel de carga
            person = new Person(); //realizando la inicializacion de la clase
            practice = new Practice(); //realizando la inicializacion de la clase
            person._id = DataInfo.dataInfo._authentication._id; //cargando el id del usuario logeado
            person.nombre = DataInfo.dataInfo._authentication.nombre; //cargando el nombre del usuario logeado
            person.apellido = DataInfo.dataInfo._authentication.apellido; //cargando el apellido del usuario logeado
            practice.tema = inputField[0].text.Trim(); //tomando el tema ingreasado en el input
            practice.capacidad = System.Convert.ToInt32(inputField[1].text.Trim())+1; //tomando la capacidad ingresada en el input
            practice.id_lab = DataInfo.dataInfo._laboratoryList.laboratory[index - 1]._id; //toamdnod el id del laboratorio en el index del select
            practice.status = true; // dando el estado true para que aparesca en la busquedas de las practicas creadas
            practice.universidad = index_universidad -1; // dando el estado true para que aparesca en la busquedas de las practicas creadas
            practice.docente.Add(person); //añadiendo el objeto del docente
            string json = JsonUtility.ToJson(practice); //convirtindo a tipo json
            StartCoroutine(requestHttp.store(requestHttp.getPractice(), json, typeRequest.getPractice()));//enviando la informacion por medio de la peticion para la creacion de la sala
            correct = false;
            submit.enabled = false;
            MultiplayerSetting.multiplayerSetting.maxPlayer = practice.capacidad;
        }
    }
    /**
     * metodo para la union a la sala 
     */
    public override void OnJoinedRoom()
    {
        //Debug.Log(PhotonNetwork.CurrentRoom.Name);
        //SceneManager.LoadScene(MultiplayerSetting.multiplayerSetting.lobby);
    }
    /**
     * metodo ejecuta si ocurrio algun error al crear la sala
     */
    public override void OnCreateRoomFailed(short returnCode, string message) //callback function for if we fail to create a room. Most likely fail because room name was taken.
    {
        Debug.Log("Fallo al crear una sala nueva, seguramente ya existe una sala con ese nombre");
        errors.SetActive(true);
        text.text = "!Upss Ocurrió un error intente después unos segundos"; // cambiando el texto 
    }

    IEnumerator wait(int seconds)
    {
        correct = true; // cambiando el estado para que no se repita de nuevo la configuracion.s
        yield return new WaitForSeconds(seconds);
        if (string.IsNullOrEmpty(DataInfo.dataInfo._error.error_practica) && DataInfo.dataInfo._error.errors_array.Count == 0 && DataInfo.dataInfo.status == 200) //verifica que el status sea 200 que es exitoso
        {
            text.text = "Creando Sala"; // cambiando el texto 
            RoomOptions roomOptions = new RoomOptions();//cargando las diferente propiedades que requiere para la creacion de la sala
            roomOptions.IsVisible = true;
            roomOptions.IsOpen = true;
            roomOptions.MaxPlayers = (byte) practice.capacidad;
            PhotonNetwork.CreateRoom(practice.tema, roomOptions);//creando la sala en el servidor de photon
            correct = true; // cambiando el estado para que no se repita de nuevo la configuracion.
        }
        else if (DataInfo.dataInfo.status == 500)
        {
            errors.SetActive(true);
            Debug.Log(DataInfo.dataInfo._error.error_practica);
            text.text = "!Upss Ocurrió un error intente después unos segundos"; // cambiando el texto 
        }
        else if (!string.IsNullOrEmpty(DataInfo.dataInfo._error.error_user) && DataInfo.dataInfo.status == 200)
        {
            errors.SetActive(true);
            Debug.Log(DataInfo.dataInfo._error.error_practica);
            text.text = DataInfo.dataInfo._error.error_practica.ToString(); // cambiando el texto 
        }
        else
        {
            errors.SetActive(true);
            Debug.Log(DataInfo.dataInfo._error.error_practica);
            text.text = "!Upss Ocurrió un error intente después unos segundos"; // cambiando el texto
        }
    }
    void OnApplicationQuit()
    {
        StartCoroutine(requestHttp.show(requestHttp.getLogout(), typeRequest.getLogout()));//enviando la informacion por medio de la peticion para la creacion de la sala
    }

    public void ActiveSubmit()
    {
        submit.enabled = true;
        errors.SetActive(false);
        panel.SetActive(false); //activando el panel de carga
        DataInfo.dataInfo._error = null;
        DataInfo.dataInfo.status = 0;
    }
}
