﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TabGroup : MonoBehaviour
{
    [SerializeField]
    private List<TabButton> tabButtons;
    [SerializeField]
    private Sprite tabIdle;
    [SerializeField]
    private Sprite tabHover;
    [SerializeField]
    private Sprite tabActive;
    [SerializeField]
    private TabButton selectedTab;
    [SerializeField]
    private List<GameObject> objectToSwap;
    private int index;
    public void Subscribe(TabButton button)
    {
        if (tabButtons == null)
        {
            tabButtons = new List<TabButton>();
        }
        tabButtons.Add(button);
    }
    public void OnTabEnter(TabButton button)
    {
        ResetTabs();
        if (selectedTab == null || button != selectedTab)
        {
            button.getBackground().sprite = tabHover;
        }
    }
    public void OnTabExit(TabButton button)
    {
        ResetTabs();
    }

    public void OnTabSelected(TabButton button)
    {
        if(selectedTab != null)
        {
            selectedTab.Deselect();
        }
        selectedTab = button;
        selectedTab.Select();
        if (selectedTab.name == "Desconectar")
        {
            if (SceneManager.GetActiveScene().buildIndex == 4)
            {
                SceneManager.LoadScene(MultiplayerSetting.multiplayerSetting.menuScene);
            }
            else if (SceneManager.GetActiveScene().buildIndex == MultiplayerSetting.multiplayerSetting.multiplayerScene)
            {
                GameSetup.gameSetup.DisconnectPlayer();
            }
            else if (SceneManager.GetActiveScene().buildIndex == MultiplayerSetting.multiplayerSetting.lobby)
            {
                GameSetup.gameSetup.DisconnectPlayer();
            }
        }
        else if (selectedTab.name == "Empezar")
        {
            if (SceneManager.GetActiveScene().buildIndex == MultiplayerSetting.multiplayerSetting.lobby)
            {
                //Debug.Log("entre al if de empezar");
                GameSetup.gameSetup.StartPractice();
            }
        }
        else
        {
            ResetTabs();
            button.getBackground().sprite = tabActive;
            index = button.transform.GetSiblingIndex();
            for (int i = 0; i < objectToSwap.Count; i++)
            {
                if (i == index)
                {
                    objectToSwap[i].SetActive(true);
                }
                else
                {
                    objectToSwap[i].SetActive(false);
                }
            }
        }
    }
    public void OnDefault()
    {
        if (selectedTab != null)
        {
            selectedTab.Deselect();
        }
        if (SceneManager.GetActiveScene().buildIndex == MultiplayerSetting.multiplayerSetting.lobby || SceneManager.GetActiveScene().buildIndex == MultiplayerSetting.multiplayerSetting.multiplayerScene)
        {
            selectedTab = tabButtons.Last();
            selectedTab.Select();
            tabButtons.Last().getBackground().sprite = tabActive;
            index = tabButtons.Last().transform.GetSiblingIndex();
        }
        else
        {
            selectedTab = tabButtons.First();
            selectedTab.Select();
            tabButtons.First().getBackground().sprite = tabActive;
            index = tabButtons.First().transform.GetSiblingIndex();
        }
        ResetTabs();
        for (int i = 0; i < objectToSwap.Count; i++)
        {
            if (i == index)
            {
                objectToSwap[i].SetActive(true);
            }
            else
            {
                objectToSwap[i].SetActive(false);
            }
        }
    }
    public void ResetTabs()
    {
        foreach(TabButton button in tabButtons)
        {
            if (selectedTab != null && button == selectedTab)
            {
                continue;
            }
            button.getBackground().sprite = tabIdle;
            
        }
    }

    
}
