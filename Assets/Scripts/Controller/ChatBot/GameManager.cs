﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Networking;
using JsonData;

public class GameManager : MonoBehaviour
{
    private string url = "https://dialogflow.googleapis.com/v2/projects/labot-jjhojl/agent/sessions/" + DataInfo.dataInfo._authentication._id + ":detectIntent";

    public string username;
    public int maxMessages = 25;

    public GameObject chatPanel, textObject;
    public UnityEngine.UI.InputField chatBox;

    public static string externalMessage;

    // public Color playerMessage, info;

    [SerializeField]
    List<Message> messageList;

    void Start()
    {

    }

    void Update()
    {
        if (!String.IsNullOrEmpty(externalMessage))
        {
            StartCoroutine(PostRequest(externalMessage));
            externalMessage = "";
        }
        if(chatBox.text != "")
        {
            if(Input.GetKeyDown(KeyCode.Return))
            {
                SendMessageToChat(username + ": " + chatBox.text, Message.MessageType.playerMessage);
                StartCoroutine(PostRequest(chatBox.text));
                chatBox.text = "";
            }
        }
        else
        {
            if(!chatBox.isFocused && Input.GetKeyDown(KeyCode.Return))
            {
                chatBox.ActivateInputField();
            }
        }

         //if(!chatBox.isFocused)
         //{
         //    if(Input.GetKeyDown(KeyCode.Space))
         //    {
         //        SendMessageToChat("You pressed space key!", Message.MessageType.info);
         //        Debug.Log("Space");
         //    }
         //}
    }

    public void SendMessageToChat(string text, Message.MessageType messageType)
    {

        if (messageList.Count >= maxMessages)
        {
            Destroy(messageList[0].textObject.gameObject);
            messageList.Remove(messageList[0]);
        }

        Message newMessage = new Message();

        newMessage.text = text;

        GameObject newText = Instantiate(textObject, chatPanel.transform);

        newMessage.textObject = newText.GetComponent<UnityEngine.UI.Text>();

        newMessage.textObject.text = newMessage.text;
        newMessage.textObject.color = MessageTypeColor(messageType);

        messageList.Add(newMessage);
    }

    Color MessageTypeColor(Message.MessageType messageType)
    {
        Color color = Color.black;

        switch(messageType)
        {
            case Message.MessageType.playerMessage:
                color = Color.black;
                break;
            case Message.MessageType.info:
                color = Color.blue;
                break;
        }

        return color;
    }


    public IEnumerator PostRequest(string chatBoxText){

        // Debug.Log(chatBoxText);
        string accessToken = string.Empty;
			while (!JwtCache.TryGetToken("labot-213@labot-jjhojl.iam.gserviceaccount.com", out accessToken))
				yield return JwtCache.GetToken("labot-jjhojl-ce93dc3d35fc",
					"labot-213@labot-jjhojl.iam.gserviceaccount.com");

        Debug.Log(chatBoxText);
		UnityWebRequest postRequest = new UnityWebRequest(url, "POST");
		RequestBody requestBody = new RequestBody();
		requestBody.queryInput = new QueryInput();
		requestBody.queryInput.text = new TextInput();
		requestBody.queryInput.text.text = chatBoxText;
		requestBody.queryInput.text.languageCode = "es";

		string jsonRequestBody = JsonUtility.ToJson(requestBody,true);
		Debug.Log(jsonRequestBody);

		byte[] bodyRaw = System.Text.Encoding.UTF8.GetBytes(jsonRequestBody);
		//Debug.Log(bodyRaw);
		postRequest.SetRequestHeader("Authorization", "Bearer " + accessToken);
		postRequest.uploadHandler = (UploadHandler)new UploadHandlerRaw(bodyRaw);
		postRequest.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
		postRequest.SetRequestHeader("Content-Type", "application/json");

		yield return postRequest.SendWebRequest();

		if(postRequest.isNetworkError || postRequest.isHttpError)
		{
			Debug.LogError(postRequest.responseCode);
            SendMessageToChat("chatbot:" + postRequest.responseCode, Message.MessageType.info);
			Debug.LogError(postRequest.error);
            SendMessageToChat("chatbot:" + postRequest.error, Message.MessageType.info);
		}
		else
		{
			// Show results as text
			Debug.Log("Response: " + postRequest.downloadHandler.text);

			// Or retrieve results as binary data
			byte[] resultbyte = postRequest.downloadHandler.data;
			string result = System.Text.Encoding.UTF8.GetString(resultbyte);
			ResponseBody content = (ResponseBody)JsonUtility.FromJson<ResponseBody>(result);
			Debug.Log(content.queryResult.fulfillmentText);

            SendMessageToChat("chatbot:" + content.queryResult.fulfillmentText, Message.MessageType.info);
            DialogManager.externalMessage = content.queryResult.fulfillmentText;
        }
	}

	IEnumerator GetAgent(string AccessToken)
	{
		UnityWebRequest www = UnityWebRequest.Get("https://dialogflow.googleapis.com/v2/projects/labot-jjhojl/agent");

		www.SetRequestHeader("Authorization", "Bearer " + AccessToken);

		yield return www.SendWebRequest();
		//myHttpWebRequest.PreAuthenticate = true;
		//myHttpWebRequest.Headers.Add("Authorization", "Bearer " + AccessToken);
		//myHttpWebRequest.Accept = "application/json";

		if (www.isNetworkError || www.isHttpError)
		{
			Debug.Log(www.error);
		}
		else
		{
			// Show results as text
			Debug.Log(www.downloadHandler.text);

			// Or retrieve results as binary data
			byte[] results = www.downloadHandler.data;
		}
	}
}

[System.Serializable]
public class Message
{
    public string text;
    public UnityEngine.UI.Text textObject;
    public MessageType messageType;

    public enum MessageType
    {
        playerMessage,
        info
    }
}