﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class DialogManager : MonoBehaviour
{

    [SerializeField] private float typingSpeed = 0.1f;

    [SerializeField] private float closeDelay = 3f;

    [Header("Dialouge TMP text")]
    [SerializeField] private TextMeshPro playerDialougeText;

    [Header("Continue Button")]
    [SerializeField] private GameObject playerContinueButton;

    [Header("Animation Controller")]
    [SerializeField] private Animator playerSpeechBubbleAnimator;

    [Header("Dialouge Sentences")]
    [TextArea]
    [SerializeField] private string[] playerDialougeSentences;

    private int playerIndex;

    private float speechBubbleAnimationDelay = 0.6f;

    public static string externalMessage;

    void Update()
    {
        if (!String.IsNullOrEmpty(externalMessage))
        {
            StartCoroutine(StartExternalDialog(externalMessage));
            externalMessage = "";
            Debug.Log("Entro");
        }
    }

    private void Start()
    {
        StartCoroutine(StartDialog());
    }

    public IEnumerator StartExternalDialog(string message)
    {
        playerSpeechBubbleAnimator.SetTrigger("Open");
        yield return new WaitForSeconds(speechBubbleAnimationDelay);
        
        foreach (char letter in message.ToCharArray())
        {
            playerDialougeText.text += letter;
            yield return new WaitForSeconds(typingSpeed);
        }

        StartCoroutine(CloseDialouge());
    }

    public IEnumerator StartDialog()
    {
        playerSpeechBubbleAnimator.SetTrigger("Open");
        yield return new WaitForSeconds(speechBubbleAnimationDelay);
        StartCoroutine(TypePlayerDialouge());
    }

    private IEnumerator TypePlayerDialouge()
    {
        foreach(char letter in playerDialougeSentences[playerIndex].ToCharArray())
        {
            playerDialougeText.text += letter;
            yield return new WaitForSeconds(typingSpeed);
        }

        StartCoroutine(CloseDialouge());

    }

    private IEnumerator CloseDialouge()
    {
        yield return new WaitForSeconds(closeDelay);
        playerDialougeText.text = string.Empty;
        playerSpeechBubbleAnimator.SetTrigger("Close");

        
    }
}
