﻿public class TypeRequest
{
    private static string auth; //agregando variables para la identificacion en switch
    private static string laboratory; //agregando variables para la identificacion en switch
    private static string admin; //agregando variables para la identificacion en switch
    private static string student; //agregando variables para la identificacion en switch
    private static string logout; //agregando variables para la identificacion en switch
    private static string teacher; //agregando variables para la identificacion en switch
    private static string practice; //agregando variables para la identificacion en switch
    private static string eventos; //agregando variables para la identificacion en switch
    private static string normal; //agregando variables para la identificacion en switch
    private static string insdustrial; //agregando variables para la identificacion en switch
    /**
     * Constructor para la construccion el tipo de peiticion o rol para la realizacion de la peticion a la api 
     */
    public TypeRequest()
    {
        auth = "auth";
        laboratory = "laboratory";
        admin = "administrador";
        student = "estudiante";
        teacher = "docente";
        practice = "practice";
        eventos = "eventPractice";
        normal = "Normal";
        insdustrial = "Industrial";
        logout = "logout";
    }
    public string getAuth() //metodo para obtener el valor de la variable
    {
        return auth;
    }
    public string getLogout() //metodo para obtener el valor de la variable
    {
        return logout;
    }
    public string getAdmin() //metodo para obtener el valor de la variable
    {
        return admin;
    }
    public string getLaboratory() //metodo para obtener el valor de la variable
    {
        return laboratory;
    }
    public string getPractice() //metodo para obtener el valor de la variable
    {
        return practice;
    }
    public string getEventPractice() //metodo para obtener el valor de la variable
    {
        return eventos;
    }
    public string getStudent() //metodo para obtener el valor de la variable
    {
        return student;
    }
    public string getTeacher() //metodo para obtener el valor de la variable
    {
        return teacher;
    }
    public string getNormal() //metodo para obtener el valor de la variable
    {
        return normal;
    }
    public string getIndustrial() //metodo para obtener el valor de la variable
    {
        return insdustrial;
    }

}
