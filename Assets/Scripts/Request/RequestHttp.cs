﻿using System.Collections;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;
using System;
using UnityEngine.SceneManagement;
using SimpleJSON;

public class RequestHttp
{
    //public static string url = "http://192.168.0.105/api/"; //dominio de la api
    public static string url = "http://201.159.223.211/api/"; //dominio de la api
    private static string auth; //url para la autentificacion de usuario
    private static string logut; //url para el studante
    public static string laboratory; //url de los laboratorios
    private static string practice; //url para la creacion de las practicas
    private static string listpractice; //url para listado de las practicas creadas
    private static string updatepractice; //url para la actualizacion del estado de la practica
    private static string storepractice; //url para la actualizacion del datos de la practica
    private Error errors;
    private PracticeList practiceList; //listado de las practicas creadas
    /**
     * Constructor para la construccion de las url para la utilizacion con la api 
     */
    public RequestHttp()
    {
        auth = url + "login"; //asignando el armado de la url correspondiente
        laboratory = url + "laboratory"; //asignando el armado de la url correspondiente
        practice = url + "practice/create"; //asignando el armado de la url correspondiente
        listpractice = url + "practice"; //asignando el armado de la url correspondiente
        updatepractice = url + "practice/update"; //asignando el armado de la url correspondiente
        storepractice = url + "practice/store"; //asignando el armado de la url correspondiente
        logut = url + "logout"; //asignando el armado de la url correspondiente
    }
    public string getAuth() //obteniendo la url de la autentifiacion de usuario
    {
        return auth;
    }
    public string getLogout() //obteniendo la url de la autentifiacion de usuario
    {
        return logut;
    }
    public string getLaboratory() //obteniendo la url de los laboratorios
    {
        return laboratory;
    }
    public string getPractice() //obteniendo la url para la creacion de la practica
    {
        return practice;
    }
    public string getListPractice() //obteniendo la url para el listado de las practicacs creadas
    {
        return listpractice;
    }
    public string getUpdatePractice() //obteniendo la url para la actualizacion del estado de la practica creada cuando se llene la sala
    {
        return updatepractice;
    }
    public string getStorePractice() //obteniendo la url para la actualizacion del estado de la practica creada cuando se llene la sala
    {
        return storepractice;
    }
    /**
     * Metodo para realizar la peticiones a la api mediante la utilizacion del metodo post
     */
    public IEnumerator store(string urls, string json, string type) //funciones que recive como parametros la url, json y el tipo donde se encuentra el envio de respectivo y el json de los datos
    {
        var request = new UnityWebRequest(urls, "POST"); //creao la peticion post mediante la utilizacion de UnityReqest donde recive la url y el tipo de metodo post
        byte[] bodyRaw = Encoding.UTF8.GetBytes(json); //decodifica el formato json a tipo byte para poder realizar el armado del json que se requiere para el envio a post. 
        request.uploadHandler = (UploadHandler)new UploadHandlerRaw(bodyRaw); //cargamos el dato codificado de tipo byte para la trasmicion textualmente como datos del cuerpo de solicitud HTTP esta es un búfer de memoria de código nativo.
        request.downloadHandler = new DownloadHandlerBuffer(); //permite la resepcion de datos de memoria nativa   
        request.SetRequestHeader("Access-Control-Allow-Credentials", "true"); //cors para la autorizacion del servidor
        request.SetRequestHeader("Access-Control-Allow-Methods", "GET, POST"); //metodos autorizados dentro de; servidor
        request.SetRequestHeader("Content-Type", "*"); //cabecera para la url
        request.SetRequestHeader("Content-Type", "application/json"); //se establece el tipo de cabecera que se va utilizar para la cual se establece de tipo json
        if (DataInfo.dataInfo._authentication != null) //comprobacion si la auntentificacion se encuentra logeado para aplicar la autentificacion Oauth2.
        {
            request.SetRequestHeader("Authorization", "Bearer " + DataInfo.dataInfo._authentication.token); //cabecera para la valizacion de la utilizacion de Oauth2 mediante api
        }
        yield return request.SendWebRequest(); //realiza el envio de la peticion al servidor para el registro de datos y devuelve la respuesta del mismo en la variable request
        if (request.isNetworkError || request.isHttpError) //verificacion si existio algun problema al enviar los datos dentro del api
        {
            DataInfo.dataInfo.status = request.responseCode; //tomama el status devuelto por la peticion al si sucedio algun error
        }
        else
        {
            DataInfo.dataInfo.status = request.responseCode;
            switch (type) //verifica que tipo de peticion esta realizando el HMDI para irse al caso corresponiente
            {
                case "auth":  //caso para la authentificacion del usuario 
                    //authenticationList = JsonUtility.FromJson<AuthenticationList>(request.downloadHandler.text); //transformado a formato json los datos devueltos por el api y al dato de tipo lista Authenticationlist que es un lista de tipo usuario
                    JSONNode user = JSON.Parse(request.downloadHandler.text);
                    errors = new Error();
                    errors.error_user = user["users"]["error"];
                    if (string.IsNullOrEmpty(errors.error_user))
                    {
                        DataInfo.dataInfo._error = errors;
                        Authentication user_autnentication = new Authentication();
                        user_autnentication._id = user["users"]["_id"];
                        user_autnentication.nombre = user["users"]["nombre"];
                        user_autnentication.apellido = user["users"]["apellido"];
                        user_autnentication.tipo_us = user["users"]["tipo_us"];
                        user_autnentication.universidad = user["users"]["universidad"];
                        user_autnentication.token = user["users"]["token"];
                        user_autnentication.refresh_token  = user["users"]["refresh_token"];
                        user_autnentication.expires_in = user["users"]["expires_in"];
                        DataInfo.dataInfo._authentication = user_autnentication;
                        DataInfo.dataInfo._error = errors;
                    }
                    else
                    {
                        DataInfo.dataInfo._error = errors;
                    }
                    break; //temina el caso
                case "practice":
                    JSONNode newPractice = JSON.Parse(request.downloadHandler.text);
                    errors = new Error();
                    if (newPractice["practice"]["error"].Count != 0)
                    {
                        foreach (var validate in newPractice["practice"]["error"])//realizando un foreach para realizar la carga en el select 
                        {
                            Debug.Log(validate.Key);
                            Debug.Log(validate.Value[0]);
                            Verification verification = new Verification();
                            verification.key = validate.Key;
                            verification.value = validate.Value[0];
                            errors.errors_array.Add(verification);
                        }
                    }
                    else
                    {
                        errors.error_practica = newPractice["practice"]["error"];
                    }
                    if (string.IsNullOrEmpty(errors.error_practica) && (errors.errors_array.Count == 0))
                    {
                        DataInfo.dataInfo._error = errors;
                        Practice respractice = new Practice();
                        respractice._id = newPractice["practice"]["_id"];
                        respractice.id_lab = newPractice["practice"]["id_lab"];
                        respractice.tema = newPractice["practice"]["tema"];
                        respractice.capacidad = newPractice["practice"]["capacidad"];
                        respractice.status = newPractice["practice"]["status"];
                        respractice.universidad = newPractice["practice"]["universidad"];
                        Person person = new Person();
                        person._id = newPractice["practice"]["docente"][0]["_id"];
                        person.nombre = newPractice["practice"]["docente"][0]["nombre"];
                        person.apellido = newPractice["practice"]["docente"][0]["apellido"];
                        respractice.docente.Add(person);
                        //DataInfo.dataInfo.room = true; //validando con un atributo boolean para la creacion en el servicio de photon una sola vez
                        DataInfo.dataInfo._practiceJoind = respractice; //guardando los datos enviados y transformados para la utilizacion dentro del proyecto.
                        //if (DataInfo.dataInfo.singlePractice)
                        //{
                        //    SceneManager.LoadScene(4); //cambiando a otra escena luego de conectarce
                        //}
                    }
                    else
                    {
                        DataInfo.dataInfo._error = errors;
                    }
                    break;//temina el caso
                case "eventPractice":
                    DataInfo.dataInfo.eventStore = false;
                    break;//termina el caso
            }
        }
    }
    /**
     * Metodo para realizar la peticiones a la api mediante la utilizacion del metodo get
     */
    public IEnumerator show(string urls, string type) //funciones que recive como parametros la urly tipo donde se encuentra el envio de peticion respectivo.
    {
        UnityWebRequest request = UnityWebRequest.Get(urls);//creao la peticion post mediante la utilizacion de UnityReqest donde recive la url y el tipo de get aqui no es necesario la utilizacion de buffer, debido a que la respuesta se genera apartir del api y no desde el la aplicativo.
        request.SetRequestHeader("Access-Control-Allow-Credentials", "true");//cors para la autorizacion del servidor
        request.SetRequestHeader("Access-Control-Allow-Methods", "GET, POST");//metodos autorizados dentro de; servidor
        request.SetRequestHeader("Content-Type", "*");//cabecera para la url
        request.SetRequestHeader("Content-Type", "application/json");//se establece el tipo de cabecera que se va utilizar para la cual se establece de tipo json
        if (DataInfo.dataInfo._authentication != null)//comprobacion si la auntentificacion se encuentra logeado para aplicar la autentificacion Oauth2.
        {
            request.SetRequestHeader("Authorization", "Bearer " + DataInfo.dataInfo._authentication.token);//cabecera para la valizacion de la utilizacion de Oauth2 mediante api
        }
        yield return request.SendWebRequest();//realiza el envio de la peticion al servidor para el registro de datos y devuelve la respuesta del mismo en la variable request
        if (request.isNetworkError || request.isHttpError)
        {
            DataInfo.dataInfo.status = request.responseCode;//tomama el status devuelto por la peticion al si sucedio algun error
        }
        else
        {
            switch (type)//verifica que tipo de peticion esta realizando el HMDI para irse al caso corresponiente
            {
                case "laboratory"://caso para los laboratorios
                    JSONNode laboratorys = JSON.Parse(request.downloadHandler.text);
                    LaboratoryList laboratoryList = new LaboratoryList(); //listado de los laboratorios
                    for (int i=0;i<laboratorys["laboratory"].Count;i++)
                    {
                        Laboratory lab = new Laboratory();
                        lab._id = laboratorys["laboratory"][i]["_id"];
                        lab.name = laboratorys["laboratory"][i]["name"];
                        lab.position = laboratorys["laboratory"][i]["position"];
                        lab.tipo_lab = laboratorys["laboratory"][i]["tipo_lab"];
                        laboratoryList.laboratory.Add(lab);
                    }
                    DataInfo.dataInfo._laboratoryList = laboratoryList; //guardando los datos enviados y transformados para la utilizacion dentro del proyecto.
                    break;//temina el caso
                case "practice"://caso para los laboratorios
                    practiceList = JsonUtility.FromJson<PracticeList>(request.downloadHandler.text);//transformado a formato json los datos devueltos por el api y al dato de tipo lista PracticeList que es un lista de tipo practice
                    DataInfo.dataInfo.status = request.responseCode; //tomando el status enviado por la peticion 
                    DataInfo.dataInfo._practiceList = practiceList;//guardando los datos enviados y transformados para la utilizacion dentro del proyecto.
                    break;//temina el caso
            }

        }
    }   
}