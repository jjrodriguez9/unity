﻿using System;
using System.Collections;
using System.Collections.Generic;
[Serializable]
public class EventPractice
{
    public string _id;
    public string id_estudiante;
    public List<DataEvent> eventos;
    public EventPractice()
    {
        eventos = new List<DataEvent>();
    }
}
