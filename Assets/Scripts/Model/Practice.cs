﻿using System;
using System.Collections.Generic;
/*
 * clase de la entidad de la practica para creacion de la misma con su atributos
 */
[Serializable]
public class Practice
{
    public string _id;
    public string tema;
    public int capacidad;
    public int universidad;
    public string id_lab;
    public bool status;
    public List<Person> docente;
    public Practice()
    {
        //docente = new PersonList();
        docente = new List<Person>();
    }
}


