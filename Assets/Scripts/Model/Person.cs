﻿using System;
/**
 * Clase de la entidad persona donde solo tiene los atributos de la persona para utilizan dentro de la creacion de la practica
 */
[Serializable]
public class Person
{
    public string _id;
    public string nombre;
    public string apellido;
}
