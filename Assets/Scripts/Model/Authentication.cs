﻿using System;
/**
 * clase de la entidad para el usuario que se a logeado y tiene los atirbutos
 */
[Serializable]
public class Authentication
{
    public string _id;
    public string nombre;
    public string apellido;
    public string tipo_us;
    public string universidad;
    public string token;
    public string refresh_token;
    public float expires_in;
}
