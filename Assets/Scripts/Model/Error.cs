﻿using System;
using System.Collections.Generic;

[Serializable]
public class Error
{
    public string error_user;
    public string error_practica;
    public List<Verification> errors_array;
    public Error()
    {
        errors_array = new List<Verification>();
    }
}
