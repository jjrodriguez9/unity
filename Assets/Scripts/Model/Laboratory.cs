﻿using System;
/**
 * Clase de la entidad laboratorio que tiene los atributos
 */
[Serializable]
public class Laboratory
{
    public string _id;
    public string name;
    public int position;
    public string tipo_lab;
}
