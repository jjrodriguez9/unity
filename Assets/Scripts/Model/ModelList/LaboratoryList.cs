﻿using System;
using System.Collections.Generic;
/**
 * clase de entidad creando una lista para los laboratorios.
 */
[Serializable]
public class LaboratoryList
{
    public List<Laboratory> laboratory;

    public LaboratoryList()
    {
        laboratory = new List<Laboratory>();
    }
}
