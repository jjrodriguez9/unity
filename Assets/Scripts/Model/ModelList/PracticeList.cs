﻿using System;
using System.Collections.Generic;
/**
 * clase de entidad creando una lista para las practicas
 */
[Serializable]
public class PracticeList
{
    public List<Practice> practice;
    public PracticeList()
    {
        practice = new List<Practice>();
    }
}
