﻿using System;
using System.Collections.Generic;
/**
 * clase de entidad creando una lista para la entidad
 */
[Serializable]
public class PersonList
{
    public List<Person> entity;
    public PersonList()
    {
        entity = new List<Person>();
    }
}
