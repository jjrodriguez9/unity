﻿using System;
using System.Collections.Generic;
/**
 * clase de entidad creando una lista para auntenticfacion del user.
 */
[Serializable]
public class AuthenticationList
{
    public List<Authentication> users;

    public AuthenticationList()
    {
        users = new List<Authentication>();
    }
}
