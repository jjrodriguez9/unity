﻿using System;
using System.Collections;
using System.Collections.Generic;
[Serializable]
public class DataEvent
{
    public List<float> Sp;
    public List<float> Error;
    public List<float> Accontrol;
    public List<float> Out;
    public DataEvent()
    {
        Sp = new List<float>();
        Error= new List<float>();
        Accontrol= new List<float>();
        Out = new List<float>();
    }
}

