﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoverGO : MonoBehaviour {
    public Transform[] target;
    public int current;
    public float speed;
    public int vuelta;
    public Quaternion quaternion;
    // Use this for initialization
    void Start()
    {
        current = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if(transform.position == target[current].position && vuelta == 1)
        {
            vuelta = 0;
            quaternion = Quaternion.Euler(0, 0, 0);
            transform.rotation = quaternion;
            current = 0;
        }
        if (transform.position != target[current].position)
        {
            Vector3 pos = Vector3.MoveTowards(transform.position, target[current].position, speed
                * Time.deltaTime);
            GetComponent<Rigidbody>().MovePosition(pos);
        }
        else
        {
            //current = (current + 1) % target.Length;
            if (current == target.Length - 1)
            {
                if (vuelta == 0)
                {
                    quaternion = Quaternion.Euler(0, -180, 0);
                    transform.rotation = quaternion;
                    current = 0;
                    vuelta = 1;

                }
            }
            else
            {
                current++;
            }
        }
    }
}
