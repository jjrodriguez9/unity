﻿using UnityEngine;
public class DataInfo : MonoBehaviour
{
    public static DataInfo dataInfo; //clase asignada y de tipi static para que almacene la informacion enviada para la utilizacion de proyecto, declaro que publica la clase setting para poder aceder por este medio a los demas atributos y static para que mantenga los datos a llamar a la clase si no colocamos el static
    //al momento de llamar la clase los valor  estaran incialiados en cero o nullos dependiendo el tipo de dato de la varibale que se tenga en este archivo.
    public long status; //atributo status para las peticiones 
    public bool room; //atributo room para la creacion de la sala en photon una sola vez
    public bool startPractice; // para inicializar la practica
    public bool eventStore; //atributo para controlar la peticion
    public bool singlePractice;
    [SerializeField]
    private Authentication authentication; //atributos de una entidad para el guadado de la informacion que viene del json
    public Authentication _authentication { set { this.authentication = value; } get { return authentication; } } //atributos publico que es un intemedio entre la variable publica y la privada para la obtencion de sus datos y modifcacion de los mismos
    [SerializeField]
    private Error error; //atributos de una entidad para el guadado de la informacion que viene del json
    public Error _error { set { this.error = value; } get { return error; } } //atributos publico que es un intemedio entre la variable publica y la privada para la obtencion de sus datos y modifcacion de los mismos
    [SerializeField]
    private LaboratoryList laboratoryList; //atributos de una entidad para el guadado de la informacion que viene del json 
    public LaboratoryList _laboratoryList { set { this.laboratoryList = value; } get { return laboratoryList; } } //atributos publico que es un intermedio entre la variable publica y la privada para la obtencion de sus datos y modifcacion de los mismos
    [SerializeField]
    private PracticeList practiceList; //atributos de una entidad para el guadado de la informacion que viene del json 
    [SerializeField]
    private Practice practice; //atributos de una entidad para el guadado de la informacion que viene del json 
    public PracticeList _practiceList { set { this.practiceList = value; } get { return practiceList; } } //atributos publico que es un intermedio entre la variable publica y la privada para la obtencion de sus datos y modifcacion de los mismos
    public Practice _practiceJoind { set { this.practice = value; } get { return practice; } } //atributos publico que es un intermedio entre la variable publica y la privada para la obtencion de sus datos y modifcacion de los mismos
    private void Awake() //funcion que se ejectua antes que el metodo start y poder crear el objeto en el proyecto
    {
        if (DataInfo.dataInfo == null) //verificar que el objeto se encuentre en nulo
        {
            DataInfo.dataInfo = this; //se asiga el objeto dataInfo  y this a punta si mismo.
        }
        else
        {
            if (DataInfo.dataInfo != this) //si dataInfo es diferente de si mismo procede a destruir.
            {
                Destroy(this.gameObject); //Desgtruye el objeto.
            }
        }
        DontDestroyOnLoad(this.gameObject); //crea el objeto pero que no sea destruido al momento de realizar un cambio de escena.
    }
}
